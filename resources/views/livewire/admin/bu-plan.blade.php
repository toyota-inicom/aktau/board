<div>
    <div class="row">
        <div class="col-12 px-2">
            <h1 class="text-center mt-5 mb-4 font-weight-bold" style="color: #b3b6ba">
                {{ config($depName.'_fields.title') }}
            </h1>
            <form action="" class="bg-light mb-5 p-4">
                <table class="table table-hover">
                    <thead>
                        <tr>
                            <th scope="col"></th>
                            @foreach (config($depName.'_fields.th') as $f)
                            <th scope="col" class="text-center">{{ $f }}</th>
                            @endforeach

                        </tr>
                    </thead>
                    <tbody>
                        <tr>
                            <td colspan="10">
                                <h4 class="text-center mt-1">Общие планы</h4>
                            </td>
                        </tr>
                        <tr>
                            <th scope="row" class="text-nowrap">Текущий год</th>
                            @foreach (config($depName.'_fields.y') as $f)
                            @if($f)
                            <td>
                                <input type="number" class="form-control" wire:model="yearPlan.{{ $f }}">
                            </td>
                            @else
                            <td></td>
                            @endif
                            @endforeach
                        </tr>
                        <tr>
                            <th scope="row" class="text-nowrap">Текущий месяц</th>
                            @foreach (config($depName.'_fields.y') as $f)
                            @if($f)
                            <td>
                                <input type="number" class="form-control" wire:model="monthPlan.{{ $f }}">
                            </td>
                            @else
                            <td></td>
                            @endif
                            @endforeach
                        </tr>
                        <tr>
                            <td colspan="10">
                                <h4 class="text-center mt-1">Планы сотрудников</h4>
                            </td>
                        </tr>
                        @foreach($users as $i=>$user)
                        <tr class="plan-tr">
                            <th scope="row" class="text-nowrap">{{$user->name}}</th>
                            @foreach (config($depName.'_fields.user') as $f)
                            @if(is_array($f))
                            @include('2line_plan', [
                            'title' => $f[2],
                            'i'=>$i,
                            'planField'=>$f[0],
                            'factField'=>$f[1]
                            ])
                            @else
                            @include('1line_plan',['f'=>$f,'i'=>$i])
                            @endif
                            @endforeach


                        </tr>
                        @endforeach

                        @foreach ($chiefs as $i=>$chief)
                        <tr>
                            <td colspan="10">
                                <h4 class="text-center mt-1">{{$chief->name}}</h4>
                            </td>
                        </tr>

                        <tr class="plan-tr">
                            <td colspan="2">
                                <!--выкуп-->
                                <div class="">
                                    <small class="form-text  text-center text-muted">Выкуп факт</small>
                                    <input type="number" class="form-control" wire:model="chiefs.{{$i}}.fields.redemption_fact">
                                </div>
                            </td>
                            <td colspan="2">
                                <!--огпо-->
                                <div class="">
                                    <small class="form-text  text-center text-muted">ОГПО факт</small>
                                    <input type="number" class="form-control" wire:model="chiefs.{{$i}}.fields.ogpo_fact">
                                </div>
                            </td>
                            <td colspan="3">
                                <!--постановка-->
                                <div class="">
                                    <small class="form-text  text-center text-muted">Постановка факт</small>
                                    <input type="number" class="form-control" wire:model="chiefs.{{$i}}.fields.staging_fact">
                                </div>
                            </td>
                            <td colspan="3">
                                <!--каско-->
                                <div class="">
                                    <small class="form-text  text-center text-muted">КАСКО факт</small>
                                    <input type="number" class="form-control" wire:model="chiefs.{{$i}}.fields.kasko_fact">
                                </div>
                            </td>
                        </tr>
                        @endforeach
                    </tbody>
                </table>
                <hr class="pb-3">
                <div class="actions d-flex pb-3">
                    <div class="col text-right">
                        <div wire:loading wire:target="save">
                            Сохраняем...
                        </div>
                        <button type="button" class="btn btn-success col-4" wire:click="save()">Сохранить</button>

                    </div>
                </div>

            </form>
        </div>
    </div>
    @section('styles')
    <style>
        .plan-tr td {
            vertical-align: bottom !important;
        }

        .plan-tr th {
            vertical-align: middle !important;
        }
    </style>
    @endsection
</div>