<div>
    <div class="row">
        <div class="col-12 px-5">
            <h1 class="text-center mt-5 mb-4 font-weight-bold" style="color: #b3b6ba">ПЛАНЫ ОПАСП</h1>
            <form action="" class="bg-light mb-5 p-4">
                <table class="table table-hover">
                    <thead>
                    <tr>
                        <th scope="col"></th>
                        <th scope="col" class="text-center">Принято</th>
                        <th scope="col" class="text-center">Оценки</th>
                        <th scope="col" class="text-center">CRM</th>
                        <th scope="col" class="text-center">Снятие</th>
                    </tr>
                    </thead>
                    <tbody>
                    <tr>
                        <td colspan="8"><h4 class="text-center mt-1">Общие планы</h4></td>
                    </tr>
                    <tr>
                        <th scope="row" class="text-nowrap">Текущий год</th>
                        <td>
                            <!--Принято-->
                            <input type="number" class="form-control" wire:model="yearPlan.aspsale_plan">
                        </td>
                        <td>
                            <!--Оценки-->
                            <input type="number" class="form-control" wire:model="yearPlan.asprequest_plan">
                        </td>
                        <td>

                        </td>
                        <td>
                            <input type="number" class="form-control" wire:model="yearPlan.withdrawal_plan">
                        </td>


                    </tr>
                    <tr>
                        <th scope="row" class="text-nowrap">Текущий месяц</th>

                        <td>
                            <!--Принято-->
                            <input type="number" class="form-control" wire:model="monthPlan.aspsale_plan">
                        </td>
                        <td>
                            <!--Оценки-->
                            <input type="number" class="form-control" wire:model="monthPlan.asprequest_plan">
                        </td>
                        <td>

                        </td>
                        <td>
                            <input type="number" class="form-control" wire:model="monthPlan.withdrawal_plan">
                        </td>

                    </tr>
                    <tr>
                        <td colspan="8"><h4 class="text-center mt-1">Планы сотрудников</h4></td>
                    </tr>
                    @foreach($users as $i=>$user)
                        <tr class="plan-tr">
                            <th scope="row" class="text-nowrap">{{$user->name}}</th>

                            <td>
                                <div class="">
                                    <small class="form-text mt-n2 text-center text-muted">план</small>
                                    <input type="number" class="form-control" wire:model="users.{{$i}}.fields.aspsale_plan">
                                </div>
                            </td>

                            <td>
                                <div class="">
                                    <small class="form-text mt-n2 text-center text-muted">план</small>
                                    <input type="number" class="form-control" wire:model="users.{{$i}}.fields.asprequest_plan">
                                </div>
                            </td>
                            <td>
                                <div class="">
                                    <small class="form-text mt-n2 text-center text-muted">факт</small>
                                    <input type="number" class="form-control" wire:model="users.{{$i}}.fields.crm">
                                </div>
                            </td>
                            <td>
                                <div class="col-6 float-left pl-0">
                                    <small class="form-text text-center text-muted">Снятие план</small>
                                    <input type="number" class="form-control" wire:model="users.{{$i}}.fields.withdrawal_plan">
                                </div>
                                <div class="col-6 float-right pr-0">
                                    <small class="form-text  text-center text-muted">Снятие факт</small>
                                    <input type="number" class="form-control" wire:model="users.{{$i}}.fields.withdrawal_fact">
                                </div>
                            </td>



                        </tr>
                    @endforeach
                    </tbody>
                </table>
                <hr class="pb-3">
                <div class="actions d-flex pb-3">
                    <div class="col text-right">
                        <div wire:loading.delay wire:target="save">
                            Сохраняем...
                        </div>
                        <button type="button" class="btn btn-success col-4" wire:click="save()">Сохранить</button>

                    </div>
                </div>

            </form>
        </div>
    </div>
    @section('styles')
        <style>
            .plan-tr td {
                vertical-align: bottom !important;
            }

            .plan-tr th {
                vertical-align: middle !important;
            }
        </style>
    @endsection
</div>
