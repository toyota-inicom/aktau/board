<div>
    <div class="row">
        <div class="col-12 px-5">
            <h1 class="text-center mt-5 mb-4 font-weight-bold" style="color: #b3b6ba">ПЛАНЫ ОПАСП</h1>
            <form action="" class="bg-light mb-5 p-4">
                <table class="table table-hover">
                    <thead>
                    <tr>
                        <th scope="col"></th>
                        <th scope="col" class="text-center">Продажи</th>
                        <th scope="col" class="text-center">Аксессуары</th>
                        <th scope="col" class="text-center">Пакеты ТО</th>
                        <th scope="col" class="text-center">КАСКО</th>
                        <th scope="col" class="text-center">ОГПО + Safe</th>
                    </tr>
                    </thead>
                    <tbody>
                    <tr>
                        <td colspan="8"><h4 class="text-center mt-1">Общие планы</h4></td>
                    </tr>
                    <tr>
                        <th scope="row" class="text-nowrap">Текущий год</th>
                        <td>
                            <!--аксессуары-->
                            <input type="number" class="form-control" wire:model="yearPlan.sales_plan">
                        </td>
                        <td>
                            <!--аксессуары-->
                            <input type="number" class="form-control" wire:model="yearPlan.acc_plan">
                        </td>
                        <td>
                            <!--пакеты то-->
                            <input type="number" class="form-control" wire:model="yearPlan.topack_plan">
                        </td>
                        <td>
                            <!--каско-->
                            <input type="number" class="form-control" wire:model="yearPlan.kasko_plan">
                        </td>

                        <td>
                            <!--тестдрайв-->
                            <input type="number" class="form-control" wire:model="yearPlan.ogpo_plan">
                        </td>

                    </tr>
                    <tr>
                        <th scope="row" class="text-nowrap">Текущий месяц</th>

                        <td>
                            <!--аксессуары-->
                            <input type="number" class="form-control" wire:model="monthPlan.sales_plan">
                        </td>
                        <td>
                            <!--аксессуары-->
                            <input type="number" class="form-control" wire:model="monthPlan.acc_plan">
                        </td>
                        <td>
                            <!--пакеты то-->
                            <input type="number" class="form-control" wire:model="monthPlan.topack_plan">
                        </td>
                        <td>
                            <!--каско-->
                            <input type="number" class="form-control" wire:model="monthPlan.kasko_plan">
                        </td>

                        <td>
                            <!--тестдрайв-->
                            <input type="number" class="form-control" wire:model="monthPlan.ogpo_plan">
                        </td>


                    </tr>
                    <tr>
                        <td colspan="8"><h4 class="text-center mt-1">Планы сотрудников</h4></td>
                    </tr>
                    @foreach($users as $i=>$user)
                        <tr class="plan-tr">
                            <th scope="row" class="text-nowrap">{{$user->name}}</th>

                            <td>
                                <!--аксессуары-->
                                <div class="">
                                    <small class="form-text mt-n2 text-center text-muted">план</small>
                                    <input type="number" class="form-control" wire:model="users.{{$i}}.fields.sales_plan">
                                </div>
                                <!--<div class="">
                                    <small class="form-text text-center text-muted">факт</small>
                                    <input type="number" class="form-control" v-model="worker.access[1]">
                                </div>-->
                            </td>
                            <td>
                                <!--аксессуары-->
                                <div class="">
                                    <small class="form-text mt-n2 text-center text-muted">план</small>
                                    <input type="number" class="form-control" wire:model="users.{{$i}}.fields.acc_plan">
                                </div>
                                <!--<div class="">
                                    <small class="form-text text-center text-muted">факт</small>
                                    <input type="number" class="form-control" v-model="worker.access[1]">
                                </div>-->
                            </td>
                            <td>
                                <!--пакеты то-->
                                <div class="">
                                    <small class="form-text mt-n2 text-center text-muted">план</small>
                                    <input type="number" class="form-control" wire:model="users.{{$i}}.fields.topack_plan">
                                </div>
                                <!--<div class="">
                                    <small class="form-text  text-center text-muted">факт</small>
                                    <input type="number" class="form-control" v-model="worker.to_pack[1]">
                                </div>-->
                            </td>
                            <td class="" style="background-color: rgb(209, 224, 238, .3);">
                                <!--каско-->
                                <div class="col-6 float-left pl-0">
                                    <small class="form-text text-center text-muted">КАСКО план</small>
                                    <input type="number" class="form-control" wire:model="users.{{$i}}.fields.kasko_plan">
                                </div>
                                <div class="col-6 float-right pr-0">
                                    <small class="form-text  text-center text-muted">КАСКО факт</small>
                                    <input type="number" class="form-control" wire:model="users.{{$i}}.fields.kasko_fact">
                                </div>
                            </td>


                            <td class="" style="background-color: rgb(209, 224, 238, .3);">
                                <!--огпо-->
                                <div class="col-6 float-left pl-0">
                                    <small class="form-text text-center text-muted">ОГПО план</small>
                                    <input type="number" class="form-control" wire:model="users.{{$i}}.fields.ogpo_plan">
                                </div>
                                <div class="col-6 float-right pr-0">
                                    <small class="form-text  text-center text-muted">ОГПО факт</small>
                                    <input type="number" class="form-control" wire:model="users.{{$i}}.fields.ogpo_fact">
                                </div>
                            </td>

                        </tr>
                    @endforeach
                    </tbody>
                </table>
                <hr class="pb-3">
                <div class="actions d-flex pb-3">
                    <div class="col text-right">
                        <div wire:loading.delay wire:target="save">
                            Сохраняем...
                        </div>
                        <button type="button" class="btn btn-success col-4" wire:click="save()">Сохранить</button>

                    </div>
                </div>

            </form>
        </div>
    </div>
    @section('styles')
        <style>
            .plan-tr td {
                vertical-align: bottom !important;
            }

            .plan-tr th {
                vertical-align: middle !important;
            }
        </style>
    @endsection
</div>
