<div>
    <div class="row">
        <div class="col-12">
            <div class="card card-custom">
                <div class="card-header">
                    <h3 class="card-title">
                        Новый пользователь
                    </h3>
                    <div class="card-toolbar">
                        <button class="btn btn-primary mr-2"
                                wire:click.prefetch="toggleForm()">{{!$form ? 'Добавить':'Отмена'}}</button>
                    </div>
                </div>
                @if($form)
                    <form wire:submit.prevent="store">
                        <div class="card-body">
                            <div class="form-group">
                                <label>Имя и фамилия <span class="text-danger">*</span></label>
                                <input type="text" class="form-control" wire:model="newUser.name"/>
                            </div>
                            <div class="form-group">
                                <label>ID в 1С <span class="text-danger">*</span></label>
                                <input type="number" class="form-control" wire:model="newUser.ext_id"/>
                            </div>
                            <div class="form-group">
                                <label>Отдел <span class="text-danger">*</span></label>
                                <select class="form-control" wire:model="newUser.dep">
                                    <option disabled selected value="0">Выберите отдел</option>
                                    @foreach(config('app_settings.deps') as $key=>$dep)
                                        <option value="{{$key}}">{{$dep}}</option>
                                    @endforeach
                                </select>
                            </div>
                        </div>
                        <div class="card-footer">
                            <button type="submit" class="btn btn-primary mr-2">Сохранить</button>
                            {{--                        <button type="reset" class="btn btn-secondary">Cancel</button>--}}
                        </div>
                    </form>
                @endif

            </div>
        </div>
    </div>
    <div class="row ">
        <div class="col-12 mt-3 ">
            <table class="table table-dark rounded">
                <thead>
                <tr>
                    <th scope="col">ID</th>
                    <th scope="col">Имя</th>
                    <th scope="col">Отдел</th>
                    <th scope="col">Активный</th>
                    <th scope="col">Действие</th>
                </tr>
                </thead>
                <tbody>
                @foreach($users as $user)
                    <tr>
                        <td>{{$user->ext_id}}</td>
                        <td>{{$user->name}}</td>
                        <td>{{config('app_settings.deps')[$user->dep]}}</td>
                        <td>
                            @if($user->active)
                                <span class="label label-inline label-light-success font-weight-bold cursor-pointer"
                                      wire:click="toggleActive({{$user->id}})">Активен</span>
                            @else
                                <span class="label label-inline label-light-danger font-weight-bold cursor-pointer"
                                      wire:click="toggleActive({{$user->id}})">Не активен</span>
                            @endif
                        </td>
                        <td>
                            <i class="ki ki-close icon-nm text-white cursor-pointer" title="Удалить пользователя"
                               onclick="confirm('Удалить пользователя?') || event.stopImmediatePropagation()"
                               wire:click="remove({{$user->id}})"></i>
                        </td>
                    </tr>
                @endforeach

                </tbody>
            </table>
        </div>
    </div>
</div>
