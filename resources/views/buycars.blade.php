<transition name="fade" v-if="workersBuy">
    <div class="container-fluid" v-show="activePage === 3">
        <div class="row">
            <div class="1st-page" style="opacity: 1">
                <div class="align-items-stretch col-12 d-flex dashboard-wrap flex-wrap p-0">

                    <!--заголовки и общие показатели-->
                    <div class="align-items-stretch col-12 d-flex flex-wrap p-0 top-rows">
                        <!--заголовок-->
                        <div class="align-items-center col-12 d-flex db-row header justify-content-between no-gutters p-0"
                             style="height: 25%">
                            <div class="col db-col header">
                                <!--дата время-->
                                <div class="align-items-center d-flex data-time p-3">
                                    <div class="logo col-6 text-left">
                                        <img src="/images/logo.png" alt="">
                                    </div>
                                    <div class="dt col-6 text-right">
                                        <div class="clock">
                                            <div class="time d-flex justify-content-end">
                                                <span id="hours" v-text="hours"></span>
                                                <span id="point">:</span>
                                                <span id="min" v-text="min"></span>
                                            </div>
                                            <div class="data" id="Date" v-text="datestr"></div>
                                        </div>
                                    </div>
                                </div>
                            </div>
                            <div class="col db-col title">Принято</div>
                            <div class="col db-col title">Оценка</div>
                            <div class="col db-col title">Обмен</div>
                            <div class="col db-col title">Снятие</div>
                            <div class="col db-col title">Выкуп</div>
                            <div class="col db-col title">План</div>
                        </div>

                        <!--годовой план-->
                        <div class="align-items-center col-12 d-flex db-row header justify-content-between no-gutters p-0"
                             style="height: 37.5%">
                            <div class="col d-flex db-col justify-content-between">
                                <div class="current-year d-inline-block pl-4 w-auto">{{date('Y')}}
                                    <small>год</small>
                                </div>
                                <div class="d-inline-block percent pr-4 text-right w-auto">
                                        <span class="record"
                                              style="font-size: 33px;color: rgb(48, 215, 18);line-height: 33px;text-shadow: 0 0 14px;">
                                            <em style="display: block;font-size: 20px;font-style: normal;color: rgb(48, 215, 18);line-height: 20px;margin: 0 0 -3px 0;">Рекорд</em>
                                            <span v-text="recordPercentBuy+'%'"></span>
                                        </span>
                                </div>
                            </div>


                            <div class="col db-col">
                                <div class="worker_digits">
                                    <div class="fact" :class="[planCls(planBuy.y.aspsale_plan, factBuy.y.aspsale)]" v-text="factBuy.y.aspsale"></div>
                                    <div class="plan" v-text="planBuy.y.aspsale_plan"></div>
                                </div>
                            </div>

                            <div class="col db-col">
                                <div class="worker_digits">
                                    <div class="fact" :class="[planCls(planBuy.y.asprequest_plan, factBuy.y.asprequest)]" v-text="factBuy.y.asprequest"></div>
                                    <div class="plan" v-text="planBuy.y.asprequest_plan"></div>
                                </div>
                            </div>

                            <div class="col db-col">
                                <div class="worker_digits">
                                    <div class="fact" :class="[planCls(planBuy.y.obmen_plan, factBuy.y.obmen)]" v-text="factBuy.y.obmen"></div>
                                    <div class="plan" v-text="planBuy.y.obmen_plan"></div>
                                </div>
                            </div>
                            <div class="col db-col">
                                <div class="worker_digits">
                                    <div class="fact" v-text="factBuy.y.withdrawal"></div>
                                    <div class="plan">&nbsp</div>
                                </div>
                            </div>
                            <div class="col db-col">
                                <div class="worker_digits">
                                    <div class="fact" v-text="factBuy.y.staging"></div>
                                    <div class="plan">&nbsp</div>
                                </div>
                            </div>

                            <div class="col db-col">
                                <div class="worker_digits">
                                    <div class="fact" v-text="yPercentBuy+'%'"></div>
                                    <div class="plan">&nbsp</div>
                                </div>
                            </div>

                            <div class="progress-line">
                                <div class="top" :style="{width:checkOverplanP(yPercentBuy)?'100%':yPercentBuy+'%', backgroundColor:getColor(yPercentBuy)}"></div>
                                <div class="bottom-wrap">
                                    <div class="bottom" :style="{width:checkOverplanP(yPercentBuy)?'100%':yPercentBuy+'%', backgroundColor:getColor(yPercentBuy)}"></div>
                                </div>
                            </div>

                        </div>

                        <!--месячный план-->
                        <div class="align-items-center col-12 d-flex db-row header justify-content-between no-gutters p-0"
                             style="height: 37.5%">
                            <div class="col d-flex db-col justify-content-between">
                                <div class="current-year d-inline-block pl-4 w-auto" v-text="monthName"></div>
                                <div class="d-inline-block percent pr-4 text-right w-auto"></div>
                            </div>

                            <div class="col db-col">
                                <div class="worker_digits">
                                    <div class="fact" :class="[planCls(planBuy.m.aspsale_plan, factBuy.m.aspsale)]" v-text="factBuy.m.aspsale"></div>
                                    <div class="plan" v-text="planBuy.m.aspsale_plan"></div>
                                </div>
                            </div>

                            <div class="col db-col">
                                <div class="worker_digits">
                                    <div class="fact" :class="[planCls(planBuy.m.asprequest_plan, factBuy.m.asprequest)]" v-text="factBuy.m.asprequest"></div>
                                    <div class="plan" v-text="planBuy.m.asprequest_plan"></div>
                                </div>
                            </div>

                            <div class="col db-col">
                                <div class="worker_digits">
                                    <div class="fact" :class="[planCls(planBuy.m.obmen_plan, factBuy.m.obmen)]" v-text="factBuy.m.obmen"></div>
                                    <div class="plan" v-text="planBuy.m.obmen_plan"></div>
                                </div>
                            </div>

                            <div class="col db-col">
                                <div class="worker_digits">
                                    <div class="fact" v-text="factBuy.m.withdrawal"></div>
                                    <div class="plan">&nbsp</div>
                                </div>
                            </div>
                            <div class="col db-col">
                                <div class="worker_digits">
                                    <div class="fact" v-text="factBuy.m.staging"></div>
                                    <div class="plan">&nbsp</div>
                                </div>
                            </div>

                            <!--crm-->
                            <div class="col db-col">
                                <div class="worker_digits">
                                    <div class="fact" v-text="monthPercentBuy+'%'"></div>
                                    <div class="plan">&nbsp</div>
                                </div>
                            </div>

                            <div class="progress-line">
                                <div class="top"
                                     :style="{width:checkOverplanP(monthPercentBuy)?'100%':monthPercentBuy+'%', backgroundColor:getColor(monthPercentBuy)}"></div>
                                <div class="bottom-wrap">
                                    <div class="bottom"
                                         :style="{width:checkOverplanP(monthPercentBuy)?'100%':monthPercentBuy+'%', backgroundColor:getColor(monthPercentBuy)}"></div>
                                </div>
                            </div>

                        </div>
                    </div>

                    <!--сотрудники-->
                    <transition-group
                            name="flip-list"
                            tag="div"
                            class="align-items-stretch col-12 d-flex flex-wrap p-0 top-rows"
                            v-if="orderedWorkersBuy">
                        <div :key="worker.id" v-for="worker,i in orderedWorkersBuy" v-if="i<5"
                             class="align-items-center col-12 d-flex db-row justify-content-between no-gutters p-0">
                            <div class="col d-flex db-col justify-content-between worker "
                                 :class="{'best':i===0, 'bad':i===workers.length-1}">
                                <div class="worker-photo d-inline-block w-auto">
                                    <img :src="worker.image" alt="">
                                </div>
                                <div class="align-items-end d-flex flex-column h-75 justify-content-end pr-4 rating w-auto mt-2">
                                    <div class="rating-arrow float-right green" :class="[worker.arrow]"
                                         :data-id="worker.id"></div>
                                    <div class="percent" v-text="worker.total_efficiency+'%'"></div>
                                    <div class="name text-right" v-html="(worker.name).split(' ').join('<br>')"></div>

                                    <!--Stars-->
                                    <div class="starsWrap">
                                        <i class="fa fa-star" :class="{'on':worker.rate>=70}"></i>
                                        <i class="fa fa-star" :class="{'on':worker.rate>=75}"></i>
                                        <i class="fa fa-star" :class="{'on':worker.rate>=80}"></i>
                                        <i class="fa fa-star" :class="{'on':worker.rate>=90}"></i>
                                        <i class="fa fa-star" :class="{'on':worker.rate>=95}"></i>
                                    </div>

                                </div>
                            </div>


                            <div class="col db-col">
                                <div class="worker_digits">
                                    <div class="fact" :class="{
                                                                    'planRed':worker.aspsale_fact*1 == 0,
                                                                    'planGreen':worker.aspsale_fact*1 >= worker.aspsale_plan*1
                                                                    }" v-text="worker.aspsale_fact">
                                    </div>
                                    <div class="plan" v-text="worker.aspsale_plan"></div>
                                </div>
                            </div>

                            <div class="col db-col">
                                <div class="worker_digits">
                                    <div class="fact" :class="{
                                                                    'planRed':worker.asprequest_fact*1 == 0,
                                                                    'planGreen':worker.asprequest_fact*1 >= worker.asprequest_plan*1
                                                                    }" v-text="worker.asprequest_fact">
                                    </div>
                                    <div class="plan" v-text="worker.asprequest_plan"></div>
                                </div>
                            </div>

                            <div class="col db-col">
                                <div class="worker_digits">
                                    <div class="fact" :class="{
                                                                    'planRed':worker.obmen_fact*1 == 0,
                                                                    'planGreen':worker.obmen_fact*1 >= worker.obmen_plan*1
                                                                    }" v-text="worker.obmen_fact">
                                    </div>
                                    <div class="plan" v-text="worker.obmen_plan"></div>
                                </div>
                            </div>


                            <div class="col db-col">
                                <div class="worker_digits">
                                    <div class="fact" :class="{
                                                                    'planRed':worker.withdrawal_fact*1 == 0,
                                                                    'planGreen':worker.withdrawal_fact*1 >= worker.withdrawal_plan*1
                                                                    }" v-text="worker.withdrawal_fact">
                                    </div>
                                    <div class="plan" v-text="worker.withdrawal_plan"></div>
                                </div>
                            </div>

                            <div class="col db-col">
                                <div class="worker_digits">
                                    <div class="fact" class="planGreen" v-text="worker.staging_fact">
                                    </div>
                                    <div class="plan">&nbsp</div>
                                </div>
                            </div>

                            <div class="col db-col">
                                <div class="worker_digits">
                                    <div class="fact" v-text="worker.total_percent+'%'"></div>
                                    <div class="plan">&nbsp</div>
                                </div>
                            </div>

                            <div class="progress-line">
                                <div class="top" :class="{'overplan':checkOverplan(worker)}"
                                     :style="{width:checkOverplan(worker)?'100%':worker.total_percent+'%', backgroundColor:getColor(worker.total_percent)}"></div>
                                <div class="bottom-wrap">
                                    <div class="bottom" :class="{'overplan':checkOverplan(worker)}"
                                         :style="{width:checkOverplan(worker)?'100%':worker.total_percent+'%', backgroundColor:getColor(worker.total_percent)}"></div>
                                </div>
                            </div>
                        </div>
                    </transition-group>


                </div>
            </div>
        </div>
    </div>
</transition>
