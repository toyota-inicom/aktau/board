<td style="vertical-align: middle !important;">
    <div class="">
        <small class="form-text mt-n2 text-center text-muted">{{ str_ends_with($f,'_plan')  ? 'план': 'факт'}}</small>
        <input type="number" class="form-control" wire:model="users.{{$i}}.fields.{{ $f }}">
    </div>
</td>
