<transition name="fade" v-if="workers">
    <div class="container-fluid" v-show="activePage === 1">
        <div class="row">
            <div class="1st-page" style="opacity: 1">
                <div class="align-items-stretch col-12 d-flex dashboard-wrap flex-wrap p-0">

                    <!--заголовки и общие показатели-->
                    <div class="align-items-stretch col-12 d-flex flex-wrap p-0 top-rows">
                        <!--заголовок-->
                        <div class="align-items-center col-12 d-flex db-row header justify-content-between no-gutters p-0"
                             style="height: 25%">
                            <div class="col db-col header">
                                <!--дата время-->
                                <div class="align-items-center d-flex data-time p-3">
                                    <div class="logo col-6 text-left">
                                        <img src="/images/logo.png" alt="">
                                    </div>
                                    <div class="dt col-6 text-right">
                                        <div class="clock">
                                            <div class="time d-flex justify-content-end">
                                                <span id="hours" v-text="hours"></span>
                                                <span id="point">:</span>
                                                <span id="min" v-text="min"></span>
                                            </div>
                                            <div class="data" id="Date" v-text="datestr"></div>
                                        </div>
                                    </div>
                                </div>
                            </div>
                            <div class="col db-col title">Колличество проданных автомобилей</div>
<!--                            <div class="col db-col title">Сумма дополнительного оборудования</div>
                            <div class="col db-col title">CR</div>-->
                            <div class="col db-col title">Авто проданные по программе трейд ИН</div>
                            <div class="col db-col title">План</div>
                        </div>

                        <!--годовой план-->
                        <div class="align-items-center col-12 d-flex db-row header justify-content-between no-gutters p-0"
                             style="height: 37.5%">
                            <div class="col d-flex db-col justify-content-between">
                                <div class="current-year d-inline-block pl-4 w-auto">{{date('Y')}}
                                    <small>год</small>
                                </div>
                                <div class="d-inline-block percent pr-4 text-right w-auto">
                                        <span class="record"
                                              style="font-size: 33px;color: rgb(48, 215, 18);line-height: 33px;text-shadow: 0 0 14px;">
                                            <em style="display: block;font-size: 20px;font-style: normal;color: rgb(48, 215, 18);line-height: 20px;margin: 0 0 -3px 0;">Лучший</em>
                                            <span v-text="recordPercent+'%'"></span>
                                        </span>
                                </div>
                            </div>

                            <!--Колличество проданных автомобилей	-->
                            <div class="col db-col">
                                <div class="worker_digits">
                                    <div class="fact"  v-text="fact.y.sales_fact"></div>
                                    <div class="plan" v-text="plan.y.sales_plan"></div>
                                </div>
                            </div>

                            <!--Сумма дополнительного оборудования-->
<!--                            <div class="col db-col accessories">
                                <div class="worker_digits">
                                    <div class="fact"  v-text="fact.y.acc_fact"></div>
                                    <div class="plan" v-text="plan.y.acc_plan"></div>
                                </div>
                            </div>-->

                            <!--CR-->
<!--                            <div class="col db-col ">
                                <div class="worker_digits">
                                    <div class="fact"  v-text="fact.y.cr_fact"></div>
                                    <div class="plan" v-text="plan.y.cr_plan"></div>
                                </div>
                            </div>-->

                            <!--Авто проданные по программе трейд ИН-->
                            <div class="col db-col ">
                                <div class="worker_digits">
                                    <div class="fact"  v-text="fact.y.tradein_fact"></div>
                                    <div class="plan" v-text="plan.y.tradein_plan"></div>
                                </div>
                            </div>

                            <!--План-->
                            <div class="col db-col">
                                <div class="worker_digits">
                                    <div class="fact mt-n4" v-text="yPercent+'%'"></div>
                                </div>
                            </div>


                            <div class="progress-line">
                                <div class="top" :style="{width:checkOverplanP(yPercent)?'100%':yPercent+'%', backgroundColor:getColor(yPercent)}"></div>
                                <div class="bottom-wrap">
                                    <div class="bottom" :style="{width:checkOverplanP(yPercent)?'100%':yPercent+'%', backgroundColor:getColor(yPercent)}"></div>
                                </div>
                            </div>

                        </div>

                        <!--месячный план-->
                        <div class="align-items-center col-12 d-flex db-row header justify-content-between no-gutters p-0"
                             style="height: 37.5%">
                            <div class="col d-flex db-col justify-content-between">
                                <div class="current-year d-inline-block pl-4 w-auto" v-text="monthName"></div>
                                <div class="d-inline-block percent pr-4 text-right w-auto"></div>
                            </div>

                            <!--Колличество проданных автомобилей	-->
                            <div class="col db-col">
                                <div class="worker_digits">
                                    <div class="fact"  v-text="fact.m.sales_fact"></div>
                                    <div class="plan" v-text="plan.m.sales_plan"></div>
                                </div>
                            </div>

                            <!--Сумма дополнительного оборудования-->
<!--                            <div class="col db-col accessories">
                                <div class="worker_digits">
                                    <div class="fact"  v-text="fact.m.acc_fact"></div>
                                    <div class="plan" v-text="plan.m.acc_plan"></div>
                                </div>
                            </div>-->

                            <!--CR-->
<!--                            <div class="col db-col ">
                                <div class="worker_digits">
                                    <div class="fact"  v-text="fact.m.cr_fact"></div>
                                    <div class="plan" v-text="plan.m.cr_plan"></div>
                                </div>
                            </div>-->

                            <!--Авто проданные по программе трейд ИН-->
                            <div class="col db-col ">
                                <div class="worker_digits">
                                    <div class="fact"  v-text="fact.m.tradein_fact"></div>
                                    <div class="plan" v-text="plan.m.tradein_plan"></div>
                                </div>
                            </div>

                            <!--crm-->
                            <div class="col db-col">
                                <div class="worker_digits">
                                    <div class="fact mt-n4" v-text="monthPercent+'%'"></div>
                                </div>
                            </div>

                            <div class="progress-line">
                                <div class="top" :style="{width:checkOverplanP(monthPercent)?'100%':monthPercent+'%', backgroundColor:getColor(monthPercent)}"></div>
                                <div class="bottom-wrap">
                                    <div class="bottom" :style="{width:checkOverplanP(monthPercent)?'100%':monthPercent+'%', backgroundColor:getColor(monthPercent)}"></div>
                                </div>
                            </div>

                        </div>
                    </div>

                    <!--сотрудники-->
                    <transition-group
                        name="flip-list"
                        tag="div"
                        class="align-items-stretch col-12 d-flex flex-wrap p-0 top-rows"
                        v-if="orderedWorkers">
                        <div :key="worker.id" v-for="worker,i in orderedWorkers" v-if="i<5"
                             class="align-items-center col-12 d-flex db-row justify-content-between no-gutters p-0">
                            <div class="col d-flex db-col justify-content-between worker "
                                 :class="{'best':i===0, 'bad':i===workers.length-1}">
                                <div class="worker-photo d-inline-block w-auto">
                                    <img :src="worker.image" alt="">
                                </div>
                                <div class="align-items-end d-flex flex-column h-75 justify-content-end pr-4 rating w-auto mt-2">
                                    <div class="rating-arrow float-right green" :class="[worker.arrow]"
                                         :data-id="worker.id"></div>
                                    <div class="percent" v-text="worker.total_percent+'%'"></div>
                                    <div class="name text-right" v-html="(worker.name).split(' ').join('<br>')"></div>

                                    <!--Stars-->
<!--                                    <div class="starsWrap">
                                        <i class="fa fa-star" :class="{'on':worker.rate>=70}"></i>
                                        <i class="fa fa-star" :class="{'on':worker.rate>=75}"></i>
                                        <i class="fa fa-star" :class="{'on':worker.rate>=80}"></i>
                                        <i class="fa fa-star" :class="{'on':worker.rate>=90}"></i>
                                        <i class="fa fa-star" :class="{'on':worker.rate>=95}"></i>
                                    </div>-->

                                </div>
                            </div>


                            <!--Колличество проданных автомобилей-->
                            <div class="col db-col">
                                <div class="worker_digits">
                                    <div class="fact" :class="{
                                    'planRed':worker.sales_fact*1 == 0,
                                    'planGreen':worker.sales_fact*1 >= worker.sales_plan*1
                                    }" v-text="worker.sales_fact">
                                    </div>
                                    <div class="plan" v-text="worker.sales_plan">b</div>
                                </div>
                            </div>

                            <!--Сумма проданых запасных частей-->
<!--                            <div class="col db-col accessories">
                                <div class="worker_digits">
                                    <div class="fact" :class="{
                                    'planRed':worker.acc_fact*1 == 0,
                                    'planGreen':worker.acc_fact*1 >= worker.acc_plan*1,
                                    }" v-text="cur(worker.acc_fact)">
                                    </div>
                                    <div class="plan" v-text="cur(worker.acc_plan)"></div>
                                </div>
                            </div>-->

                            <!--CR-->
<!--                            <div class="col db-col">
                                <div class="worker_digits">
                                    <div class="fact" :class="{
                                    'planRed':worker.cr_fact*1 == 0,
                                    'planGreen':worker.cr_fact*1 >= worker.cr_plan*1
                                    }" v-text="worker.cr_fact">
                                    </div>
                                    <div class="plan" v-text="worker.cr_plan"></div>
                                </div>
                            </div>-->

                            <!--Авто проданные по программе трейд ИН-->
                            <div class="col db-col">
                                <div class="worker_digits">
                                    <div class="fact" :class="{
                                    'planRed':worker.tradein_fact*1 == 0,
                                    'planGreen':worker.tradein_fact*1 >= worker.tradein_plan*1,
                                    }" v-text="worker.tradein_fact">
                                    </div>
                                    <div class="plan" v-text="worker.tradein_plan"></div>
                                </div>
                            </div>

                            <!--crm-->
                            <div class="col db-col">
                                <div class="worker_digits">
                                    <div class="fact" v-text="worker.total_percent+'%'"></div>
                                    <div class="plan"></div>
                                </div>
                            </div>

                            <div class="progress-line">
                                <div class="top" :class="{'overplan':checkOverplan(worker)}"
                                     :style="{width:checkOverplan(worker)?'100%':worker.total_percent+'%', backgroundColor:getColor(worker.total_percent)}"></div>
                                <div class="bottom-wrap">
                                    <div class="bottom" :class="{'overplan':checkOverplan(worker)}"
                                         :style="{width:checkOverplan(worker)?'100%':worker.total_percent+'%', backgroundColor:getColor(worker.total_percent)}"></div>
                                </div>
                            </div>
                        </div>
                    </transition-group>
                </div>
            </div>
        </div>
    </div>
</transition>
