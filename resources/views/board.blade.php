@extends("layouts.board")

@section('content')
    @include("newcars")
    @include("salecars")
    @include("buycars")
    @include("service_mechcars")
    @include("bestmonth")
@endsection
