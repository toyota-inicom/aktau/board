<transition name="fade" v-if="workersServiceMech">
    <div class="container-fluid" v-show="activePage === 5">
        <div class="row">
            <div class="1st-page" style="opacity: 1">
                <div class="align-items-stretch col-12 d-flex dashboard-wrap flex-wrap p-0">

                    <!--заголовки и общие показатели-->
                    <div class="align-items-stretch col-12 d-flex flex-wrap p-0 top-rows">
                        <!--заголовок-->
                        <div class="align-items-center col-12 d-flex db-row header justify-content-between no-gutters p-0"
                             style="height: 25%">
                            <div class="col db-col header">
                                <!--дата время-->
                                <div class="align-items-center d-flex data-time p-3">
                                    <div class="logo col-6 text-left">
                                        <img src="/images/logo.png" alt="">
                                    </div>
                                    <div class="dt col-6 text-right">
                                        <div class="clock">
                                            <div class="time d-flex justify-content-end">
                                                <span id="hours" v-text="hours"></span>
                                                <span id="point">:</span>
                                                <span id="min" v-text="min"></span>
                                            </div>
                                            <div class="data" id="Date" v-text="datestr"></div>
                                        </div>
                                    </div>
                                </div>
                            </div>
                            <div class="col db-col title">
                                Продажи
                            </div>
                            <div class="col db-col title">Аксессуары</div>
                            <div class="col db-col title">Пакеты ТО</div>
                            <div class="col db-col title">КАСКО</div>
                            <div class="col db-col title">Trade-In + Выкуп</div>
                            <div class="col db-col title">Test Drive</div>
                            <div class="col db-col title">ОГПО + Safe</div>
                            <div class="col db-col title">Постановка за наличку</div>
                            <div class="col db-col title">План</div>
                        </div>

                        <!--годовой план-->
                        <div class="align-items-center col-12 d-flex db-row header justify-content-between no-gutters p-0"
                             style="height: 37.5%">
                            <div class="col d-flex db-col justify-content-between">
                                <div class="current-year d-inline-block pl-4 w-auto">{{date('Y')}}
                                    <small>год</small>
                                </div>
                                <div class="d-inline-block percent pr-4 text-right w-auto">
                                        <span class="record"
                                              style="font-size: 33px;color: rgb(48, 215, 18);line-height: 33px;text-shadow: 0 0 14px;">
                                            <em style="display: block;font-size: 20px;font-style: normal;color: rgb(48, 215, 18);line-height: 20px;margin: 0 0 -3px 0;">Рекорд</em>
                                            <span v-text="recordPercent+'%'"></span>
                                        </span>
                                </div>
                            </div>

                            <!--продажи-->
                            <div class="col db-col">
                                <div class="worker_digits">
                                    <div class="fact" :class="[planCls(plan.y.sales_plan, fact.y.sales)]" v-text="fact.y.sales"></div>
                                    <div class="plan" v-text="plan.y.sales_plan"></div>
                                </div>
                            </div>

                            <!--аксессуары-->
                            <div class="col db-col accessories">
                                <div class="worker_digits">
                                    <div class="fact" :class="[planCls(plan.y.acc_plan, fact.y.acc)]" v-text="cur(fact.y.acc)"></div>
                                    <div class="plan" v-text="cur(plan.y.acc_plan)"></div>
                                </div>
                            </div>

                            <!--пакеты ТО-->
                            <div class="col db-col ">
                                <div class="worker_digits">
                                    <div class="fact" :class="[planCls(plan.y.topack_plan, fact.y.topack)]" v-text="fact.y.topack"></div>
                                    <div class="plan" v-text="plan.y.topack_plan"></div>
                                </div>
                            </div>

                            <!--каско-->
                            <div class="col db-col ">
                                <div class="worker_digits">
                                    <div class="fact" :class="[planCls(plan.y.kasko_plan, fact.y.kasko)]" v-text="fact.y.kasko"></div>
                                    <div class="plan" v-text="plan.y.kasko_plan"></div>
                                </div>
                            </div>

                            <!--trade in-->
                            <div class="col db-col ">
                                <div class="worker_digits">
                                    <div class="fact" :class="[planCls(plan.y.tradein_plan, fact.y.tradein)]" v-text="fact.y.tradein"></div>
                                    <div class="plan" v-text="plan.y.tradein_plan"></div>
                                </div>
                            </div>

                            <!--тест-драйв-->
                            <div class="col db-col ">
                                <div class="worker_digits">
                                    <div class="fact" :class="[planCls(plan.y.testdrive_plan, fact.y.testdrive)]" v-text="fact.y.testdrive"></div>
                                    <div class="plan" v-text="plan.y.testdrive_plan"></div>
                                </div>
                            </div>

                            <div class="col db-col ">
                                <div class="worker_digits">
                                    <div class="fact" :class="[planCls(plan.y.ogpo_plan, fact.y.ogpo)]" v-text="fact.y.ogpo"></div>
                                    <div class="plan" v-text="plan.y.ogpo_plan"></div>
                                </div>
                            </div>

                            <div class="col db-col ">
                                <div class="worker_digits">
                                    <div class="fact" :class="[planCls(plan.y.staging_plan, fact.y.staging)]" v-text="fact.y.staging"></div>
                                    <div class="plan" v-text="plan.y.staging_plan"></div>
                                </div>
                            </div>

                            <!--crm-->
                            <div class="col db-col">
                                <div class="worker_digits">
                                    <div class="fact mt-n4" v-text="yPercent+'%'"></div>
                                </div>
                            </div>

                            <div class="progress-line">
                                <div class="top" :style="{width:checkOverplanP(yPercent)?'100%':yPercent+'%', backgroundColor:getColor(yPercent)}"></div>
                                <div class="bottom-wrap">
                                    <div class="bottom" :style="{width:checkOverplanP(yPercent)?'100%':yPercent+'%', backgroundColor:getColor(yPercent)}"></div>
                                </div>
                            </div>

                        </div>

                        <!--месячный план-->
                        <div class="align-items-center col-12 d-flex db-row header justify-content-between no-gutters p-0"
                             style="height: 37.5%">
                            <div class="col d-flex db-col justify-content-between">
                                <div class="current-year d-inline-block pl-4 w-auto" v-text="monthName"></div>
                                <div class="d-inline-block percent pr-4 text-right w-auto"></div>
                            </div>

                            <!--продажи-->
                            <div class="col db-col">
                                <div class="worker_digits">
                                    <div class="fact" :class="[planCls(plan.m.sales_plan, fact.m.sales)]" v-text="fact.m.sales"></div>
                                    <div class="plan" v-text="plan.m.sales_plan"></div>
                                </div>
                            </div>

                            <!--аксессуары-->
                            <div class="col db-col accessories">
                                <div class="worker_digits">
                                    <div class="fact" :class="[planCls(plan.m.acc_plan, fact.m.acc)]" v-text="cur(fact.m.acc)"></div>
                                    <div class="plan" v-text="cur(plan.m.acc_plan)"></div>
                                </div>
                            </div>

                            <!--пакеты ТО-->
                            <div class="col db-col ">
                                <div class="worker_digits">
                                    <div class="fact" :class="[planCls(plan.m.topack_plan, fact.m.topack)]" v-text="fact.m.topack"></div>
                                    <div class="plan" v-text="plan.m.topack_plan"></div>
                                </div>
                            </div>

                            <!--каско-->
                            <div class="col db-col">
                                <div class="worker_digits">
                                    <div class="fact" :class="[planCls(plan.m.kasko_plan, fact.m.kasko)]" v-text="fact.m.kasko"></div>
                                    <div class="plan" v-text="plan.m.kasko_plan"></div>
                                </div>
                            </div>

                            <!--trade in-->
                            <div class="col db-col">
                                <div class="worker_digits">
                                    <div class="fact" :class="[planCls(plan.m.tradein_plan, fact.m.tradein)]" v-text="fact.m.tradein"></div>
                                    <div class="plan" v-text="plan.m.tradein_plan"></div>
                                </div>
                            </div>

                            <!--тест-драйв-->
                            <div class="col db-col">
                                <div class="worker_digits">
                                    <div class="fact" :class="[planCls(plan.m.testdrive_plan, fact.m.testdrive)]" v-text="fact.m.testdrive"></div>
                                    <div class="plan" v-text="plan.m.testdrive_plan"></div>
                                </div>
                            </div>

                            <div class="col db-col">
                                <div class="worker_digits">
                                    <div class="fact" :class="[planCls(plan.m.ogpo_plan, fact.m.ogpo)]" v-text="fact.m.ogpo"></div>
                                    <div class="plan" v-text="plan.m.ogpo_plan"></div>
                                </div>
                            </div>

                            <div class="col db-col">
                                <div class="worker_digits">
                                    <div class="fact" :class="[planCls(plan.m.staging_plan, fact.m.staging)]" v-text="fact.m.staging"></div>
                                    <div class="plan" v-text="plan.m.staging_plan"></div>
                                </div>
                            </div>

                            <!--crm-->
                            <div class="col db-col">
                                <div class="worker_digits">
                                    <div class="fact mt-n4" v-text="monthPercent+'%'"></div>
                                </div>
                            </div>

                            <div class="progress-line">
                                <div class="top"
                                     :style="{width:checkOverplanP(monthPercent)?'100%':monthPercent+'%', backgroundColor:getColor(monthPercent)}"></div>
                                <div class="bottom-wrap">
                                    <div class="bottom"
                                         :style="{width:checkOverplanP(monthPercent)?'100%':monthPercent+'%', backgroundColor:getColor(monthPercent)}"></div>
                                </div>
                            </div>

                        </div>
                    </div>

                    <!--сотрудники-->
                    <transition-group
                        name="flip-list"
                        tag="div"
                        class="align-items-stretch col-12 d-flex flex-wrap p-0 top-rows"
                        v-if="orderedWorkersServiceMech">
                        <div :key="worker.id" v-for="worker,i in orderedWorkersServiceMech" v-if="i<5"
                             class="align-items-center col-12 d-flex db-row justify-content-between no-gutters p-0">
                            <div class="col d-flex db-col justify-content-between worker "
                                 :class="{'best':i===0, 'bad':i===workers.length-1}">
                                <div class="worker-photo d-inline-block w-auto">
                                    <img :src="worker.image" alt="">
                                </div>
                                <div class="align-items-end d-flex flex-column h-75 justify-content-end pr-4 rating w-auto mt-2">
                                    <div class="rating-arrow float-right green" :class="[worker.arrow]"
                                         :data-id="worker.id"></div>
                                    <div class="percent" v-text="worker.total_efficiency+'%'"></div>
                                    <div class="name text-right" v-html="(worker.name).split(' ').join('<br>')"></div>

                                    <!--Stars-->
                                    <div class="starsWrap">
                                        <i class="fa fa-star" :class="{'on':worker.rate>=70}"></i>
                                        <i class="fa fa-star" :class="{'on':worker.rate>=75}"></i>
                                        <i class="fa fa-star" :class="{'on':worker.rate>=80}"></i>
                                        <i class="fa fa-star" :class="{'on':worker.rate>=90}"></i>
                                        <i class="fa fa-star" :class="{'on':worker.rate>=95}"></i>
                                    </div>

                                </div>
                            </div>


                            <!--продажи-->
                            <div class="col db-col">
                                <div class="worker_digits">
                                    <div class="fact" :class="{
                                    'planRed':worker.sales_fact*1 == 0,
                                    'planGreen':worker.sales_fact*1 >= worker.sales_plan*1
                                    }" v-text="worker.sales_fact">
                                    </div>
                                    <div class="plan" v-text="worker.sales_plan">b</div>
                                </div>
                            </div>

                            <!--аксессуары-->
                            <div class="col db-col accessories">
                                <div class="worker_digits">
                                    <div class="fact" :class="{
                                    'planRed':worker.access_fact*1 == 0,
                                    'planGreen':worker.access_fact*1 >= worker.access_plan*1,
                                    }" v-text="cur(worker.access_fact)">
                                    </div>
                                    <div class="plan" v-text="cur(worker.access_plan)"></div>
                                </div>
                            </div>

                            <!--пакеты ТО-->
                            <div class="col db-col">
                                <div class="worker_digits">
                                    <div class="fact" :class="{
                                                                    'planRed':worker.to_pack_fact*1 == 0,
                                                                    'planGreen':worker.to_pack_fact*1 >= worker.to_pack_plan*1
                                                                    }" v-text="worker.to_pack_fact">
                                    </div>
                                    <div class="plan" v-text="worker.to_pack_plan"></div>
                                </div>
                            </div>

                            <!--каско-->
                            <div class="col db-col">
                                <div class="worker_digits">
                                    <div class="fact" :class="{
                                                                    'planRed':worker.kasko_fact*1 == 0,
                                                                    'planGreen':worker.kasko_fact*1 >= worker.kasko_plan*1,
                                                                    }" v-text="worker.kasko_fact">
                                    </div>
                                    <div class="plan" v-text="worker.kasko_plan"></div>
                                </div>
                            </div>

                            <!--trade in-->
                            <div class="col db-col">
                                <div class="worker_digits">
                                    <div class="fact" :class="{
                                                                    'planRed':worker.tradein_fact*1 == 0,
                                                                    'planGreen':worker.tradein_fact*1 >= worker.tradein_plan*1,
                                                                    }" v-text="worker.tradein_fact">
                                    </div>
                                    <div class="plan" v-text="worker.tradein_plan"></div>
                                </div>
                            </div>

                            <!--тест-драйв-->
                            <div class="col db-col">
                                <div class="worker_digits">
                                    <div class="fact" :class="{
                                                                    'planRed':worker.testdrive_fact*1 == 0,
                                                                    'planGreen':worker.testdrive_fact*1 >= worker.testdrive_plan*1,
                                                                    }" v-text="worker.testdrive_fact">
                                    </div>
                                    <div class="plan" v-text="worker.testdrive_plan"></div>
                                </div>
                            </div>

                            <!--ОГПО+Safe-->
                            <div class="col db-col">
                                <div class="worker_digits">
                                    <div class="fact" :class="{
                                                                    'planRed':worker.ogpo_fact*1 == 0,
                                                                    'planGreen':worker.ogpo_fact*1 >= worker.ogpo_plan*1,
                                                                    }" v-text="worker.ogpo_fact">
                                    </div>
                                    <div class="plan" v-text="worker.ogpo_plan"></div>
                                </div>
                            </div>

                            <div class="col db-col">
                                <div class="worker_digits">
                                    <div class="fact" :class="{
                                                                    'planRed':worker.staging_fact*1 == 0,
                                                                    'planGreen':worker.staging_fact*1 >= worker.staging_plan*1,
                                                                    }" v-text="worker.staging_fact">
                                    </div>
                                    <div class="plan" v-text="worker.staging_plan"></div>
                                </div>
                            </div>

                            <!--crm-->
                            <div class="col db-col">
                                <div class="worker_digits">
                                    <div class="fact" v-text="worker.total_percent+'%'"></div>
                                    <div class="plan"></div>
                                </div>
                            </div>

                            <div class="progress-line">
                                <div class="top" :class="{'overplan':checkOverplan(worker)}"
                                     :style="{width:checkOverplan(worker)?'100%':worker.total_percent+'%', backgroundColor:getColor(worker.total_percent)}"></div>
                                <div class="bottom-wrap">
                                    <div class="bottom" :class="{'overplan':checkOverplan(worker)}"
                                         :style="{width:checkOverplan(worker)?'100%':worker.total_percent+'%', backgroundColor:getColor(worker.total_percent)}"></div>
                                </div>
                            </div>
                        </div>
                    </transition-group>


                </div>
            </div>
        </div>
    </div>
</transition>
