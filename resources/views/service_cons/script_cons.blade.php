<script>
window.POOLTIME = 30; //секунд
window.FIRST = 11; //секунд
window.SECOND = 50; //секунд
var app_cons = new Vue({
    el: '#app_cons',
        data: {
        datestr: "",
            hours: 0,
            min: 0,
            activePage: 1,

            workers: false,
            plan: null,
            fact: null,
            yPercent: 0,
            monthPercent: 0,
            recordPercent: 0,

            monthsNamesList: ['Январь', 'Февраль', 'Март', 'Апрель', 'Май', 'Июнь', 'Июль', 'Август', 'Сентябрь', 'Октябрь', 'Ноябрь', 'Декабрь'],
            monthName:"",
            timer: null,
            bests: []
        },
        computed: {
        orderedWorkers: function () {
            return _.orderBy(this.workers, 'total_efficiency', 'desc')
            },
    },
        methods: {
        checkOverplan(worker) {
                if (worker.total_percent > 100) {
                    return true
                }
                return false
            },
            checkOverplanP(p) {
                if (p > 100) {
                    return true
                }
                return false
            },
            getColor(percent) {
            r = percent < 50 ? 255 : Math.floor(255 - (percent * 2 - 100) * 255 / 100);
                g = percent > 50 ? 255 : Math.floor((percent * 2) * 255 / 100);
                return 'rgb(' + r + ',' + g + ',0)';
            },
            planCls(plan, fact) {
                if (fact === 0) {
                    return "planRed"
                }
                if(fact === plan){
                    return "planGreen"
                }
                return "";
            },
            getDataServiceCons() {
            axios.get("/data/service_cons").then(res => {
                console.log(res);
                this.workers = res.data.workers
                this.plan = res.data.plan
                this.fact = res.data.fact
                this.yPercent = res.data.yPercent
                this.monthPercent = res.data.monthPercent
                this.recordPercent = res.data.recordPercent
                this.bests = res.data.bests
                let vm = this
                if (!this.timer) {
                    this.timer = setTimeout(function () {
                        vm.showSecond()
                    }, window.FIRST * 100)
                }
                })


            },
            showSecond() {
            let vm = this
                this.activePage = 1
                setTimeout(function () {
                    vm.showFirst()
                }, window.SECOND * 1000)
            },
            showFirst(){
            let vm = this
                this.activePage = 2
                setTimeout(function () {
                    vm.showSecond()
                }, window.FIRST * 1000)
            },
            cur(c) {
                return numeral(c).format('0,0')
            },
        },
        mounted() {
        let vm = this
            vm.getDataServiceCons()
            var newDate = new Date();
            newDate.setDate(newDate.getDate());
            this.monthName = this.monthsNamesList[newDate.getMonth()]
            setInterval(function () {
                vm.getDataServiceCons()
            }, window.POOLTIME * 1000)
        }
    })


    /* Date Time */
    var monthNames = ["янв", "фев", "мар", "апр", "мая", "июн", "июл", "авг", "сен", "окт", "ноя", "дек"];
    var dayNames = ["вс", "пн", "вт", "ср", "чт", "пт", "сб"]
    var newDate = new Date();



    setInterval(function () {
        app_cons.datestr = newDate.getDate() + ' ' + monthNames[newDate.getMonth()] + ', ' + dayNames[newDate.getDay()];
        var hours = new Date().getUTCHours() + 5;
        app_cons.hours = (hours < 10 ? "0" : "") + hours;
        var minutes = new Date().getMinutes();
        app_cons.min = (minutes < 10 ? "0" : "") + minutes;
    }, 1000);
    /* Date Time */

</script>

