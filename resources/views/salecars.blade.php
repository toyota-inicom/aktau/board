<transition name="fade" v-if="workersBu">
    <div class="container-fluid" v-show="activePage === 2">
        <div class="row">
            <div class="1st-page" style="opacity: 1">
                <div class="align-items-stretch col-12 d-flex dashboard-wrap flex-wrap p-0">

                    <!--заголовки и общие показатели-->
                    <div class="align-items-stretch col-12 d-flex flex-wrap p-0 top-rows">
                        <!--заголовок-->
                        <div class="align-items-center col-12 d-flex db-row header justify-content-between no-gutters p-0"
                             style="height: 25%">
                            <div class="col db-col header">
                                <!--дата время-->
                                <div class="align-items-center d-flex data-time p-3">
                                    <div class="logo col-6 text-left">
                                        <img src="/images/logo.png" alt="">
                                    </div>
                                    <div class="dt col-6 text-right">
                                        <div class="clock">
                                            <div class="time d-flex justify-content-end">
                                                <span id="hours" v-text="hours"></span>
                                                <span id="point">:</span>
                                                <span id="min" v-text="min"></span>
                                            </div>
                                            <div class="data" id="Date" v-text="datestr"></div>
                                        </div>
                                    </div>
                                </div>
                            </div>
                            <div class="col db-col title">Продажи</div>
                            <div class="col db-col title">Аксессуары</div>
                            <div class="col db-col title">Пакеты ТО</div>
                            <div class="col db-col title">КАСКО</div>
                            <div class="col db-col title">ОГПО + Safe</div>
                            <div class="col db-col title">Trade-In + Выкуп</div>
                            <div class="col db-col title">План</div>
                        </div>

                        <!--годовой план-->
                        <div class="align-items-center col-12 d-flex db-row header justify-content-between no-gutters p-0"
                             style="height: 37.5%">
                            <div class="col d-flex db-col justify-content-between">
                                <div class="current-year d-inline-block pl-4 w-auto">{{date('Y')}}
                                    <small>год</small>
                                </div>
                                <div class="d-inline-block percent pr-4 text-right w-auto">
                                        <span class="record"
                                              style="font-size: 33px;color: rgb(48, 215, 18);line-height: 33px;text-shadow: 0 0 14px;">
                                            <em style="display: block;font-size: 20px;font-style: normal;color: rgb(48, 215, 18);line-height: 20px;margin: 0 0 -3px 0;">Рекорд</em>
                                            <span v-text="recordPercentBu+'%'"></span>
                                        </span>
                                </div>
                            </div>


                            <div class="col db-col ">
                                <div class="worker_digits">
                                    <div class="fact" :class="[planCls(planBu.y.sales_plan, factBu.y.sales)]" v-text="factBu.y.sales"></div>
                                    <div class="plan" v-text="planBu.y.sales_plan"></div>
                                </div>
                            </div>
                            <!--аксессуары-->
                            <div class="col db-col accessories">
                                <div class="worker_digits">
                                    <div class="fact" :class="[planCls(planBu.y.acc_plan, factBu.y.acc)]" v-text="cur(factBu.y.acc)"></div>
                                    <div class="plan" v-text="cur(planBu.y.acc_plan)"></div>
                                </div>
                            </div>

                            <!--пакеты ТО-->
                            <div class="col db-col ">
                                <div class="worker_digits">
                                    <div class="fact" :class="[planCls(planBu.y.topack_plan, factBu.y.topack)]" v-text="factBu.y.topack"></div>
                                    <div class="plan" v-text="planBu.y.topack_plan"></div>
                                </div>
                            </div>

                            <!--каско-->
                            <div class="col db-col ">
                                <div class="worker_digits">
                                    <div class="fact" :class="[planCls(planBu.y.kasko_plan, factBu.y.kasko)]" v-text="factBu.y.kasko"></div>
                                    <div class="plan" v-text="planBu.y.kasko_plan"></div>
                                </div>
                            </div>



                            <div class="col db-col ">
                                <div class="worker_digits">
                                    <div class="fact" :class="[planCls(planBu.y.ogpo_plan, factBu.y.ogpo)]" v-text="factBu.y.ogpo"></div>
                                    <div class="plan" v-text="planBu.y.ogpo_plan"></div>
                                </div>
                            </div>

                            <div class="col db-col ">
                                <div class="worker_digits">
                                    <div class="fact" v-text="factBu.y.staging"></div>
                                    <div class="plan">&nbsp</div>
                                </div>
                            </div>
                            <!--crm-->
                            <div class="col db-col">
                                <div class="worker_digits">
                                    <div class="fact" v-text="yPercentBu+'%'"></div>
                                    <div class="plan">&nbsp</div>
                                </div>
                            </div>

                            <div class="progress-line">
                                <div class="top" :style="{width:checkOverplanP(yPercentBu)?'100%':yPercentBu+'%', backgroundColor:getColor(yPercentBu)}"></div>
                                <div class="bottom-wrap">
                                    <div class="bottom" :style="{width:checkOverplanP(yPercentBu)?'100%':yPercentBu+'%', backgroundColor:getColor(yPercentBu)}"></div>
                                </div>
                            </div>

                        </div>

                        <!--месячный план-->
                        <div class="align-items-center col-12 d-flex db-row header justify-content-between no-gutters p-0"
                             style="height: 37.5%">
                            <div class="col d-flex db-col justify-content-between">
                                <div class="current-year d-inline-block pl-4 w-auto" v-text="monthName"></div>
                                <div class="d-inline-block percent pr-4 text-right w-auto"></div>
                            </div>

                            <!--аксессуары-->
                            <div class="col db-col">
                                <div class="worker_digits">
                                    <div class="fact" :class="[planCls(planBu.m.sales_plan, factBu.m.sales)]" v-text="factBu.m.sales"></div>
                                    <div class="plan" v-text="planBu.m.sales_plan"></div>
                                </div>
                            </div>

                            <!--аксессуары-->
                            <div class="col db-col accessories">
                                <div class="worker_digits">
                                    <div class="fact" :class="[planCls(planBu.m.acc_plan, factBu.m.acc)]" v-text="cur(factBu.m.acc)"></div>
                                    <div class="plan" v-text="cur(planBu.m.acc_plan)"></div>
                                </div>
                            </div>

                            <!--пакеты ТО-->
                            <div class="col db-col ">
                                <div class="worker_digits">
                                    <div class="fact" :class="[planCls(planBu.m.topack_plan, factBu.m.topack)]" v-text="factBu.m.topack"></div>
                                    <div class="plan" v-text="planBu.m.topack_plan"></div>
                                </div>
                            </div>

                            <!--каско-->
                            <div class="col db-col">
                                <div class="worker_digits">
                                    <div class="fact" :class="[planCls(planBu.m.kasko_plan, factBu.m.kasko)]" v-text="factBu.m.kasko"></div>
                                    <div class="plan" v-text="planBu.m.kasko_plan"></div>
                                </div>
                            </div>


                            <div class="col db-col">
                                <div class="worker_digits">
                                    <div class="fact" :class="[planCls(planBu.m.ogpo_plan, factBu.m.ogpo)]" v-text="factBu.m.ogpo"></div>
                                    <div class="plan" v-text="planBu.m.ogpo_plan"></div>
                                </div>
                            </div>

                            <div class="col db-col">
                                <div class="worker_digits">
                                    <div class="fact" v-text="factBu.m.staging"></div>
                                    <div class="plan">&nbsp</div>
                                </div>
                            </div>

                            <!--crm-->
                            <div class="col db-col">
                                <div class="worker_digits">
                                    <div class="fact" v-text="monthPercentBu+'%'"></div>
                                    <div class="plan">&nbsp</div>
                                </div>
                            </div>

                            <div class="progress-line">
                                <div class="top"
                                     :style="{width:checkOverplanP(monthPercentBu)?'100%':monthPercentBu+'%', backgroundColor:getColor(monthPercentBu)}"></div>
                                <div class="bottom-wrap">
                                    <div class="bottom"
                                         :style="{width:checkOverplanP(monthPercentBu)?'100%':monthPercentBu+'%', backgroundColor:getColor(monthPercentBu)}"></div>
                                </div>
                            </div>

                        </div>
                    </div>

                    <!--сотрудники-->
                    <transition-group
                            name="flip-list"
                            tag="div"
                            class="align-items-stretch col-12 d-flex flex-wrap p-0 top-rows"
                            v-if="orderedWorkersBu">
                        <div :key="worker.id" v-for="worker,i in orderedWorkersBu" v-if="i<5"
                             class="align-items-center col-12 d-flex db-row justify-content-between no-gutters p-0">
                            <div class="col d-flex db-col justify-content-between worker "
                                 :class="{'best':i===0, 'bad':i===workers.length-1}">
                                <div class="worker-photo d-inline-block w-auto">
                                    <img :src="worker.image" alt="">
                                </div>
                                <div class="align-items-end d-flex flex-column h-75 justify-content-end pr-4 rating w-auto mt-2">
                                    <div class="rating-arrow float-right green" :class="[worker.arrow]"
                                         :data-id="worker.id"></div>
                                    <div class="percent" v-text="worker.total_efficiency+'%'"></div>
                                    <div class="name text-right" v-html="(worker.name).split(' ').join('<br>')"></div>

                                    <!--Stars-->
                                    <div class="starsWrap">
                                        <i class="fa fa-star" :class="{'on':worker.rate>=70}"></i>
                                        <i class="fa fa-star" :class="{'on':worker.rate>=75}"></i>
                                        <i class="fa fa-star" :class="{'on':worker.rate>=80}"></i>
                                        <i class="fa fa-star" :class="{'on':worker.rate>=90}"></i>
                                        <i class="fa fa-star" :class="{'on':worker.rate>=95}"></i>
                                    </div>

                                </div>
                            </div>


                            <!--аксессуары-->
                            <div class="col db-col">
                                <div class="worker_digits">
                                    <div class="fact" :class="{
                                                                    'planRed':worker.sales_fact*1 == 0,
                                                                    'planGreen':worker.sales_fact*1 >= worker.sales_plan*1,
                                                                    }" v-text="cur(worker.sales_fact)">
                                    </div>
                                    <div class="plan" v-text="cur(worker.sales_plan)"></div>
                                </div>
                            </div>


                            <!--аксессуары-->
                            <div class="col db-col accessories">
                                <div class="worker_digits">
                                    <div class="fact" :class="{
                                                                   'planRed':worker.acc_fact*1 == 0,
                                                                   'planGreen':worker.acc_fact*1 >= worker.acc_plan*1,
                                                                   }" v-text="cur(worker.acc_fact)">
                                    </div>
                                    <div class="plan" v-text="cur(worker.acc_plan)"></div>
                                </div>
                            </div>

                            <!--пакеты ТО-->
                            <div class="col db-col">
                                <div class="worker_digits">
                                    <div class="fact" :class="{
                                                                   'planRed':worker.topack_fact*1 == 0,
                                                                   'planGreen':worker.topack_fact*1 >= worker.topack_plan*1
                                                                   }" v-text="worker.topack_fact">
                                    </div>
                                    <div class="plan" v-text="worker.topack_plan"></div>
                                </div>
                            </div>

                            <!--каско-->
                            <div class="col db-col">
                                <div class="worker_digits">
                                    <div class="fact" :class="{
                                                                   'planRed':worker.kasko_fact*1 == 0,
                                                                   'planGreen':worker.kasko_fact*1 >= worker.kasko_plan*1,
                                                                   }" v-text="worker.kasko_fact">
                                    </div>
                                    <div class="plan" v-text="worker.kasko_plan"></div>
                                </div>
                            </div>


                            <div class="col db-col">
                                <div class="worker_digits">
                                    <div class="fact" :class="{
                                                                    'planRed':worker.ogpo_fact*1 == 0,
                                                                    'planGreen':worker.ogpo_fact*1 >= worker.ogpo_plan*1,
                                                                    }" v-text="worker.ogpo_fact">
                                    </div>
                                    <div class="plan" v-text="worker.ogpo_plan"></div>
                                </div>
                            </div>

                            <div class="col db-col">
                                <div class="worker_digits">
                                    <div class="fact" class="planGreen" v-text="worker.staging_fact">
                                    </div>
                                    <div class="plan">&nbsp</div>
                                </div>
                            </div>

                            <!--crm-->
                            <div class="col db-col">
                                <div class="worker_digits">
                                    <div class="fact" v-text="worker.total_percent+'%'"></div>
                                    <div class="plan">&nbsp</div>
                                </div>
                            </div>

                            <div class="progress-line">
                                <div class="top" :class="{'overplan':checkOverplan(worker)}"
                                     :style="{width:checkOverplan(worker)?'100%':worker.total_percent+'%', backgroundColor:getColor(worker.total_percent)}"></div>
                                <div class="bottom-wrap">
                                    <div class="bottom" :class="{'overplan':checkOverplan(worker)}"
                                         :style="{width:checkOverplan(worker)?'100%':worker.total_percent+'%', backgroundColor:getColor(worker.total_percent)}"></div>
                                </div>
                            </div>
                        </div>
                    </transition-group>


                </div>
            </div>
        </div>
    </div>
</transition>
