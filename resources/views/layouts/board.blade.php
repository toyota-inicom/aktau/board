<!DOCTYPE html>
<html lang="{{ str_replace('_', '-', app()->getLocale()) }}">
<head>
    <meta charset="utf-8">
    <meta name="viewport" content="width=device-width, initial-scale=1, shrink-to-fit=no">
    <link rel="stylesheet" href="{{asset('fonts/stylesheet.css')}}">
    <link rel="stylesheet" href="{{asset('css/app.css')}}">
	<meta name="google" content="notranslate" />

	<meta http-equiv="content-language" content="ru">


    <title>ДЕПАРТАМЕНТ ПРОДАЖ</title>
    <style>
        .flip-list-move {
            transition: transform 3s;
        }

        .fade-enter-active, .fade-leave-active {
            transition: opacity 1s;
        }

        .fade-enter, .fade-leave-to {
            opacity: 0;
        }

        .starsWrap .fa-star.on {
            color: #ffd500;
        }

        .planRed {
            color: #cc0907 !important;
        }

        .planGreen {
            color: #0fa732 !important;
        }
    </style>
</head>
<body style="overflow: hidden; position: absolute; width: 100%; height: 100%;">
<div id="app">
    @yield('content')
</div>
<script src="js/app.js?t=<?php echo time(); ?>"></script>
<script src="https://cdn.jsdelivr.net/npm/vue/dist/vue.js"></script>
<script src="https://cdnjs.cloudflare.com/ajax/libs/axios/0.19.2/axios.min.js" integrity="sha256-T/f7Sju1ZfNNfBh7skWn0idlCBcI3RwdLSS4/I7NQKQ=" crossorigin="anonymous"></script>
<script src="https://cdnjs.cloudflare.com/ajax/libs/lodash.js/4.14.1/lodash.min.js"></script>
<script src="//cdnjs.cloudflare.com/ajax/libs/numeral.js/2.0.6/numeral.min.js"></script>
<script src="https://cdnjs.cloudflare.com/ajax/libs/jquery/3.5.1/jquery.min.js" ></script>
@include('script')

</body>
</html>
