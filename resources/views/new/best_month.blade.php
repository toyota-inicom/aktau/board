<transition v-if="Object.keys(bests).length" name="fade">
    <div class="container-fluid" v-show="activePage === 2">
        <div class="row">
            <div class="dashboard-wrap">

                <div class="best_header_wrap d-flex justify-content-between w-100">
                    <div class="best_h1 text-center w-100 animate__animated animate__backInDown mt-5 text-uppercase" v-text="'ЛУЧШИЙ ПО ИТОГАМ ЗА '+monthName">
                    </div>
                </div>

                <div class="align-items-center best-wrap d-flex justify-content-center">

                    <div
                        class="text-center worker-best best_1 d-flex align-items-center flex-column justify-content-end animate__animated animate__backInUp"
                        style="width: 33%;">
                        <div class="best_department animate__animated animate__backInDown">
                            Отдел продаж<br>новых автомобилей
                        </div>
                        <div class="w-photo">
                            <img :src="bests.new.images[2]" alt="" class="animate__animated animate__fadeIn">
                            <div class="best-bg-radius"></div>
                        </div>
                        <div class="w-name">
                            <span class="animate__animated animate__bounceIn" v-text="bests.new.name"></span>
                        </div>
                        <div class="w-percent animate__animated animate__backInUp" v-text="bests.new.fields.eff + '%'"></div>
                    </div>

                </div>
            </div>
        </div>
    </div>
</transition>
