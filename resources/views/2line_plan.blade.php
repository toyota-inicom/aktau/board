<td class="" style="background-color: rgb(209, 224, 238, .3);">
    <div class="d-flex flex-column">
        <div class="col-12">
            <small class="form-text text-center text-muted">{{ $title }} план</small>
            <input type="number" class="form-control" wire:model="users.{{$i}}.fields.{{ $planField }}">
        </div>
        <div class="col-12">
            <small class="form-text  text-center text-muted">{{ $title }} факт</small>
            <input type="number" class="form-control" wire:model="users.{{$i}}.fields.{{ $factField }}">
        </div>
    </div>
</td>
