<script>
    window.POOLTIME = 30; //секунд
    window.FIRST = 10; //секунд
    window.SECOND = 10; //секунд
    window.THIRD = 10; //секунд
    window.FOURS = 10; //секунд
    window.FIVE = 10; //секунд
    window.SIX = 10; //секунд
    var app = new Vue({
        el: '#app',
        data: {
            datestr: "",
            hours: 0,
            min: 0,
            activePage: 1,
            workers: false,
            plan: null,
            fact: null,
            yPercent: 0,
            monthPercent: 0,
            recordPercent: 0,
            workersBu: false,
            planBu: null,
            factBu: null,
            yPercentBu: 0,
            monthPercentBu: 0,
            recordPercentBu: 0,

            workersBuy: false,
            planBuy: null,
            factBuy: null,
            yPercentBuy: 0,
            monthPercentBuy: 0,
            recordPercentBuy: 0,

            workersServiceCons: false,
            planServiceCons: null,
            factServiceCons: null,
            yPercentServiceCons: 0,
            monthPercentServiceCons: 0,
            recordPercentServiceCons: 0,

            workersServiceMech: false,
            planServiceMech: null,
            factServiceMech: null,
            yPercentServiceMech: 0,
            monthPercentServiceMech: 0,
            recordPercentServiceMech: 0,

            monthsNamesList: ['Январь', 'Февраль', 'Март', 'Апрель', 'Май', 'Июнь', 'Июль', 'Август', 'Сентябрь', 'Октябрь', 'Ноябрь', 'Декабрь'],
            monthName:"",
            timer: null,
            bests: []
        },
        computed: {
            orderedWorkers: function () {
                return _.orderBy(this.workers, 'total_efficiency', 'desc')
            },
            orderedWorkersBu: function () {
                return _.orderBy(this.workersBu, 'total_efficiency', 'desc')
            },
            orderedWorkersBuy: function () {
                return _.orderBy(this.workersBuy, 'total_efficiency', 'desc')
            },
            orderedWorkersServiceCons: function () {
                return _.orderBy(this.workersServiceCons, 'total_efficiency', 'desc')
            },
            orderedWorkersServiceMech: function () {
                return _.orderBy(this.workersServiceMech, 'total_efficiency', 'desc')
            },
        },
        methods: {
            checkOverplan(worker) {
                if (worker.total_percent > 100) {
                    return true
                }
                return false
            },
            checkOverplanP(p) {
                if (p > 100) {
                    return true
                }
                return false
            },
            getColor(percent) {
                r = percent < 50 ? 255 : Math.floor(255 - (percent * 2 - 100) * 255 / 100);
                g = percent > 50 ? 255 : Math.floor((percent * 2) * 255 / 100);
                return 'rgb(' + r + ',' + g + ',0)';
            },
            planCls(plan, fact) {
                if (fact === 0) {
                    return "planRed"
                }
                if(fact === plan){
                    return "planGreen"
                }
                return "";
            },
            getData() {
                axios.get("/data/opna").then(res => {
                    this.workers = res.data.workers
                    this.plan = res.data.plan
                    this.fact = res.data.fact
                    this.yPercent = res.data.yPercent
                    this.monthPercent = res.data.monthPercent
                    this.recordPercent = res.data.recordPercent
                    this.bests = res.data.bests
                })
            },
            getDataBu() {
                axios.get("/data/opasp").then(res => {
                    this.workersBu = res.data.workers
                    this.planBu = res.data.plan
                    this.factBu = res.data.fact
                    this.yPercentBu = res.data.yPercent
                    this.monthPercentBu = res.data.monthPercent
                    this.recordPercentBu = res.data.recordPercent
                    let vm = this
                    if (!this.timer) {
                        this.timer = setTimeout(function () {
                            vm.showSecond()
                        }, window.FIRST * 100)
                    }

                })
            },
            getDataBuy() {
                axios.get("/data/buy").then(res => {
                    this.workersBuy = res.data.workers
                    this.planBuy = res.data.plan
                    this.factBuy = res.data.fact
                    this.yPercentBuy = res.data.yPercent
                    this.monthPercentBuy = res.data.monthPercent
                    this.recordPercentBuy = res.data.recordPercent
                })
            },
            getDataServiceCons() {
                axios.get("/data/service_cons").then(res => {
                    this.workersServiceCons = res.data.workers
                    this.planServiceCons = res.data.plan
                    this.factServiceCons = res.data.fact
                    this.yPercentServiceCons = res.data.yPercent
                    this.monthPercentServiceCons = res.data.monthPercent
                    this.recordPercentServiceCons = res.data.recordPercent
                })
            },
            getDataServiceMech() {
                axios.get("/data/service_mech").then(res => {
                    this.workersServiceMech = res.data.workers
                    this.planServiceMech = res.data.plan
                    this.factServiceMech = res.data.fact
                    this.yPercentServiceMech = res.data.yPercent
                    this.monthPercentServiceMech = res.data.monthPercent
                    this.recordPercentServiceMech = res.data.recordPercent
                })
            },
            showSecond() {
                let vm = this
                this.activePage = 2
                setTimeout(function () {
                    vm.showThird()
                }, window.SECOND * 100)
            },
            showThird() {
                let vm = this
                this.activePage = 3
                setTimeout(function () {
                    vm.showFour()
                }, window.THIRD * 1000)
            },
            showFour() {
                let vm = this
                this.activePage = 4
                setTimeout(function () {
                    vm.showFive()
                }, window.FOURS * 1000)
            },
            showFive() {
                let vm = this
                this.activePage = 5
                setTimeout(function () {
                    vm.showSix()
                }, window.FIVE * 1000)
            },
            showSix() {
                let vm = this
                this.activePage = 6
                setTimeout(function () {
                    vm.showFirst()
                }, window.SIX * 1000)
            },
            showFirst(){
                let vm = this
                this.activePage = 1
                setTimeout(function () {
                    vm.showSecond()
                }, window.FIRST * 1000)
            },
            cur(c) {
                return numeral(c).format('0,0')
            },
        },
        mounted() {
            let vm = this
            vm.getData()
            vm.getDataBu()
            vm.getDataBuy()
            vm.getDataServiceCons()
            vm.getDataServiceMech()
            var newDate = new Date();
            newDate.setDate(newDate.getDate());
            this.monthName = this.monthsNamesList[newDate.getMonth()]
            setInterval(function () {
                vm.getData()
                vm.getDataBu()
                vm.getDataBuy()
                vm.getDataServiceCons()
                vm.getDataServiceMech()
            }, window.POOLTIME * 1000)
        }
    })


    /* Date Time */
    var monthNames = ["янв", "фев", "мар", "апр", "мая", "июн", "июл", "авг", "сен", "окт", "ноя", "дек"];
    var dayNames = ["вс", "пн", "вт", "ср", "чт", "пт", "сб"]
    var newDate = new Date();



    setInterval(function () {
        app.datestr = newDate.getDate() + ' ' + monthNames[newDate.getMonth()] + ', ' + dayNames[newDate.getDay()];
        var hours = new Date().getUTCHours() + 6;
        app.hours = (hours < 10 ? "0" : "") + hours;
        var minutes = new Date().getMinutes();
        app.min = (minutes < 10 ? "0" : "") + minutes;
    }, 1000);
    /* Date Time */

</script>
