<?php

use App\Http\Controllers\ServiceConsController;
use App\Http\Controllers\ServiceMechController;
use Illuminate\Support\Facades\Http;
use Illuminate\Support\Facades\Route;
use App\Http\Controllers\ImportController;
use App\Http\Controllers\OpnaController;
use App\Http\Controllers\OpaspController;
use App\Http\Controllers\BuyController;



Route::get('api/get/cron', [\App\Http\Controllers\CronController::class, 'getCron']);

//Route::get('api/get/service_cons', [\App\Http\Controllers\CronController::class, 'getServiceConsultant']);
//Route::get('api/get/service_mech', [\App\Http\Controllers\CronController::class, 'getServiceWorkshop']);
//Route::get('api/get/opna', [\App\Http\Controllers\CronController::class, 'getOpna']);
//Route::get('api/get/opasp', [\App\Http\Controllers\CronController::class, 'getOpasp']);

Route::post('/api/1c.php', [ImportController::class, "import"]);
Route::post('/api/testdrive', [ImportController::class, "importTestDrive"]);



Route::get('/test', [BuyController::class, "gUpAll"]);


Route::post('/login', [\App\Http\Controllers\AuthController::class, 'login'])->name('login.auth');
Route::get('/login', function () {
    return view('login');
})->name('login');
Route::get('/data/opna', [OpnaController::class, "get"]);
Route::get('/data/opasp', [OpaspController::class, "get"]);
Route::get('/data/buy', [BuyController::class, "get"]);
Route::get('/data/service_cons', [ServiceConsController::class, "get"]);
Route::get('/data/service_mech', [ServiceMechController::class, "get"]);
Route::middleware(['auth'])->group(function () {

    Route::get('/admin', \App\Http\Livewire\Admin\Users::class)->name("admin.users");
    Route::get('/admin/newplan', \App\Http\Livewire\Admin\NewPlan::class)->name("admin.plan.opna");
    Route::get('/admin/buplan', \App\Http\Livewire\Admin\BuPlan::class)->name("admin.plan.bu");
    Route::get('/admin/buyplan', \App\Http\Livewire\Admin\BuyPlan::class)->name("admin.plan.buy");
    Route::get('/admin/service_cons', \App\Http\Livewire\Admin\ServiceConsPlan::class)->name("admin.plan.service_cons");
    Route::get('/admin/service_mech', \App\Http\Livewire\Admin\ServiceMechPlan::class)->name("admin.plan.service_mech");

    /* 1. Доска отдела продаж новых автомобилей */
    Route::get('/', function () {
        return redirect()->route('service_consultant');
    })->name('service_consultant');

    Route::get('/service', function () {
        return view('service_cons.board_cons');
    })->name('service_consultant');

    Route::get('/service-mech', function () {
        return view('service_mech.board');
    })->name('service_workshop');

    Route::get('/opna', function () {
        return view('new.board');
    })->name('opna');

    Route::get('/opasp', function () {
        return view('opasp.board');
    })->name('opasp');
});


///* 2. Доска отдела продаж автомобилей с пробегом */
//Route::get('/2', function () {
//    return view('salecars');
//});
//
///* 3. Доска отдела выкупа автомобилей с пробегом */
//Route::get('/3', function () {
//    return view('buycars');
//});
//
///* 4. Доска с лучшими из отделов по итогам прошлого месяца */
//Route::get('/4', function () {
//    return view('bestmonth');
//});
