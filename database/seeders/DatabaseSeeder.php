<?php

namespace Database\Seeders;

use App\Models\User;
use Illuminate\Database\Seeder;

class DatabaseSeeder extends Seeder
{
    /**
     * Seed the application's database.
     *
     * @return void
     */
    public function run()
    {
        $seed = [
            -1   => ['admin', ''],
            350  => ['Рамазанов Рустам', 'new'],
            91   => ['Утешев Даулет', 'new'],
            100  => ['Яковенко Евгений', 'new'],
            266  => ['Пюрко Андрей', 'new'],
            349  => ['Кусанов Руслан', 'new'],
            242  => ['Мантулин Артём', 'bu'],
            397  => ['Рязанов Дмитрий', 'bu'],
            424  => ['Павленок Михаил', 'bu'],
            403  => ['Сакау Тасболат', 'bu'],
            173  => ['Манько Денис', 'buy'],
            376  => ['Сальмурзин Амир', 'buy'],
            354  => ['Полякова Марина', 'new_add'],
            386  => ['Дюсембаев Каршыга', 'new_add'],
            88   => ['Бакаушина Регина', 'new_add'],
            395  => ['Усенов Аблайхан', 'new'],
            112  => ['Лысенко Владимир', 'bu_add'],
            427  => ['Богомолов Андрей', 'buy'],

        ];

        foreach ($seed as $id => $u) {
            if ($u[0] == "admin") {
                $user = User::updateOrCreate([
                    'ext_id' => $id
                ], [
                    'name'     => $u[0],
                    'dep'      => $u[1],
                    'password' => \Hash::make("iWmn8j")
                ]);
            } else {
                $user = User::updateOrCreate([
                    'ext_id' => $id
                ], [
                    'name' => $u[0],
                    'dep'  => $u[1]
                ]);
                $user->fields()->firstOrCreate();
            }

            dump(str_slug($user->name));
        }


    }
}
