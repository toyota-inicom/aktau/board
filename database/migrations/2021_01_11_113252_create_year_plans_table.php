<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class CreateYearPlansTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('plans', function (Blueprint $table) {
            $table->id();
            $table->string('type');
            $table->string('dep');
            $table->integer('year')->default((int)date('Y'));
            $table->integer('month')->default((int)date('m'));
            $table->integer("tradein_plan")->default(0);
            $table->integer("testdrive_plan")->default(0);
            $table->integer("sales_plan")->default(0);
            $table->integer("acc_plan")->default(0);
            $table->integer("topack_plan")->default(0);
            $table->integer("kasko_plan")->default(0);
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('year_plans');
    }
}
