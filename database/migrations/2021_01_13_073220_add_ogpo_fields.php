<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class AddOgpoFields extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::table("fields", function (Blueprint $table) {
            $table->integer('ogpo_fact')->default(0);
            $table->integer('ogpo_plan')->default(0);
            $table->integer('staging_fact')->default(0);
            $table->integer('staging_plan')->default(0);
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::table("fields", function (Blueprint $table) {
            $table->dropColumn('ogpo_fact');
            $table->dropColumn('ogpo_plan');
            $table->dropColumn('staging_fact');
            $table->dropColumn('staging_plan');
        });
    }
}
