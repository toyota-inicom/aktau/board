<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class AddPlanSaleC extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::table("plans", function (Blueprint $table) {
            $table->integer('aspsale_plan')->default(0);
            $table->integer('asprequest_plan')->default(0);
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::table("plans", function (Blueprint $table) {
            $table->dropColumn('aspsale_plan');
            $table->dropColumn('asprequest_plan');
        });
    }
}
