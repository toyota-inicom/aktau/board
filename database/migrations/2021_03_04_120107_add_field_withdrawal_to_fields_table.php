<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class AddFieldWithdrawalToFieldsTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::table('fields', function (Blueprint $table) {
            $table->integer('withdrawal_fact')->default(0);
            $table->integer('withdrawal_plan')->default(0);
        });
        Schema::table('plans', function (Blueprint $table) {
            $table->integer('withdrawal_plan')->default(0);
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::table('fields', function (Blueprint $table) {
            $table->dropColumn('withdrawal_fact');
            $table->dropColumn('withdrawal_plan');
        });

        Schema::table('plans', function (Blueprint $table) {
            $table->dropColumn('withdrawal_plan');
        });
    }
}
