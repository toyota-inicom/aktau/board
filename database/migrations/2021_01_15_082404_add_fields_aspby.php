<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class AddFieldsAspby extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::table("fields", function (Blueprint $table) {
            $table->integer('aspsale_fact')->default(0);
            $table->integer('aspsale_plan')->default(0);
            $table->integer('asprequest_fact')->default(0);
            $table->integer('asprequest_plan')->default(0);
            $table->integer('standarts')->default(0);
            $table->integer('crm')->default(0);
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::table("fields", function (Blueprint $table) {
            $table->dropColumn('aspsale_fact');
            $table->dropColumn('asprequest_fact');
            $table->dropColumn('aspsale_plan');
            $table->dropColumn('asprequest_plan');
            $table->dropColumn('standarts');
            $table->dropColumn('crm');
        });
    }
}
