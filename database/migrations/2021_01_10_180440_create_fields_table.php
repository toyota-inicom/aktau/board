<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class CreateFieldsTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('fields', function (Blueprint $table) {
            $table->id();
            $table->integer("user_id");
            $table->integer("month")->default((int)date('m'));
            $table->integer("trafic_fact")->default(0);
            $table->integer("tradein_fact")->default(0);
            $table->integer("tradein_plan")->default(0);
            $table->integer("testdrive_fact")->default(0);
            $table->integer("testdrive_plan")->default(0);
            $table->integer("sales_fact")->default(0);
            $table->integer("sales_plan")->default(0);
            $table->integer("acc_fact")->default(0);
            $table->integer("acc_plan")->default(0);
            $table->integer("topack_fact")->default(0);
            $table->integer("topack_plan")->default(0);
            $table->integer("kasko_fact")->default(0);
            $table->integer("kasko_plan")->default(0);
            $table->integer("redemption_fact")->default(0);
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('fields');
    }
}
