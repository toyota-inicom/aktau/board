const mix = require('laravel-mix');

mix.js('resources/js/app.js', 'public/js')
    .sass('resources/sass/app.scss', 'public/css')
    .options({
        processCssUrls: false
    })
    .browserSync({
        proxy: 'board-aktau.loc',
        files: [
            'resources/js/*.js',
            'resources/sass/*.scss',
            'resources/views/*.*'
        ]
    });

