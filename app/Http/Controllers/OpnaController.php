<?php

namespace App\Http\Controllers;

use App\Http\Controllers\GoogleTable;
use App\Models\Fields;
use App\Models\Plan;
use App\Models\User;
use Google_Service_Sheets;
use Illuminate\Support\Collection;

class OpnaController extends Controller
{
    public function percent($from, $to)
    {
        if ((int)$to) {
            return round((100 * $from) / $to);
        }
        return 0;
    }

    public function countBests(User $user)
    {
        return $user->all_fields->where('best', true)->count();
    }

    public function get($fsName = 'fields')
    {
        $workers = User::whereActive(true)
            ->where('dep', 'new')
            ->with($fsName, 'year_fields')
            ->get();

        foreach ($workers as $worker) {
            $worker->fields()->firstOrCreate();
            $worker->load($fsName);
        }

        $workerList = collect();

        foreach ($workers as $worker) {
            $output_workers = collect();
            $fs = $worker->{$fsName};

            if (!$fs) continue;

            /*$totalPrcFields = collect([
                $this->percent($fs->sales_fact, $fs->sales_plan),
                $this->percent($fs->acc_fact, $fs->acc_plan),
                $this->percent($fs->cr_fact, $fs->cr_plan),
                $this->percent($fs->tradein_fact, $fs->tradein_plan),
            ]);*/

            $totalPrcFields = collect([
                round(($fs->sales_fact * 40) / $fs->sales_plan, 0),
                round(($fs->acc_fact * 30) / $fs->acc_plan, 0),
                round(($fs->cr_fact * 15) / $fs->cr_plan,0),
                round(($fs->tradein_fact * 15) / $fs->tradein_plan,0),
            ]);

            //$total_percent = $totalPrcFields->sum() / $totalPrcFields->count();
            $total_percent = $totalPrcFields->sum();

            /* Процент эффективности сотрудника */


            /*$effFields = collect([

                $this->percent($fs->sales_fact, $fs->sales_plan),
                $this->percent($fs->acc_fact, $fs->acc_plan),
                $this->percent($fs->cr_fact, $fs->cr_plan),
                $this->percent($fs->tradein_fact, $fs->tradein_plan),

                $worker->rate
            ]);*/

            $effFields = collect([
                round(($fs->sales_fact * 40) / $fs->sales_plan, 0),
                round(($fs->acc_fact * 30) / $fs->acc_plan, 0),
                round(($fs->cr_fact * 15) / $fs->cr_plan,0),
                round(($fs->tradein_fact * 15) / $fs->tradein_plan,0),

            ]);

            //$total_efficiency = round(($effFields->sum()) / $effFields->count(), 0);
            $total_efficiency = round(($effFields->sum()));

            $output_workers['image'] = '/images/workers/' . str_slug($worker->name) . '.png';
            $output_workers['image_m'] = '/images/workers/' . str_slug($worker->name) . '_220x265.png';
            $output_workers['image_l'] = '/images/workers/' . str_slug($worker->name) . '_375x440.png';
            $output_workers['name'] = $worker->name;

            $output_workers['sales_fact'] = $fs->sales_fact;
            $output_workers['sales_plan'] = $fs->sales_plan;

            $output_workers['acc_fact'] = $fs->acc_fact;
            $output_workers['acc_plan'] = $fs->acc_plan;

            $output_workers['cr_fact'] = $fs->cr_fact;
            $output_workers['cr_plan'] = $fs->cr_plan;

            $output_workers['tradein_fact'] = $fs->tradein_fact;
            $output_workers['tradein_plan'] = $fs->tradein_plan;

            $output_workers['total_percent'] = round($total_percent, 0);

            //$output_workers['total_efficiency'] = $total_efficiency;
            $output_workers['total_efficiency'] = round($total_percent, 0);
            $output_workers['position'] = $worker->position;
            $output_workers['rate'] = $worker->rate;

            $output_workers['id'] = $worker->ext_id;
            $workerList->push($output_workers);
        }

        //arrows
        $workerListSorted = $workerList->sortByDesc('total_efficiency');
        $timedKey = 0;
        foreach ($workerListSorted as $i => &$w) {
            $wItem = $workers->where('ext_id', $w['id'])->first();
            $w['arrow'] = $w['position'] >= $timedKey ? 'green' : 'red';
            if ($w['position'] > $timedKey) {
                $w['arrow'] = 'green';
            }
            if ($w['position'] < $timedKey) {
                $w['arrow'] = 'red';
            }
            if ($w['position'] === $timedKey) {
                $w['arrow'] = $wItem->arrow;
            }
            if ($timedKey === 0) {
                $wItem->{$fsName}->best = true;
            } else {
                $wItem->{$fsName}->best = false;
            }
            $wItem->{$fsName}->eff = $w['total_efficiency'];
            $wItem->position = $timedKey;
            $wItem->arrow = $w['arrow'];
            $wItem->{$fsName}->save();
            $wItem->save();
            $timedKey++;
        }
        // end arrows

        $yearPlan = Plan::whereDep('new')
            ->where('year', date('Y'))
            ->where('type', 'year')
            ->first();
        $monthPlan = Plan::whereDep('new')
            ->where('month', (int)date('m'))
            ->where('year', date('Y'))
            ->where('type', 'month')
            ->first();

        $plan = [
            'y' => $yearPlan,
            'm' => $monthPlan
        ];

        $allFields = $workers->pluck('year_fields')->flatten();

        $fact = [
            'y' => [
                'sales_fact' => $allFields->sum('sales_fact'),
                'acc_fact' => $allFields->sum('acc_fact'),
                'cr_fact' => $allFields->sum('cr_fact') / $allFields->count() > 100 ? 100 : $allFields->sum('cr_fact') / $allFields->count(),
                'tradein_fact' => $allFields->sum('tradein_fact'),
            ],
            'm' => [
                'sales_fact' => $workers->sum('fields.sales_fact'),
                'acc_fact' => $workers->sum('fields.acc_fact'),
                'cr_fact' => $workers->sum('fields.cr_fact') / $workers->count() > 100 ? 100 : $workers->sum('fields.cr_fact') / $workers->count(),
                'tradein_fact' => round($workers->sum('fields.tradein_fact')),
            ],
        ];
        $monthPercent = round($workerList->sum('total_percent') / $workerList->count());

        $yPercent = round((
                ((100 * $fact['y']['sales_fact']) / $plan['y']['sales_plan']) +
                ((100 * $fact['y']['acc_fact']) / $plan['y']['acc_plan']) +
                ((100 * $fact['y']['cr_fact']) / $plan['y']['cr_plan']) +
                ((100 * $fact['y']['tradein_fact']) / $plan['y']['tradein_plan'])
            ) / 4);

        $consDepUsers = User::whereDep('new')->with($fsName)->get();

        $besters = [
            'new' => $consDepUsers->sortByDesc($fsName . '.eff')->first()->toArray(),
        ];

        return [
            'workers' => $workerListSorted->toArray(),
            'plan' => $plan,
            'fact' => $fact,
            'yPercent' => $yPercent,
            'monthPercent' => $monthPercent,
            'recordPercent' => $allFields->max('eff'),
            'bests' => $besters
        ];
    }
}
