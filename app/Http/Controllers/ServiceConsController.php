<?php


namespace App\Http\Controllers;

use App\Models\Plan;
use App\Models\User;

class ServiceConsController extends Controller
{
    public function percent($from, $to)
    {
        if ((int)$to) {
            return round((100 * $from) / $to);
        }
        return 0;
    }

    public function countBests(User $user)
    {
        return $user->all_fields->where('best', true)->count();
    }

    public function get($fsName = 'fields')
    {
        $workers = User::whereActive(true)
            ->where('dep', 'service_cons')
            ->with($fsName, 'year_fields')
            ->get();

        foreach ($workers as $worker) {
            $worker->fields()->firstOrCreate();
            $worker->load($fsName);
        }

        $workerList = collect();

        foreach ($workers as $worker) {
            $output_workers = collect();
            $fs = $worker->{$fsName};

            if (!$fs) continue;

            $totalPrcFields = collect([
                $this->percent($fs->norm_hours_fact, $fs->norm_hours_plan),
                //$this->percent($fs->amount_part_fact, $fs->amount_part_plan),
                $this->percent($fs->cr_fact, $fs->cr_plan),
                $this->percent($fs->car_entry_fact, $fs->car_entry_plan),
                //$this->percent($fs->illiquid_sales_fact, $fs->illiquid_sales_plan),
            ]);

            $total_percent = $totalPrcFields->sum() / $totalPrcFields->count();

            /* Процент эффективности сотрудника */


            $effFields = collect([

                $this->percent($fs->norm_hours_fact, $fs->norm_hours_plan),
                //$this->percent($fs->amount_part_fact, $fs->amount_part_plan),
                $this->percent($fs->cr_fact, $fs->cr_plan),
                $this->percent($fs->car_entry_fact, $fs->car_entry_plan),
                //$this->percent($fs->illiquid_sales_fact, $fs->illiquid_sales_plan),

                $worker->rate
            ]);

            $total_efficiency = round(($effFields->sum()) / $effFields->count(), 0);

            $output_workers['image'] = '/images/workers/' . str_slug($worker->name) . '.png';
            $output_workers['image_m'] = '/images/workers/' . str_slug($worker->name) . '_220x265.png';
            $output_workers['image_l'] = '/images/workers/' . str_slug($worker->name) . '_375x440.png';
            $output_workers['name'] = $worker->name;

            $output_workers['norm_hours_fact'] = $fs->norm_hours_fact;
            $output_workers['norm_hours_plan'] = $fs->norm_hours_plan;

            $output_workers['amount_part_fact'] = $fs->amount_part_fact;
            $output_workers['amount_part_plan'] = $fs->amount_part_plan;

            $output_workers['cr_fact'] = $fs->cr_fact;
            $output_workers['cr_plan'] = $fs->cr_plan;

            $output_workers['car_entry_fact'] = $fs->car_entry_fact;
            $output_workers['car_entry_plan'] = $fs->car_entry_plan;

            //$output_workers['illiquid_sales_fact'] = $fs->illiquid_sales_fact;
            //$output_workers['illiquid_sales_plan'] = $fs->illiquid_sales_plan;
//
            $output_workers['total_percent'] = round($total_percent, 0);

            //$output_workers['total_efficiency'] = $total_efficiency;
            $output_workers['total_efficiency'] = round($total_percent, 0);
            $output_workers['position'] = $worker->position;
            $output_workers['rate'] = $worker->rate;

            $output_workers['id'] = $worker->ext_id;
            $workerList->push($output_workers);
        }

        //arrows
        $workerListSorted = $workerList->sortByDesc('total_efficiency');
        $timedKey = 0;
        foreach ($workerListSorted as $i => &$w) {
            $wItem = $workers->where('ext_id', $w['id'])->first();
            $w['arrow'] = $w['position'] >= $timedKey ? 'green' : 'red';
            if ($w['position'] > $timedKey) {
                $w['arrow'] = 'green';
            }
            if ($w['position'] < $timedKey) {
                $w['arrow'] = 'red';
            }
            if ($w['position'] === $timedKey) {
                $w['arrow'] = $wItem->arrow;
            }
            if ($timedKey === 0) {
                $wItem->{$fsName}->best = true;
            } else {
                $wItem->{$fsName}->best = false;
            }
            $wItem->{$fsName}->eff = $w['total_efficiency'];
            $wItem->position = $timedKey;
            $wItem->arrow = $w['arrow'];
            $wItem->{$fsName}->save();
            $wItem->save();
            $timedKey++;
        }
        // end arrows

        $yearPlan = Plan::whereDep('service_cons')
            ->where('year', date('Y'))
            ->where('type', 'year')
            ->first();
        $monthPlan = Plan::whereDep('service_cons')
            ->where('month', (int)date('m'))
            ->where('year', date('Y'))
            ->where('type', 'month')
            ->first();

        $plan = [
            'y' => $yearPlan,
            'm' => $monthPlan
        ];

        $allFields = $workers->pluck('year_fields')->flatten();

        $fact = [
            'y' => [
                'norm_hours_fact' => $allFields->sum('norm_hours_fact'),
                //'amount_part_fact' => $allFields->sum('amount_part_fact'),
                'cr_fact' => round($allFields->sum('cr_fact') / $allFields->count()) > 100 ? 100 : round($allFields->sum('cr_fact') / $allFields->count()),
                'car_entry_fact' => $yearPlan->car_entry_fact,
                //'illiquid_sales_fact' => $allFields->sum('illiquid_sales_fact'),
            ],
            'm' => [
                'norm_hours_fact' => $workers->sum('fields.norm_hours_fact'),
                //'amount_part_fact' => $workers->sum('fields.amount_part_fact'),
                'cr_fact' => round($workers->sum('fields.cr_fact') / $workers->count()) > 100 ? 100 : round($workers->sum('fields.cr_fact') / $workers->count()),
                'car_entry_fact' => $monthPlan->car_entry_fact,
                //'illiquid_sales_fact' => $workers->sum('fields.illiquid_sales_fact'),
            ],
        ];

        $monthPercent = round($workerList->sum('total_percent') / $workerList->count());

        $yPercent = round((
                ((100 * $fact['y']['norm_hours_fact']) / $plan['y']['norm_hours_plan']) +
                //((100 * $fact['y']['amount_part_fact']) / $plan['y']['amount_part_plan']) +
                ((100 * $fact['y']['cr_fact']) / $plan['y']['cr_plan']) +
                ((100 * $fact['y']['car_entry_fact']) / $plan['y']['car_entry_plan'])
                //((100 * $fact['y']['illiquid_sales_fact']) / $plan['y']['illiquid_sales_plan'])
               ) / 5);

        $consDepUsers = User::whereDep('service_cons')->with($fsName)->get();

        $besters = [
            'service_cons' => $consDepUsers->sortByDesc($fsName.'.eff')->first()->toArray(),
        ];

        return [
            'workers'       => $workerListSorted->toArray(),
            'plan'          => $plan,
            'fact'          => $fact,
            'yPercent'      => $yPercent,
            'monthPercent'  => $monthPercent,
            'recordPercent' => $allFields->max('eff'),
            'bests'         => $besters
        ];
    }
}
