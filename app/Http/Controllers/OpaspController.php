<?php

namespace App\Http\Controllers;

use App\Models\Plan;
use App\Models\User;
use Google_Service_Sheets;

class OpaspController extends Controller
{
    public function percent($from, $to)
    {
        if ((int) $to) {
            return round((100 * $from) / $to);
        }
        return 0;
    }

    public function countBests(User $user)
    {
        return $user->all_fields->where('best', true)->count();
    }

    public function gUpAll()
    {
        $service = new Google_Service_Sheets((new GoogleTable())->getClient());
        $spreadsheetId = '1pI9tsIokfoJFG5I8ODyPUE-uJO3sIvp3qFSH5xYd-to';
        $params = ['valueInputOption' => 'RAW'];
        $list = "ОПАСП";

        $users = User::whereDep('bu')->with('year_fields')->get();
        $fs = $users->pluck('year_fields')->flatten();
        $values = [];

        foreach ($fs->groupBy('month') as $f) {
            $values[] = [
                $f->sum('sales_fact'),
                $f->sum('acc_fact'),
                $f->sum('topack_fact'),
                $f->sum('kasko_fact'),
                $f->sum('ogpo_fact'),
            ];
        }

        $body = new \Google_Service_Sheets_ValueRange(['values' => $values]);
        $range = "{$list}!C39";
        $service->spreadsheets_values->update($spreadsheetId, $range, $body, $params);
    }

    public function uploadToGoogle()
    {
        $data = $this->getPrevM();
        $service = new \Google_Service_Sheets((new GoogleTable())->getClient());
        $spreadsheetId = '1V0EduVPPJ1WDZL5u7DxLIa9Am3x32iI3jjZv2LyxlCI';
        $users = User::whereDep('bu')->with("fields", "all_fields", "year_fields")->get();
        $params = ['valueInputOption' => 'RAW'];
        $list = "ОПАСП";

        $users = $users->sortByDesc("fields.eff");
        $key = 0;
        $valuesM = [];
        foreach ($data['workers'] as $user) {
            $arrow = "";
            if (!empty($user["arrow"])) {
                $arrow = $user["arrow"] === "red" ? "↓" : "↑";
            }
            $valuesM[] = [
                $user["name"],
                $arrow,
                $user["total_efficiency"],
                $user["total_percent"],
                $user["sales_fact"],
                $user["access_fact"],
                $user["to_pack_fact"],
                $user["kasko_fact"],
                $user["ogpo_fact"],
				$user["tradein_fact"]+$user["redemption_fact"],
                $user["trafic_fact"],
            ];
        }
        $body = new \Google_Service_Sheets_ValueRange(['values' => $valuesM]);
        $range = "{$list}!A" . ($key + 3);
        $service->spreadsheets_values->update($spreadsheetId, $range, $body, $params);

        $key = 17;
        $valuesYCollection = collect();

        foreach ($users as $user) {
            $fs = $user->all_fields;
            $valuesYCollection->push(collect([
                "name" => $user->name,
                "arrow" => "",
                "sales_fact" => $fs->sum('sales_fact'),
                "acc_fact" => $fs->sum('acc_fact'),
                "topack_fact" => $fs->sum('topack_fact'),
                "kasko_fact" => $fs->sum('kasko_fact'),
                "ogpo_fact" => $fs->sum('ogpo_fact'),
                "trafic_fact" => $fs->sum('trafic_fact'),
				"trade_in" => $fs->sum('tradein_fact') + $fs->sum('redemption_fact'),
                "best" => $this->countBests($user),
            ]));
        }

        $valuesYCollection = $valuesYCollection->sortByDesc("best");
        $valuesY = [];
        foreach ($valuesYCollection as $item) {
            $valuesY[] = $item->values();
        }
        $body = new \Google_Service_Sheets_ValueRange(['values' => $valuesY]);
        $range = "{$list}!A" . $key;
        $service->spreadsheets_values->update($spreadsheetId, $range, $body, $params);

        $valuesYAll = [];
        foreach ($users as $user) {
            $row = [];
            $row[] = $user->name;
            $row[] = "";
            for ($i = 1; $i <= (int) date('m'); $i++) {
                $yrf = $user->year_fields->where('month', $i)->where('year', date('Y'));
                $row[] = $yrf->count() ? $yrf->first()->eff : 0;
            }
            $valuesYAll[] = $row;
        }

        $key = 31;
        $body = new \Google_Service_Sheets_ValueRange(['values' => $valuesYAll]);
        $range = "{$list}!A" . $key;
        $service->spreadsheets_values->update($spreadsheetId, $range, $body, $params);
    }

    public function getPrevM()
    {
        $workers = User::whereActive(true)
            ->where('dep', 'bu')
            ->with('p_fields', 'all_fields')
            ->get();

        $traffic_sum = $workers->sum('p_fields.trafic_fact');
        $summAccess = $workers->sum('p_fields.acc_fact');
        $allSales = $workers->sum('p_fields.sales_fact');
        $allTo = $workers->sum('p_fields.topack_fact');
        $allKasko = $workers->sum('p_fields.kasko_fact');
        $allTradein = $workers->sum('p_fields.tradein_fact');
        $allTestdrive = $workers->sum('p_fields.testdrive_fact');
        $ogpo_sum = $workers->sum('p_fields.ogpo_fact');
        $staging_sum = $workers->sum('p_fields.staging_fact');

        $workerList = collect();

        foreach ($workers as $worker) {
            $output_workers = collect();
            $fs = $worker->p_fields;
            if (!$fs) continue;
            $sales_fact = $fs->sales_fact;
            $sales_plan = $fs->sales_plan;
            $sales_percent = (100 * $sales_fact) / $sales_plan;

            $access_fact = $fs->acc_fact;
            $access_plan = $fs->acc_plan;
            $access_percent = (100 * $access_fact) / $access_plan;

            $to_pack_fact = $fs->topack_fact;
            $to_pack_plan = $fs->topack_plan;
            $to_pack_percent = (100 * $to_pack_fact) / $to_pack_plan;

            $kasko_fact = $fs->kasko_fact;
            $kasko_plan = $fs->kasko_plan;
            $kasko_percent = (100 * $kasko_fact) / $kasko_plan;

            $ogpo_fact = $fs->ogpo_fact;
            $ogpo_plan = $fs->ogpo_plan;
            $ogpo_percent = (100 * $ogpo_fact) / $ogpo_plan;

            $trafic_fact = $fs->trafic_fact;
            $trafic_percent = ($trafic_fact != 0 && $traffic_sum != 0) ? (100 * $trafic_fact) / $traffic_sum : 0;

            $total_percent = ($ogpo_percent + $sales_percent + $access_percent + $to_pack_percent + $kasko_percent + $trafic_percent) / 6;
            /* Процент эффективности сотрудника */

            $efficiency_ogpo = $this->percent($ogpo_fact, $ogpo_sum);
            $efficiency_ogpo_all = $this->percent($ogpo_fact, $sales_fact);
            $efficiency_traffic = ($traffic_sum != 0 && $trafic_fact != 0) ? round(100 * $trafic_fact / $traffic_sum, 2) : 0;
            $efficiency_sales = ($trafic_fact != 0 && $sales_fact != 0) ? round(100 * $sales_fact / $trafic_fact, 2) : 0;
            $efficiency_sales_all = ($sales_fact != 0 && $allSales != 0) ? (100 * $sales_fact) / $allSales : 0;
            $efficiency_access = ($summAccess != 0 && $access_fact != 0) ? round(100 * $access_fact / $summAccess, 2) : 0;
            $efficiency_to = ($to_pack_fact != 0 && $sales_fact != 0) ? round(100 * $to_pack_fact / $sales_fact, 2) : 0;

            // 100 * ТО сотрудника / Общие ТО
            $efficiency_to_all = ($to_pack_fact != 0 && $allTo != 0) ? round(100 * $to_pack_fact / $allTo, 2) : 0;

            $efficiency_kasko = ($kasko_fact != 0 && $sales_fact != 0) ? round(100 * $kasko_fact / $sales_fact, 2) : 0;

            // 100 * КАСКО сотрудника / Общие КАСКО
            $efficiency_kasko_all = ($kasko_fact != 0 && $allKasko != 0) ? round(100 * $kasko_fact / $allKasko, 2) : 0;

            $total_efficiency = round((

                // 100 * Трафик сотрудника / Общий трафик
                $efficiency_traffic

                // 100 * Продажи сотрудника / Трафик сотрудника
                + $efficiency_sales

                // 100 * Продажи сотрудника / Общие продажи
                + $efficiency_sales_all

                // 100 * Аксессуары сотрудника / Общие Аксессуары
                + $efficiency_access

                // 100 * ТО сотрудника / Продажи сотрудника
                + $efficiency_to

                // 100 * ТО сотрудника / Общие ТО
                + $efficiency_to_all

                // 100 * КАСКО сотрудника / Продажи сотрудника
                + $efficiency_kasko

                // 100 * КАСКО сотрудника / Общие КАСКО
                + $efficiency_kasko_all

                + $efficiency_ogpo_all
                + $efficiency_ogpo
                + $worker->rate) / 11, 0);

            $output_workers['image'] = '/images/workers_bu/' . str_slug($worker->name) . '.png';
            $output_workers['image_m'] = '/images/workers_bu/' . str_slug($worker->name) . '_220x265.png';
            $output_workers['image_l'] = '/images/workers_bu/' . str_slug($worker->name) . '_375x440.png';
            $output_workers['name'] = $worker->name;

            $output_workers['sales_fact'] = $fs->sales_fact;
            $output_workers['sales_plan'] = $fs->sales_plan;

            $output_workers['access_fact'] = $fs->acc_fact;
            $output_workers['access_plan'] = $fs->acc_plan;

            $output_workers['to_pack_fact'] = $fs->topack_fact;
            $output_workers['to_pack_plan'] = $fs->topack_plan;

            $output_workers['kasko_fact'] = $fs->kasko_fact;
            $output_workers['kasko_plan'] = $fs->kasko_plan;

            $output_workers['ogpo_fact'] = $fs->ogpo_fact;
            $output_workers['ogpo_plan'] = $fs->ogpo_plan;

            $output_workers['trafic_fact'] = $fs->trafic_fact;
            $output_workers['tradein_fact'] = $fs->tradein_fact;
            $output_workers['redemption_fact'] = $fs->redemption_fact;


            $output_workers['total_percent'] = round($total_percent, 0);

            $output_workers['total_efficiency'] = $total_efficiency;
            $output_workers['position'] = $worker->position;
            $output_workers['rate'] = $worker->rate;

            $output_workers['id'] = $worker->ext_id;
            $workerList->push($output_workers);
        }

        //arrows
        $workerListSorted = $workerList->sortByDesc('total_efficiency');
        $timedKey = 0;
        foreach ($workerListSorted as $i => &$w) {
            $wItem = $workers->where('ext_id', $w['id'])->first();
            $w['arrow'] = $w['position'] >= $timedKey ? 'green' : 'red';
            if ($w['position'] > $timedKey) {
                $w['arrow'] = 'green';
            }
            if ($w['position'] < $timedKey) {
                $w['arrow'] = 'red';
            }
            if ($w['position'] === $timedKey) {
                $w['arrow'] = $wItem->arrow;
            }

            if ($timedKey === 0) {
                $wItem->p_fields->best = true;
            } else {
                $wItem->p_fields->best = false;
            }

            $wItem->p_fields->eff = $w['total_efficiency'];
            $wItem->position = $timedKey;
            $wItem->arrow = $w['arrow'];
            $wItem->p_fields->save();
            $wItem->save();
            $timedKey++;
        }
        // end arrows

        $yearPlan = Plan::whereDep('bu')
            ->where('year', date('Y'))
            ->where('type', 'year')
            ->first();
        $monthPlan = Plan::whereDep('bu')
            ->where('month', (int) date('m'))
            ->where('year', date('Y'))
            ->where('type', 'month')
            ->first();

        $plan = [
            'y' => $yearPlan,
            'm' => $monthPlan,
        ];

        //сотдрудники которых нет на доске но данные идут в учет на месяц и год прим. Регина или Владимир
        $addUsers = User::whereIn('ext_id', [
            112, //Лысенко Владимир
        ])->with('all_fields', 'p_fields')->get();
        $allFieldsAdd = $addUsers->pluck('all_fields')->flatten();

        $allFields = $workers->pluck('all_fields')->flatten();
        $fact = [
            'y' => [
                'trafic' => $allFields->sum('trafic_fact'),
                'sales' => $allFields->sum('sales_fact'),
                'acc' => $allFields->sum('acc_fact'),
                'topack' => $allFields->sum('topack_fact'),
                'kasko' => $allFields->sum('kasko_fact'),
                'ogpo' => $allFields->sum('ogpo_fact'),
            ],
            'm' => [
                'trafic' => $workers->sum('p_fields.trafic_fact'),
                'sales' => $workers->sum('p_fields.sales_fact'),
                'acc' => $workers->sum('p_fields.acc_fact'),
                'topack' => $workers->sum('p_fields.topack_fact'),
                'kasko' => $workers->sum('p_fields.kasko_fact'),
                'ogpo' => $workers->sum('p_fields.ogpo_fact'),
            ],
        ];
        $monthPercent = round($workerList->sum('total_percent') / $workerList->count());
        $yPercent = round((
            ((100 * $fact['y']['sales']) / $plan['y']['sales_plan']) +
            ((100 * $fact['y']['acc']) / $plan['y']['acc_plan']) +
            ((100 * $fact['y']['topack']) / $plan['y']['topack_plan']) +
            ((100 * $fact['y']['kasko']) / $plan['y']['kasko_plan']) +
            ((100 * $fact['y']['ogpo']) / $plan['y']['ogpo_plan'])) / 5);

        return [
            'workers' => $workerListSorted->toArray(),
            'plan' => $plan,
            'fact' => $fact,
            'yPercent' => $yPercent,
            'monthPercent' => $monthPercent,
            'recordPercent' => $allFields->max('eff'),
        ];
    }

    public function getOld()
    {
        $workers = User::whereActive(true)
            ->where('dep', 'bu')
            ->with('fields', 'all_fields')
            ->get();

        foreach ($workers as $worker) {
            $worker->fields()->firstOrCreate();
            $worker->load('fields');
        }
        $traffic_sum = $workers->sum('fields.trafic_fact');
        $summAccess = $workers->sum('fields.acc_fact');
        $allSales = $workers->sum('fields.sales_fact');
        $allTo = $workers->sum('fields.topack_fact');
        $allKasko = $workers->sum('fields.kasko_fact');
        $allTradein = $workers->sum('fields.tradein_fact');
        $allTestdrive = $workers->sum('fields.testdrive_fact');
        $ogpo_sum = $workers->sum('fields.ogpo_fact');
        $staging_sum = $workers->sum('fields.staging_fact');

        $workerList = collect();

        foreach ($workers as $worker) {
            $output_workers = collect();
            $fs = $worker->fields;

            $sales_fact = $fs->sales_fact;
            $sales_plan = $fs->sales_plan;
            $sales_percent = (100 * $sales_fact) / $sales_plan;

            $access_fact = $fs->acc_fact;
            $access_plan = $fs->acc_plan;
            $access_percent = (100 * $access_fact) / $access_plan;

            $to_pack_fact = $fs->topack_fact;
            $to_pack_plan = $fs->topack_plan;
            $to_pack_percent = (100 * $to_pack_fact) / $to_pack_plan;

            $kasko_fact = $fs->kasko_fact;
            $kasko_plan = $fs->kasko_plan;
            $kasko_percent = (100 * $kasko_fact) / $kasko_plan;

            $ogpo_fact = $fs->ogpo_fact;
            $ogpo_plan = $fs->ogpo_plan;
            $ogpo_percent = (100 * $ogpo_fact) / $ogpo_plan;

            $trafic_fact = $fs->trafic_fact;
            $trafic_percent = ($trafic_fact != 0 && $traffic_sum != 0) ? (100 * $trafic_fact) / $traffic_sum : 0;

            $total_percent = ($ogpo_percent + $sales_percent + $access_percent + $to_pack_percent + $kasko_percent + $trafic_percent) / 6;
            /* Процент эффективности сотрудника */

            $efficiency_ogpo = $this->percent($ogpo_fact, $ogpo_sum);
            $efficiency_ogpo_all = $this->percent($ogpo_fact, $sales_fact);
            $efficiency_traffic = ($traffic_sum != 0 && $trafic_fact != 0) ? round(100 * $trafic_fact / $traffic_sum, 2) : 0;
            $efficiency_sales = ($trafic_fact != 0 && $sales_fact != 0) ? round(100 * $sales_fact / $trafic_fact, 2) : 0;
            $efficiency_sales_all = ($sales_fact != 0 && $allSales != 0) ? (100 * $sales_fact) / $allSales : 0;
            $efficiency_access = ($summAccess != 0 && $access_fact != 0) ? round(100 * $access_fact / $summAccess, 2) : 0;
            $efficiency_to = ($to_pack_fact != 0 && $sales_fact != 0) ? round(100 * $to_pack_fact / $sales_fact, 2) : 0;

            // 100 * ТО сотрудника / Общие ТО
            $efficiency_to_all = ($to_pack_fact != 0 && $allTo != 0) ? round(100 * $to_pack_fact / $allTo, 2) : 0;

            $efficiency_kasko = ($kasko_fact != 0 && $sales_fact != 0) ? round(100 * $kasko_fact / $sales_fact, 2) : 0;

            // 100 * КАСКО сотрудника / Общие КАСКО
            $efficiency_kasko_all = ($kasko_fact != 0 && $allKasko != 0) ? round(100 * $kasko_fact / $allKasko, 2) : 0;

            $total_efficiency = round((

                // 100 * Трафик сотрудника / Общий трафик
                $efficiency_traffic

                // 100 * Продажи сотрудника / Трафик сотрудника
                + $efficiency_sales

                // 100 * Продажи сотрудника / Общие продажи
                + $efficiency_sales_all

                // 100 * Аксессуары сотрудника / Общие Аксессуары
                + $efficiency_access

                // 100 * ТО сотрудника / Продажи сотрудника
                + $efficiency_to

                // 100 * ТО сотрудника / Общие ТО
                + $efficiency_to_all

                // 100 * КАСКО сотрудника / Продажи сотрудника
                + $efficiency_kasko

                // 100 * КАСКО сотрудника / Общие КАСКО
                + $efficiency_kasko_all

                + $efficiency_ogpo_all
                + $efficiency_ogpo
                + $worker->rate) / 11, 0);

            $output_workers['image'] = '/images/workers_bu/' . str_slug($worker->name) . '.png';
            $output_workers['image_m'] = '/images/workers_bu/' . str_slug($worker->name) . '_220x265.png';
            $output_workers['image_l'] = '/images/workers_bu/' . str_slug($worker->name) . '_375x440.png';
            $output_workers['name'] = $worker->name;

            $output_workers['sales_fact'] = $fs->sales_fact;
            $output_workers['sales_plan'] = $fs->sales_plan;

            $output_workers['access_fact'] = $fs->acc_fact;
            $output_workers['access_plan'] = $fs->acc_plan;

            $output_workers['to_pack_fact'] = $fs->topack_fact;
            $output_workers['to_pack_plan'] = $fs->topack_plan;

            $output_workers['kasko_fact'] = $fs->kasko_fact;
            $output_workers['kasko_plan'] = $fs->kasko_plan;

            $output_workers['ogpo_fact'] = $fs->ogpo_fact;
            $output_workers['ogpo_plan'] = $fs->ogpo_plan;

            $output_workers['trafic_fact'] = $fs->trafic_fact;

            $output_workers['total_percent'] = round($total_percent, 0);

            $output_workers['total_efficiency'] = $total_efficiency;
            $output_workers['position'] = $worker->position;
            $output_workers['rate'] = $worker->rate;

            $output_workers['id'] = $worker->ext_id;
            $workerList->push($output_workers);
        }

        //arrows
        $workerListSorted = $workerList->sortByDesc('total_efficiency');
        $timedKey = 0;
        foreach ($workerListSorted as $i => &$w) {
            $wItem = $workers->where('ext_id', $w['id'])->first();
            $w['arrow'] = $w['position'] >= $timedKey ? 'green' : 'red';
            if ($w['position'] > $timedKey) {
                $w['arrow'] = 'green';
            }
            if ($w['position'] < $timedKey) {
                $w['arrow'] = 'red';
            }
            if ($w['position'] === $timedKey) {
                $w['arrow'] = $wItem->arrow;
            }

            if ($timedKey === 0) {
                $wItem->fields->best = true;
            } else {
                $wItem->fields->best = false;
            }

            $wItem->fields->eff = $w['total_efficiency'];
            $wItem->position = $timedKey;
            $wItem->arrow = $w['arrow'];
            $wItem->fields->save();
            $wItem->save();
            $timedKey++;
        }
        // end arrows

        $yearPlan = Plan::whereDep('bu')
            ->where('year', date('Y'))
            ->where('type', 'year')
            ->first();
        $monthPlan = Plan::whereDep('bu')
            ->where('month', (int) date('m'))
            ->where('year', date('Y'))
            ->where('type', 'month')
            ->first();

        $plan = [
            'y' => $yearPlan,
            'm' => $monthPlan,
        ];

        //сотдрудники которых нет на доске но данные идут в учет на месяц и год прим. Регина или Владимир
        $addUsers = User::whereIn('ext_id', [
            112, //Лысенко Владимир
        ])->with('all_fields', 'fields')->get();
        $allFieldsAdd = $addUsers->pluck('all_fields')->flatten();

        $allFields = $workers->pluck('all_fields')->flatten();
        $fact = [
            'y' => [
                'trafic' => $allFields->sum('trafic_fact'),
                'sales' => $allFields->sum('sales_fact') + $allFieldsAdd->sum('sales_fact'),
                'acc' => $allFields->sum('acc_fact'),
                'topack' => $allFields->sum('topack_fact'),
                'kasko' => $allFields->sum('kasko_fact'),
                'ogpo' => $allFields->sum('ogpo_fact'),
            ],
            'm' => [
                'trafic' => $workers->sum('fields.trafic_fact'),
                'sales' => $workers->sum('fields.sales_fact') + $addUsers->sum('fields.sales_fact'),
                'acc' => $workers->sum('fields.acc_fact'),
                'topack' => $workers->sum('fields.topack_fact'),
                'kasko' => $workers->sum('fields.kasko_fact'),
                'ogpo' => $workers->sum('fields.ogpo_fact'),
            ],
        ];
        $monthPercent = round($workerList->sum('total_percent') / $workerList->count());
        $yPercent = round((
            ((100 * $fact['y']['sales']) / $plan['y']['sales_plan']) +
            ((100 * $fact['y']['acc']) / $plan['y']['acc_plan']) +
            ((100 * $fact['y']['topack']) / $plan['y']['topack_plan']) +
            ((100 * $fact['y']['kasko']) / $plan['y']['kasko_plan']) +
            ((100 * $fact['y']['ogpo']) / $plan['y']['ogpo_plan'])) / 5);

        return [
            'workers' => $workerListSorted->toArray(),
            'plan' => $plan,
            'fact' => $fact,
            'yPercent' => $yPercent,
            'monthPercent' => $monthPercent,
            'recordPercent' => $allFields->max('eff'),
        ];
    }



    public function get($fsName = 'fields')
    {
        $workers = User::whereActive(true)
            ->where('dep', 'bu')
            ->with($fsName, 'year_fields')
            ->get();

        foreach ($workers as $worker) {
            $worker->fields()->firstOrCreate();
            $worker->load($fsName);
        }

        $workerList = collect();

        foreach ($workers as $worker) {
            $output_workers = collect();
            $fs = $worker->{$fsName};

            if (!$fs) continue;

            $totalPrcFields = collect([
                $this->percent($fs->sales_fact, $fs->sales_plan),
                //$this->percent($fs->acc_fact, $fs->acc_plan),
                //$this->percent($fs->cr_fact, $fs->cr_plan),
                $this->percent($fs->tradein_fact, $fs->tradein_plan),
            ]);

            $total_percent = $totalPrcFields->sum() / $totalPrcFields->count();

            /* Процент эффективности сотрудника */


            $effFields = collect([

                $this->percent($fs->sales_fact, $fs->sales_plan),
                //$this->percent($fs->acc_fact, $fs->acc_plan),
                //$this->percent($fs->cr_fact, $fs->cr_plan),
                $this->percent($fs->tradein_fact, $fs->tradein_plan),

                $worker->rate
            ]);

            $total_efficiency = round(($effFields->sum()) / $effFields->count(), 0);

            $output_workers['image'] = '/images/workers/' . str_slug($worker->name) . '.png';
            $output_workers['image_m'] = '/images/workers/' . str_slug($worker->name) . '_220x265.png';
            $output_workers['image_l'] = '/images/workers/' . str_slug($worker->name) . '_375x440.png';
            $output_workers['name'] = $worker->name;

            $output_workers['sales_fact'] = $fs->sales_fact;
            $output_workers['sales_plan'] = $fs->sales_plan;

            $output_workers['acc_fact'] = $fs->acc_fact;
            $output_workers['acc_plan'] = $fs->acc_plan;

            $output_workers['cr_fact'] = $fs->cr_fact;
            $output_workers['cr_plan'] = $fs->cr_plan;

            $output_workers['tradein_fact'] = $fs->tradein_fact;
            $output_workers['tradein_plan'] = $fs->tradein_plan;

            $output_workers['total_percent'] = round($total_percent, 0);

            //$output_workers['total_efficiency'] = $total_efficiency;
            $output_workers['total_efficiency'] = round($total_percent, 0);
            $output_workers['position'] = $worker->position;
            $output_workers['rate'] = $worker->rate;

            $output_workers['id'] = $worker->ext_id;
            $workerList->push($output_workers);
        }

        //arrows
        $workerListSorted = $workerList->sortByDesc('total_efficiency');
        $timedKey = 0;
        foreach ($workerListSorted as $i => &$w) {
            $wItem = $workers->where('ext_id', $w['id'])->first();
            $w['arrow'] = $w['position'] >= $timedKey ? 'green' : 'red';
            if ($w['position'] > $timedKey) {
                $w['arrow'] = 'green';
            }
            if ($w['position'] < $timedKey) {
                $w['arrow'] = 'red';
            }
            if ($w['position'] === $timedKey) {
                $w['arrow'] = $wItem->arrow;
            }
            if ($timedKey === 0) {
                $wItem->{$fsName}->best = true;
            } else {
                $wItem->{$fsName}->best = false;
            }
            $wItem->{$fsName}->eff = $w['total_efficiency'];
            $wItem->position = $timedKey;
            $wItem->arrow = $w['arrow'];
            $wItem->{$fsName}->save();
            $wItem->save();
            $timedKey++;
        }
        // end arrows

        $yearPlan = Plan::whereDep('bu')
            ->where('year', date('Y'))
            ->where('type', 'year')
            ->first();
        $monthPlan = Plan::whereDep('bu')
            ->where('month', (int)date('m'))
            ->where('year', date('Y'))
            ->where('type', 'month')
            ->first();

        $plan = [
            'y' => $yearPlan,
            'm' => $monthPlan
        ];

        $addUsers = User::where('id',44)->with('year_fields', 'fields')->get();
        $userYearFields = $addUsers->pluck('year_fields')->flatten();
        $userMonthFields = $addUsers->pluck('fields')->flatten();

        $allFields = $workers->pluck('year_fields')->flatten();

        $fact = [
            'y' => [
                'sales_fact' => $allFields->sum('sales_fact') + $userYearFields->sum('sales_fact'),
                //'acc_fact' => $allFields->sum('acc_fact'),
                //'cr_fact' => $allFields->sum('cr_fact'),
                'tradein_fact' => $allFields->sum('tradein_fact'),
            ],
            'm' => [
                'sales_fact' => $workers->sum('fields.sales_fact') + $userMonthFields->sum('sales_fact'),
                //'acc_fact' => $workers->sum('fields.acc_fact'),
                //'cr_fact' => $workers->sum('fields.cr_fact'),
                'tradein_fact' => round($workers->sum('fields.tradein_fact')),
            ],
        ];
        $monthPercent = round($workerList->sum('total_percent') / $workerList->count());

        $yPercent = round((
                ((100 * $fact['y']['sales_fact']) / $plan['y']['sales_plan']) +
                //((100 * $fact['y']['acc_fact']) / $plan['y']['acc_plan']) +
                //((100 * $fact['y']['cr_fact']) / $plan['y']['cr_plan']) +
                ((100 * $fact['y']['tradein_fact']) / $plan['y']['tradein_plan'])
            ) / 2);

        $consDepUsers = User::whereDep('bu')->with($fsName)->get();

        $besters = [
            'bu' => $consDepUsers->sortByDesc($fsName . '.eff')->first()->toArray(),
        ];

        return [
            'workers' => $workerListSorted->toArray(),
            'plan' => $plan,
            'fact' => $fact,
            'yPercent' => $yPercent,
            'monthPercent' => $monthPercent,
            'recordPercent' => $allFields->max('eff'),
            'bests' => $besters
        ];
    }
}
