<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;

class AuthController extends Controller
{
    public function login(Request $request)
    {
        if (\Auth::attempt([
            'name'     => $request->username,
            'password' => $request->password,
        ], true)) {

            if($request->username === 'service_consultant'){
                return redirect()->route('service_consultant');
            }elseif($request->username === 'service_mechanik'){
                return redirect()->route('service_workshop');
            }elseif($request->username === 'opna'){
                return redirect()->route('opna');
            }elseif($request->username === 'opasp'){
                return redirect()->route('opasp');
            }


            return redirect()->route('service_consultant');
        }else{
            return redirect()->route('login');
        }
    }
}
