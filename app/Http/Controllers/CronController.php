<?php


namespace App\Http\Controllers;

use App\Models\Fields;
use App\Models\Plan;
use App\Models\User;
use Google_Service_Sheets;

class CronController extends Controller
{
    public function getCron(){

        self::getServiceConsultant();
        self::getOpasp();
        self::getOpna();
        self::getServiceWorkshop();
    }

    public function getServiceConsultant(){
        $service = new Google_Service_Sheets((new GoogleTable())->getClient());

        $spreadsheetId = '1_QIz4c2_Er0D2Iyab_FYt20lF6WkLEMW6mHXonmY5CA';
        $range = "Сервис консультанты!A3:G4";

        $data = $service->spreadsheets_values->get($spreadsheetId, $range);
        $values = $data->getValues();

        // Заполнение планов на текущий год и месяц
        foreach ($values as $item){
            if($item[0] == 'Текущий год'){

                if(
                    Plan::where('dep', 'service_cons')
                        ->where('type','year')
                        ->where('year', date('Y'))
                        ->where('month', date('m'))
                        ->first() != null
                ){
                    Plan::where('dep', 'service_cons')
                        ->where('type','year')
                        ->where('year', date('Y'))
                        ->where('month', date('m'))
                        ->update([
                            'norm_hours_plan' => isset($item[1]) ? self::checkingVariable($item[1]) : 0,
                            'amount_part_plan' => isset($item[2]) ? self::checkingVariable($item[2]) : 0,
                            'cr_plan' => isset($item[3]) ? self::checkingVariable($item[3]) : 0,
                            'car_entry_plan' => isset($item[4]) ? self::checkingVariable($item[4]) : 0,
                            'car_entry_fact' => isset($item[4]) ? self::checkingVariable($item[5]) : 0,
                            //'illiquid_sales_plan' => isset($item[5]) ? self::checkingVariable($item[6]) : 0,
                        ]);
                }else{
                    Plan::create([
                            'type' => 'year',
                            'dep' => 'service_cons',
                            'year' => date('Y'),
                            'month' => date('m'),
                            'norm_hours_plan' => isset($item[1]) ? self::checkingVariable($item[1]) : 0,
                            'amount_part_plan' => isset($item[2]) ? self::checkingVariable($item[2]) : 0,
                            'cr_plan' => isset($item[3]) ? self::checkingVariable($item[3]) : 0,
                            'car_entry_plan' => isset($item[4]) ? self::checkingVariable($item[4]) : 0,
                            'car_entry_fact' => isset($item[4]) ? self::checkingVariable($item[5]) : 0,
                            //'illiquid_sales_plan' => isset($item[5]) ? self::checkingVariable($item[6]) : 0,
                        ]);
                }

            }
            if($item[0] == 'Текущий месяц'){

                if(
                    Plan::where('dep', 'service_cons')
                        ->where('type','month')
                        ->where('year', date('Y'))
                        ->where('month', date('m'))
                        ->first() != null
                ){
                    Plan::where('dep', 'service_cons')
                        ->where('type','month')
                        ->where('year', date('Y'))
                        ->where('month', date('m'))
                        ->update([
                            'norm_hours_plan' => isset($item[1]) ? self::checkingVariable($item[1]) : 0,
                            'amount_part_plan' => isset($item[2]) ? self::checkingVariable($item[2]) : 0,
                            'cr_plan' => isset($item[3]) ? self::checkingVariable($item[3]) : 0,
                            'car_entry_plan' => isset($item[4]) ? self::checkingVariable($item[4]) : 0,
                            'car_entry_fact' => isset($item[4]) ? self::checkingVariable($item[5]) : 0,
                            //'illiquid_sales_plan' => isset($item[5]) ? self::checkingVariable($item[6]) : 0,
                        ]);
                }else{
                    Plan::create([
                        'type' => 'month',
                        'dep' => 'service_cons',
                        'year' => date('Y'),
                        'month' => date('m'),
                        'norm_hours_plan' => isset($item[1]) ? self::checkingVariable($item[1]) : 0,
                        'amount_part_plan' => isset($item[2]) ? self::checkingVariable($item[2]) : 0,
                        'cr_plan' => isset($item[3]) ? self::checkingVariable($item[3]) : 0,
                        'car_entry_plan' => isset($item[4]) ? self::checkingVariable($item[4]) : 0,
                        'car_entry_fact' => isset($item[4]) ? self::checkingVariable($item[5]) : 0,
                        //'illiquid_sales_plan' => isset($item[5]) ? self::checkingVariable($item[6]) : 0,
                    ]);
                }
            }
        }

        $range2 = "Сервис консультанты!A9:K";
        $data_work = $service->spreadsheets_values->get($spreadsheetId, $range2);
        $values_work = $data_work->getValues();
        //Заполнение планов и фактов сотрудников
        foreach ($values_work as $item){
            if(
                Fields::where('user_id', User::getUserId($item[0]))
                    ->where('month', date('m'))
                    ->first() != null
            ){
                Fields::where('user_id', User::getUserId($item[0]))
                    ->where('month', date('m'))
                    ->update([
                        'norm_hours_plan' =>  isset($item[1]) ? self::checkingVariable($item[1]) : 0,
                        'norm_hours_fact' =>  isset($item[2]) ? self::checkingVariable($item[2]) : 0,
                        'amount_part_plan' => isset($item[3]) ? self::checkingVariable($item[3]) : 0,
                        'amount_part_fact' => isset($item[4]) ? self::checkingVariable($item[4]) : 0,
                        'cr_plan' => isset($item[5]) ? self::checkingVariable($item[5]) : 0,
                        'cr_fact' => isset($item[6]) ? self::checkingVariable($item[6]) : 0,
                        'car_entry_plan' => isset($item[7]) ? self::checkingVariable($item[7]) : 0,
                        'car_entry_fact' => isset($item[8]) ? self::checkingVariable($item[8]) : 0,
                        //'illiquid_sales_plan' => isset($item[9]) ? self::checkingVariable($item[9]) : 0,
                        //'illiquid_sales_fact' => isset($item[10]) ? self::checkingVariable($item[10]) : 0,
                    ]);
            }else{
                Fields::create([
                        'user_id' => User::getUserId($item[0]),
                        'month' => date('m'),
                        'norm_hours_plan' =>  isset($item[1]) ? self::checkingVariable($item[1]) : 0,
                        'norm_hours_fact' =>  isset($item[2]) ? self::checkingVariable($item[2]) : 0,
                        'amount_part_plan' => isset($item[3]) ? self::checkingVariable($item[3]) : 0,
                        'amount_part_fact' => isset($item[4]) ? self::checkingVariable($item[4]) : 0,
                        'cr_plan' => isset($item[5]) ? self::checkingVariable($item[5]) : 0,
                        'cr_fact' => isset($item[6]) ? self::checkingVariable($item[6]) : 0,
                        'car_entry_plan' => isset($item[7]) ? self::checkingVariable($item[7]) : 0,
                        'car_entry_fact' => isset($item[8]) ? self::checkingVariable($item[8]) : 0,
                        //'illiquid_sales_plan' => isset($item[9]) ? self::checkingVariable($item[9]) : 0,
                        //'illiquid_sales_fact' => isset($item[10]) ? self::checkingVariable($item[10]) : 0,
                    ]);
            }
        }

        return true;
    }

    public function getServiceWorkshop(){
        $service = new Google_Service_Sheets((new GoogleTable())->getClient());

        $spreadsheetId = '1_QIz4c2_Er0D2Iyab_FYt20lF6WkLEMW6mHXonmY5CA';
        $range = "Сервис цех!A3:F4";

        $data = $service->spreadsheets_values->get($spreadsheetId, $range);
        $values = $data->getValues();

        // Заполнение планов на текущий год и месяц
        foreach ($values as $item){
            if($item[0] == 'Текущий год'){
                if(
                    Plan::where('dep', 'service_mech')
                        ->where('year', date('Y'))
                        ->where('month', 1)
                        ->first() != null
                ){
                    Plan::where('dep', 'service_mech')
                        ->where('year', date('Y'))
                        ->where('month', 1)
                        ->update([
                            'norm_hours_plan' => isset($item[1]) ? self::checkingVariable($item[1]) : 0,
                            'amount_part_plan' => isset($item[2]) ? self::checkingVariable($item[2]) : 0,
                            'cr_plan' => isset($item[3]) ? self::checkingVariable($item[3]) : 0,
                            'car_entry_plan' => isset($item[4]) ? self::checkingVariable($item[4]) : 0,
                            'illiquid_sales_plan' => isset($item[5]) ? self::checkingVariable($item[5]) : 0,
                        ]);
                }else{
                    Plan::create([
                        'type' => 'year',
                        'dep' => 'service_mech',
                        'year' => date('Y'),
                        'month' => 1,
                        'norm_hours_plan' => isset($item[1]) ? self::checkingVariable($item[1]) : 0,
                        'amount_part_plan' => isset($item[2]) ? self::checkingVariable($item[2]) : 0,
                        'cr_plan' => isset($item[3]) ? self::checkingVariable($item[3]) : 0,
                        'car_entry_plan' => isset($item[4]) ? self::checkingVariable($item[4]) : 0,
                        'illiquid_sales_plan' => isset($item[5]) ? self::checkingVariable($item[5]) : 0,
                    ]);
                }

            }
            if($item[0] == 'Текущий месяц'){
                if(
                    Plan::where('dep', 'service_mech')
                        ->where('year', date('Y'))
                        ->where('month', date('m'))
                        ->first() != null
                ){
                    Plan::where('dep', 'service_mech')
                        ->where('year', date('Y'))
                        ->where('month', date('m'))
                        ->update([
                            'norm_hours_plan' => isset($item[1]) ? self::checkingVariable($item[1]) : 0,
                            'amount_part_plan' => isset($item[2]) ? self::checkingVariable($item[2]) : 0,
                            'cr_plan' => isset($item[3]) ? self::checkingVariable($item[3]) : 0,
                            'car_entry_plan' => isset($item[4]) ? self::checkingVariable($item[4]) : 0,
                            'illiquid_sales_plan' => isset($item[5]) ? self::checkingVariable($item[5]) : 0,
                        ]);
                }else{
                    Plan::create([
                        'type' => 'month',
                        'dep' => 'service_mech',
                        'year' => date('Y'),
                        'month' => date('m'),
                        'norm_hours_plan' => isset($item[1]) ? self::checkingVariable($item[1]) : 0,
                        'amount_part_plan' => isset($item[2]) ? self::checkingVariable($item[2]) : 0,
                        'cr_plan' => isset($item[3]) ? self::checkingVariable($item[3]) : 0,
                        'car_entry_plan' => isset($item[4]) ? self::checkingVariable($item[4]) : 0,
                        'illiquid_sales_plan' => isset($item[5]) ? self::checkingVariable($item[5]) : 0,
                    ]);
                }
            }
        }

        $range2 = "Сервис цех!A9:K";
        $data_work = $service->spreadsheets_values->get($spreadsheetId, $range2);
        $values_work = $data_work->getValues();
        //Заполнение планов и фактов сотрудников
        foreach ($values_work as $item) {

            if (
                Fields::where('user_id', User::getUserId($item[0]))
                    ->where('month', date('m'))
                    ->first() != null
            ) {
                Fields::where('user_id', User::getUserId($item[0]))
                    ->where('month', date('m'))
                    ->update([
                        'norm_hours_plan' =>  isset($item[1]) ? self::checkingVariable($item[1]) : 0,
                        'norm_hours_fact' =>  isset($item[2]) ? self::checkingVariable($item[2]) : 0,
                        'amount_part_plan' => isset($item[3]) ? self::checkingVariable($item[3]) : 0,
                        'amount_part_fact' => isset($item[4]) ? self::checkingVariable($item[4]) : 0,
                        'cr_plan' => isset($item[5]) ? self::checkingVariable($item[5]) : 0,
                        'cr_fact' => isset($item[6]) ? self::checkingVariable($item[6]) : 0,
                        'car_entry_plan' => isset($item[7]) ? self::checkingVariable($item[7]) : 0,
                        'car_entry_fact' => isset($item[8]) ? self::checkingVariable($item[8]) : 0,
                        'illiquid_sales_plan' => isset($item[9]) ? self::checkingVariable($item[9]) : 0,
                        'illiquid_sales_fact' => isset($item[10]) ? self::checkingVariable($item[10]) : 0,
                    ]);
            } else {
                Fields::create([
                    'user_id' => User::getUserId($item[0]),
                    'month' => date('m'),
                    'norm_hours_plan' =>  isset($item[1]) ? self::checkingVariable($item[1]) : 0,
                    'norm_hours_fact' =>  isset($item[2]) ? self::checkingVariable($item[2]) : 0,
                    'amount_part_plan' => isset($item[3]) ? self::checkingVariable($item[3]) : 0,
                    'amount_part_fact' => isset($item[4]) ? self::checkingVariable($item[4]) : 0,
                    'cr_plan' => isset($item[5]) ? self::checkingVariable($item[5]) : 0,
                    'cr_fact' => isset($item[6]) ? self::checkingVariable($item[6]) : 0,
                    'car_entry_plan' => isset($item[7]) ? self::checkingVariable($item[7]) : 0,
                    'car_entry_fact' => isset($item[8]) ? self::checkingVariable($item[8]) : 0,
                    'illiquid_sales_plan' => isset($item[9]) ? self::checkingVariable($item[9]) : 0,
                    'illiquid_sales_fact' => isset($item[10]) ? self::checkingVariable($item[10]) : 0,
                ]);
            }
        }

        return true;
    }

    public function getOpna(){
        $service = new Google_Service_Sheets((new GoogleTable())->getClient());

        $spreadsheetId = '1_QIz4c2_Er0D2Iyab_FYt20lF6WkLEMW6mHXonmY5CA';
        $range = "ОПНА!A3:F4";

        $data = $service->spreadsheets_values->get($spreadsheetId, $range);
        $values = $data->getValues();

        // Заполнение планов на текущий год и месяц
        foreach ($values as $item){
            if($item[0] == 'Текущий год'){
                if(
                    Plan::where('dep', 'new')
                        ->where('year', date('Y'))
                        ->where('month', 1)
                        ->first() != null
                ){
                    Plan::where('dep', 'new')
                        ->where('year', date('Y'))
                        ->where('month', 1)
                        ->update([
                            'sales_plan' => isset($item[1]) ? self::checkingVariable($item[1]) : 0,
                            'acc_plan' => isset($item[2]) ? self::checkingVariable($item[2]) : 0,
                            'cr_plan' => isset($item[3]) ? self::checkingVariable($item[3]) : 0,
                            'tradein_plan' => isset($item[4]) ? self::checkingVariable($item[4]) : 0,
                        ]);
                }else{
                    Plan::create([
                        'type' => 'year',
                        'dep' => 'new',
                        'year' => date('Y'),
                        'month' => 1,
                        'sales_plan' => isset($item[1]) ? self::checkingVariable($item[1]) : 0,
                        'acc_plan' => isset($item[2]) ? self::checkingVariable($item[2]) : 0,
                        'cr_plan' => isset($item[3]) ? self::checkingVariable($item[3]) : 0,
                        'tradein_plan' => isset($item[4]) ? self::checkingVariable($item[4]) : 0,
                    ]);
                }

            }
            if($item[0] == 'Текущий месяц'){
                if(
                    Plan::where('dep', 'new')
                        ->where('year', date('Y'))
                        ->where('month', date('m'))
                        ->first() != null
                ){
                    Plan::where('dep', 'new')
                        ->where('year', date('Y'))
                        ->where('month', date('m'))
                        ->update([
                            'sales_plan' => isset($item[1]) ? self::checkingVariable($item[1]) : 0,
                            'acc_plan' => isset($item[2]) ? self::checkingVariable($item[2]) : 0,
                            'cr_plan' => isset($item[3]) ? self::checkingVariable($item[3]) : 0,
                            'tradein_plan' => isset($item[4]) ? self::checkingVariable($item[4]) : 0,
                        ]);
                }else{
                    Plan::create([
                        'type' => 'month',
                        'dep' => 'new',
                        'year' => date('Y'),
                        'month' => date('m'),
                        'sales_plan' => isset($item[1]) ? self::checkingVariable($item[1]) : 0,
                        'acc_plan' => isset($item[2]) ? self::checkingVariable($item[2]) : 0,
                        'cr_plan' => isset($item[3]) ? self::checkingVariable($item[3]) : 0,
                        'tradein_plan' => isset($item[4]) ? self::checkingVariable($item[4]) : 0,
                    ]);
                }
            }
        }

        $range2 = "ОПНА!A9:K15";
        $data_work = $service->spreadsheets_values->get($spreadsheetId, $range2);
        $values_work = $data_work->getValues();

        //Заполнение планов и фактов сотрудников
        foreach ($values_work as $item) {

            if (
                Fields::where('user_id', User::getUserId($item[0]))
                    ->where('month', date('m'))
                    ->first() != null
            ) {
                Fields::where('user_id', User::getUserId($item[0]))
                    ->where('month', date('m'))
                    ->update([
                        'sales_plan' =>  isset($item[1]) ? self::checkingVariable($item[1]) : 0,
                        'sales_fact' =>  isset($item[2]) ? self::checkingVariable($item[2]) : 0,
                        'acc_plan' => isset($item[3]) ? self::checkingVariable($item[3]) : 0,
                        'acc_fact' => isset($item[4]) ? self::checkingVariable($item[4]) : 0,
                        'cr_plan' => isset($item[5]) ? self::checkingVariable($item[5]) : 0,
                        'cr_fact' => isset($item[6]) ? self::checkingVariable($item[6]) : 0,
                        'tradein_plan' => isset($item[7]) ? self::checkingVariable($item[7]) : 0,
                        'tradein_fact' => isset($item[8]) ? self::checkingVariable($item[8]) : 0,
                    ]);
            } else {
                Fields::create([
                    'user_id' => User::getUserId($item[0]),
                    'month' => date('m'),
                    'sales_plan' =>  isset($item[1]) ? self::checkingVariable($item[1]) : 0,
                    'sales_fact' =>  isset($item[2]) ? self::checkingVariable($item[2]) : 0,
                    'acc_plan' => isset($item[3]) ? self::checkingVariable($item[3]) : 0,
                    'acc_fact' => isset($item[4]) ? self::checkingVariable($item[4]) : 0,
                    'cr_plan' => isset($item[5]) ? self::checkingVariable($item[5]) : 0,
                    'cr_fact' => isset($item[6]) ? self::checkingVariable($item[6]) : 0,
                    'tradein_plan' => isset($item[7]) ? self::checkingVariable($item[7]) : 0,
                    'tradein_fact' => isset($item[8]) ? self::checkingVariable($item[8]) : 0,
                ]);
            }
        }

        return true;
    }

    public function getOpasp(){
        $service = new Google_Service_Sheets((new GoogleTable())->getClient());

        $spreadsheetId = '1_QIz4c2_Er0D2Iyab_FYt20lF6WkLEMW6mHXonmY5CA';
        $range = "ОПАСП!A3:F4";

        $data = $service->spreadsheets_values->get($spreadsheetId, $range);
        $values = $data->getValues();

        // Заполнение планов на текущий год и месяц
        foreach ($values as $item){
            if($item[0] == 'Текущий год'){
                if(
                    Plan::where('dep', 'bu')
                        ->where('year', date('Y'))
                        ->where('month', 1)
                        ->first() != null
                ){
                    Plan::where('dep', 'bu')
                        ->where('year', date('Y'))
                        ->where('month', 1)
                        ->update([
                            'sales_plan' => isset($item[1]) ? self::checkingVariable($item[1]) : 0,
                            'acc_plan' => isset($item[2]) ? self::checkingVariable($item[2]) : 0,
                            'cr_plan' => isset($item[3]) ? self::checkingVariable($item[3]) : 0,
                            'tradein_plan' => isset($item[4]) ? self::checkingVariable($item[4]) : 0,
                        ]);
                }else{
                    Plan::create([
                        'type' => 'year',
                        'dep' => 'bu',
                        'year' => date('Y'),
                        'month' => 1,
                        'sales_plan' => isset($item[1]) ? self::checkingVariable($item[1]) : 0,
                        'acc_plan' => isset($item[2]) ? self::checkingVariable($item[2]) : 0,
                        'cr_plan' => isset($item[3]) ? self::checkingVariable($item[3]) : 0,
                        'tradein_plan' => isset($item[4]) ? self::checkingVariable($item[4]) : 0,
                    ]);
                }

            }
            if($item[0] == 'Текущий месяц'){
                if(
                    Plan::where('dep', 'bu')
                        ->where('year', date('Y'))
                        ->where('month', date('m'))
                        ->first() != null
                ){
                    Plan::where('dep', 'bu')
                        ->where('year', date('Y'))
                        ->where('month', date('m'))
                        ->update([
                            'sales_plan' => isset($item[1]) ? self::checkingVariable($item[1]) : 0,
                            'acc_plan' => isset($item[2]) ? self::checkingVariable($item[2]) : 0,
                            'cr_plan' => isset($item[3]) ? self::checkingVariable($item[3]) : 0,
                            'tradein_plan' => isset($item[4]) ? self::checkingVariable($item[4]) : 0,
                        ]);
                }else{
                    Plan::create([
                        'type' => 'month',
                        'dep' => 'bu',
                        'year' => date('Y'),
                        'month' => date('m'),
                        'sales_plan' => isset($item[1]) ? self::checkingVariable($item[1]) : 0,
                        'acc_plan' => isset($item[2]) ? self::checkingVariable($item[2]) : 0,
                        'cr_plan' => isset($item[3]) ? self::checkingVariable($item[3]) : 0,
                        'tradein_plan' => isset($item[4]) ? self::checkingVariable($item[4]) : 0,
                    ]);
                }
            }
        }

        $range2 = "ОПАСП!A9:K";
        $data_work = $service->spreadsheets_values->get($spreadsheetId, $range2);
        $values_work = $data_work->getValues();

        //Заполнение планов и фактов сотрудников
        foreach ($values_work as $item) {

            if (
                Fields::where('user_id', User::getUserId($item[0]))
                    ->where('month', date('m'))
                    ->first() != null
            ) {
                Fields::where('user_id', User::getUserId($item[0]))
                    ->where('month', date('m'))
                    ->update([
                        'sales_plan' =>  isset($item[1]) ? self::checkingVariable($item[1]) : 0,
                        'sales_fact' =>  isset($item[2]) ? self::checkingVariable($item[2]) : 0,
                        'acc_plan' => isset($item[3]) ? self::checkingVariable($item[3]) : 0,
                        'acc_fact' => isset($item[4]) ? self::checkingVariable($item[4]) : 0,
                        'cr_plan' => isset($item[5]) ? self::checkingVariable($item[5]) : 0,
                        'cr_fact' => isset($item[6]) ? self::checkingVariable($item[6]) : 0,
                        'tradein_plan' => isset($item[7]) ? self::checkingVariable($item[7]) : 0,
                        'tradein_fact' => isset($item[8]) ? self::checkingVariable($item[8]) : 0,
                    ]);
            } else {
                Fields::create([
                    'user_id' => User::getUserId($item[0]),
                    'month' => date('m'),
                    'sales_plan' =>  isset($item[1]) ? self::checkingVariable($item[1]) : 0,
                    'sales_fact' =>  isset($item[2]) ? self::checkingVariable($item[2]) : 0,
                    'acc_plan' => isset($item[3]) ? self::checkingVariable($item[3]) : 0,
                    'acc_fact' => isset($item[4]) ? self::checkingVariable($item[4]) : 0,
                    'cr_plan' => isset($item[5]) ? self::checkingVariable($item[5]) : 0,
                    'cr_fact' => isset($item[6]) ? self::checkingVariable($item[6]) : 0,
                    'tradein_plan' => isset($item[7]) ? self::checkingVariable($item[7]) : 0,
                    'tradein_fact' => isset($item[8]) ? self::checkingVariable($item[8]) : 0,
                ]);
            }
        }

        return true;
    }


    public function checkingVariable($variable){

        if(isset($variable) && $variable != ''){

            $variable = str_replace (',','.',$variable);

            return $variable;
        }else{
            $variable = 0;
            return (int)$variable;
        }
    }
}
