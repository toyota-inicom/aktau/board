<?php

namespace App\Http\Controllers;

use App\Models\Plan;
use App\Models\User;
use Google_Service_Sheets;

class BuyController extends Controller
{
    public function percent($from, $to)
    {
        if ((int) $to) {
            return round((100 * $from) / $to);
        }
        return 0;
    }

    public function countBests(User $user)
    {
        return $user->all_fields->where('best', true)->count();
    }

    public function gUpAll()
    {
        $service = new Google_Service_Sheets((new GoogleTable())->getClient());
        $spreadsheetId = '1pI9tsIokfoJFG5I8ODyPUE-uJO3sIvp3qFSH5xYd-to';
        $params = ['valueInputOption' => 'RAW'];
        $list = "ОВАСП";

        $users = User::whereDep('buy')->orWhereIn('ext_id', [
            397, //Рязанов Дмитрий
            432, //Амержанов Руслан
            424, //Павленок Михаил
            112, //Лысенко Владимир
        ])->with('year_fields')->get();
        $fs = $users->pluck('year_fields')->flatten();
        $values = [];

        foreach ($fs->groupBy('month') as $f) {
            $values[] = [
                $f->sum('aspsale_fact'),
                $f->sum('asprequest_fact'),
                $f->sum('trafic_fact'),
                $f->sum('standarts'),
                $f->sum('crm'),
            ];
        }

        $body = new \Google_Service_Sheets_ValueRange(['values' => $values]);
        $range = "{$list}!C39";
        $service->spreadsheets_values->update($spreadsheetId, $range, $body, $params);
    }

    public function uploadToGoogle()
    {
        $data = $this->getPrevM();
        $service = new \Google_Service_Sheets((new GoogleTable())->getClient());
        $spreadsheetId = '1V0EduVPPJ1WDZL5u7DxLIa9Am3x32iI3jjZv2LyxlCI';
        $users = User::whereDep('buy')->with("fields", "all_fields", "year_fields")->get();
        $params = ['valueInputOption' => 'RAW'];
        $list = "Приём";

        $users = $users->sortByDesc("fields.eff");
        $key = 0;
        $valuesM = [];
        foreach ($data['workers'] as $user) {
            $arrow = "";
            if (!empty($user["arrow"])) {
                $arrow = $user["arrow"] === "red" ? "↓" : "↑";
            }
            $valuesM[] = [
                $user["name"],
                $arrow,
                $user["total_efficiency"],
                $user["total_percent"],
                $user["aspsale_fact"],
                $user["asprequest_fact"],
                $user["withdrawal_fact"],
				$user["staging_fact"],
				$user["trafic_fact"],
            ];
        }
        $body = new \Google_Service_Sheets_ValueRange(['values' => $valuesM]);
        $range = "{$list}!A" . ($key + 3);
        $service->spreadsheets_values->update($spreadsheetId, $range, $body, $params);

        $key = 17;
        $valuesYCollection = collect();

        foreach ($users as $user) {
            $fs = $user->all_fields;
                 $valuesYCollection->push(collect([
                "name" => $user->name,
                "arrow" => "",
                "aspsale_fact" => $fs->sum('aspsale_fact'),
                "asprequest_fact" => $fs->sum('asprequest_fact'),
				"withdrawal_fact" => $fs->sum('withdrawal_fact'),
				"staging_fact" => $fs->sum('staging_fact'),
				"trafic_fact" => $fs->sum('trafic_fact'),
				"best" => $this->countBests($user),
            ]));
        }

        $valuesYCollection = $valuesYCollection->sortByDesc("best");
        $valuesY = [];
        foreach ($valuesYCollection as $item) {
            $valuesY[] = $item->values();
        }
        $body = new \Google_Service_Sheets_ValueRange(['values' => $valuesY]);
        $range = "{$list}!A" . $key;
        $service->spreadsheets_values->update($spreadsheetId, $range, $body, $params);

        $valuesYAll = [];
        foreach ($users as $user) {
            $row = [];
            $row[] = $user->name;
            $row[] = "";
            for ($i = 1; $i <= (int) date('m'); $i++) {
                $yrf = $user->year_fields->where('month', $i)->where('year',date('Y'));
                $row[] = $yrf->count() ? $yrf->first()->eff : 0;
            }

            // foreach ($user->year_fields as $field) {
            //     $row[] = $field->eff;
            // }
            $valuesYAll[] = $row;
        }

        $key = 31;
        $body = new \Google_Service_Sheets_ValueRange(['values' => $valuesYAll]);
        $range = "{$list}!A" . $key;
        $service->spreadsheets_values->update($spreadsheetId, $range, $body, $params);

    }

    public function getPrevM()
    {
        $workers = User::whereActive(true)
            ->where('dep', 'buy')
            ->with('p_fields', 'all_fields')
            ->get();

        $traffic_sum = $workers->sum('p_fields.trafic_fact');
        $summAccess = $workers->sum('p_fields.acc_fact');
        $allSales = $workers->sum('p_fields.sales_fact');
        $allTo = $workers->sum('p_fields.topack_fact');
        $allKasko = $workers->sum('p_fields.kasko_fact');
        $ogpo_sum = $workers->sum('p_fields.ogpo_fact');
        $aspsaleSum = $workers->sum('p_fields.aspsale_fact');

        $workerList = collect();

        foreach ($workers as $worker) {
			
            $output_workers = collect();
            $fs = $worker->p_fields;
            if(!$fs) continue;
            $aspsaleP = $this->percent($fs->aspsale_fact, $fs->aspsale_plan);
            $asprequestP = $this->percent($fs->asprequest_fact, $fs->asprequest_plan);

            $trafic_fact = $fs->trafic_fact;
            $trafic_percent = ($trafic_fact != 0 && $traffic_sum != 0) ? (100 * $trafic_fact) / $traffic_sum : 0;

            $total_percent = ($aspsaleP + $asprequestP + $trafic_percent) / 3;
            /* Процент эффективности сотрудника */

            $total_efficiency = round((
                //Доля Трафик = 100 * Трафик сотрудника / Общий трафик
                $this->percent($fs->trafic_fact, $traffic_sum)+

                //Прием  = 100 * Прием сотрудника/ количество оценок  
                $this->percent($fs->aspsale_fact, $workers->sum('fields.aspsale_fact'))+

                //Выкуп доля  =100*количество личных выкупов / приемы
                $this->percent($fs->staging_fact, $fs->aspsale_fact)+

                //Доля  оценок  =100* Количество оценок / Общее количество
                $this->percent($fs->asprequest_fact, $workers->sum('fields.asprequest_fact'))+

                //% выполнения стандартов = данные с таблицы уже в %
                $fs->withdrawal_fact+

                //Доля принятых авто = 100 * прием сотрудника /  на oобщий прием
                $this->percent($fs->aspsale_fact, $workers->sum('fields.aspsale_fact'))
            ) / 6, 0);

            $output_workers['image'] = '/images/workers/' . str_slug($worker->name) . '.png';
            $output_workers['image_m'] = '/images/workers/' . str_slug($worker->name) . '_220x265.png';
            $output_workers['image_l'] = '/images/workers/' . str_slug($worker->name) . '_375x440.png';
            $output_workers['name'] = $worker->name;
            $output_workers['trafic_fact'] = $fs->trafic_fact;
            $output_workers['total_percent'] = round($total_percent, 0);
            $output_workers['total_efficiency'] = $total_efficiency;
            $output_workers['position'] = $worker->position;
            $output_workers['rate'] = $worker->rate;
            $output_workers['aspsale_fact'] = $fs->aspsale_fact;
            $output_workers['aspsale_plan'] = $fs->aspsale_plan;
            $output_workers['asprequest_fact'] = $fs->asprequest_fact;
            $output_workers['asprequest_plan'] = $fs->asprequest_plan;
            $output_workers['withdrawal_fact'] = $fs->withdrawal_fact;
            $output_workers['withdrawal_plan'] = $fs->withdrawal_plan;
            $output_workers['staging_fact'] = $fs->staging_fact;
            $output_workers['crm'] = $fs->crm;

            $output_workers['id'] = $worker->ext_id;
            $workerList->push($output_workers);
        }

        //arrows
        $workerListSorted = $workerList->sortByDesc('total_efficiency');
        $timedKey = 0;
        foreach ($workerListSorted as $i => &$w) {
            $wItem = $workers->where('ext_id', $w['id'])->first();
            $w['arrow'] = $w['position'] >= $timedKey ? 'green' : 'red';
            if ($w['position'] > $timedKey) {
                $w['arrow'] = 'green';
            }
            if ($w['position'] < $timedKey) {
                $w['arrow'] = 'red';
            }
            if ($w['position'] === $timedKey) {
                $w['arrow'] = $wItem->arrow;
            }

            if ($timedKey === 0) {
                $wItem->p_fields->best = true;
            } else {
                $wItem->p_fields->best = false;
            }

            $wItem->p_fields->eff = $w['total_efficiency'];
            $wItem->position = $timedKey;
            $wItem->arrow = $w['arrow'];
            $wItem->p_fields->save();
            $wItem->save();
            $timedKey++;
        }
        // end arrows

        $yearPlan = Plan::whereDep('buy')
            ->where('year', date('Y'))
            ->where('type', 'year')
            ->first();
        $monthPlan = Plan::whereDep('buy')
            ->where('month', (int) date('m'))
            ->where('year', date('Y'))
            ->where('type', 'month')
            ->first();

        $plan = [
            'y' => $yearPlan,
            'm' => $monthPlan,
        ];

        $addUsers = User::whereIn('ext_id', [
            397, //Рязанов Дмитрий
            432, //Амержанов Руслан
            424, //Павленок Михаил
            112, //Лысенко Владимир
        ])->with('all_fields', 'p_fields')->get();

        $allFieldsAdd = $addUsers->pluck('all_fields')->flatten();

        $allFields = $workers->pluck('all_fields')->flatten();
        $fact = [
            'y' => [
                'trafic' => $allFields->sum('trafic_fact'),
                'aspsale' => $allFields->sum('aspsale_fact') + $allFieldsAdd->sum('aspsale_fact'),
                'asprequest' => $allFields->sum('asprequest_fact') + $allFieldsAdd->sum('asprequest_fact'),
                'crm' => $allFields->sum('crm'),
                'withdrawal' => $allFields->sum('withdrawal_fact'),
            ],
            'm' => [
                'trafic' => $workers->sum('p_fields.trafic_fact'),
                'aspsale' => $workers->sum('p_fields.aspsale_fact') + $addUsers->sum('p_fields.aspsale_fact'),
                'asprequest' => $workers->sum('p_fields.asprequest_fact') + $addUsers->sum('p_fields.asprequest_fact'),
                'crm' => $workers->sum('p_fields.crm'),
                'withdrawal' => $workers->sum('p_fields.withdrawal_fact'),
            ],
        ];
        $monthPercent = round($workerList->sum('total_percent') / $workerList->count());
        $yPercent = round((
            ((100 * $fact['y']['aspsale']) / $plan['y']['aspsale_plan']) +
            ((100 * $fact['y']['asprequest']) / $plan['y']['aspsale_plan'])
        ) / 2);

        return [
            'workers' => $workerListSorted->toArray(),
            'plan' => $plan,
            'fact' => $fact,
            'yPercent' => $yPercent,
            'monthPercent' => $monthPercent,
            'recordPercent' => $allFields->max('eff'),
        ];
    }

    public function get()
    {
        $workers = User::whereActive(true)
            ->where('dep', 'buy')
            ->with('fields', 'all_fields')
            ->get();
        foreach ($workers as $worker) {
            $worker->fields()->firstOrCreate();
            $worker->load('fields');
        }
        $traffic_sum = $workers->sum('fields.trafic_fact');

        $workerList = collect();

        foreach ($workers as $worker) {
            $output_workers = collect();
            $fs = $worker->fields;

            $aspsaleP = $this->percent($fs->aspsale_fact, $fs->aspsale_plan);
            $asprequestP = $this->percent($fs->asprequest_fact, $fs->asprequest_plan);

            $trafic_fact = $fs->trafic_fact;
            $trafic_percent = ($trafic_fact != 0 && $traffic_sum != 0) ? (100 * $trafic_fact) / $traffic_sum : 0;

            $total_percent = ($aspsaleP + $asprequestP + $trafic_percent) / 3;
            /* Процент эффективности сотрудника */

			//Прием + выкуп  + обмен +  прием доля +выкуп доля+ доля оценок +обмен доля/7
            $total_efficiency = round((

                    //Прием  = 100 * Прием сотрудника/ количество личных оценок
                    $this->percent($fs->aspsale_fact, $fs->asprequest_fact)+

                    //Выкуп =100*прием сотрудника /выкуп личный
                    $this->percent($fs->aspsale_fact,  $fs->staging_fact)+

                    //Обмен =100*прием сотрудника/ обмен личный
                    $this->percent($fs->aspsale_fact,  $fs->obmen_fact)+

                    //Доля принятых авто = 100 * прием сотрудника /  на общий прием
                    $this->percent($fs->aspsale_fact, $workers->sum('fields.aspsale_fact'))+

                    //Выкуп доля = 100*количество личных выкупов / количество общих выкупов.
                    $this->percent($fs->staging_fact, $workers->sum('fields.staging_fact'))+

                    //Доля  оценок  =100* Количество оценок / Общее количество
                    $this->percent($fs->asprequest_fact, $workers->sum('fields.asprequest_fact'))+

                    //Обмен доля = 100*количество личных обменов/количество общих обменов.
                    $this->percent($fs->obmen_fact, $workers->sum('fields.obmen_fact'))

                ) / 7, 0);
			
           /* $total_efficiency = round((

                //Прием  = 100 * Прием сотрудника/ количество оценок
                //Прием  = 100 * Прием сотрудника/ количество личных оценок
                //$this->percent($fs->aspsale_fact, $workers->sum('fields.aspsale_fact'))+
                $this->percent($fs->aspsale_fact, $fs->asprequest_fact)+

                //Выкуп доля  =100*количество личных выкупов / приемы
                //$this->percent($fs->staging_fact, $fs->aspsale_fact)+
                //Выкуп доля  =100*количество личных выкупов / количество общих выкупов.
                $this->percent($fs->staging_fact,  $workers->sum('fields.staging_fact'))+

                //Доля обмен =100* коллисество обмена /количества приемов сотрудника
                //Обмен доля = 100*количество личных обменов/количество общих обменов.
                $this->percent($fs->obmen_fact, $workers->sum('fields.obmen_fact'))+

                //Доля  оценок  =100* Количество оценок / Общее количество
                $this->percent($fs->asprequest_fact, $workers->sum('fields.asprequest_fact'))+

                //Доля Трафик = 100 * Трафик сотрудника / Общий трафик
                $this->percent($fs->trafic_fact, $traffic_sum)+

                //% выполнения стандартов = данные с таблицы уже в %
                $fs->withdrawal_fact+

                //Доля принятых авто = 100 * прием сотрудника /  на oобщий прием
                $this->percent($fs->aspsale_fact, $workers->sum('fields.aspsale_fact'))
            ) / 7, 0);*/

            $output_workers['image'] = '/images/workers/' . str_slug($worker->name) . '.png';
            $output_workers['image_m'] = '/images/workers/' . str_slug($worker->name) . '_220x265.png';
            $output_workers['image_l'] = '/images/workers/' . str_slug($worker->name) . '_375x440.png';
            $output_workers['name'] = $worker->name;
            $output_workers['trafic_fact'] = $fs->trafic_fact;
            $output_workers['total_percent'] = round($total_percent, 0);
            $output_workers['total_efficiency'] = $total_efficiency;
            $output_workers['position'] = $worker->position;
            $output_workers['rate'] = $worker->rate;
            $output_workers['aspsale_fact'] = $fs->aspsale_fact;
            $output_workers['aspsale_plan'] = $fs->aspsale_plan;
            $output_workers['asprequest_fact'] = $fs->asprequest_fact;
            $output_workers['asprequest_plan'] = $fs->asprequest_plan;
            $output_workers['withdrawal_fact'] = $fs->withdrawal_fact;
            $output_workers['withdrawal_plan'] = $fs->withdrawal_plan;
            $output_workers['obmen_plan'] = $fs->obmen_plan;
            $output_workers['obmen_fact'] = $fs->obmen_fact;
            $output_workers['staging_fact'] = $fs->staging_fact;
            $output_workers['crm'] = $fs->crm;

            $output_workers['id'] = $worker->ext_id;
            $workerList->push($output_workers);
        }

        //arrows
        $workerListSorted = $workerList->sortByDesc('total_efficiency');
        $timedKey = 0;
        foreach ($workerListSorted as $i => &$w) {
            $wItem = $workers->where('ext_id', $w['id'])->first();
            $w['arrow'] = $w['position'] >= $timedKey ? 'green' : 'red';
            if ($w['position'] > $timedKey) {
                $w['arrow'] = 'green';
            }
            if ($w['position'] < $timedKey) {
                $w['arrow'] = 'red';
            }
            if ($w['position'] === $timedKey) {
                $w['arrow'] = $wItem->arrow;
            }

            if ($timedKey === 0) {
                $wItem->fields->best = true;
            } else {
                $wItem->fields->best = false;
            }

            $wItem->fields->eff = $w['total_efficiency'];
            $wItem->position = $timedKey;
            $wItem->arrow = $w['arrow'];
            $wItem->fields->save();
            $wItem->save();
            $timedKey++;
        }
        // end arrows

        $yearPlan = Plan::whereDep('buy')
            ->where('year', date('Y'))
            ->where('type', 'year')
            ->first();
        $monthPlan = Plan::whereDep('buy')
            ->where('month', (int) date('m'))
            ->where('year', date('Y'))
            ->where('type', 'month')
            ->first();

        $plan = [
            'y' => $yearPlan,
            'm' => $monthPlan,
        ];

        $addUsers = User::whereIn('ext_id', [
            397, //Рязанов Дмитрий
            112, //Лысенко Владимир
        ])->with('all_fields', 'fields')->get();

        $allFieldsAdd = $addUsers->pluck('all_fields')->flatten();

        $allFields = $workers->pluck('all_fields')->flatten();
        $fact = [
            'y' => [
                'trafic' => $allFields->sum('trafic_fact'),
                'aspsale' => $allFields->sum('aspsale_fact') + $allFieldsAdd->sum('aspsale_fact'),
                'asprequest' => $allFields->sum('asprequest_fact') + $allFieldsAdd->sum('asprequest_fact'),
                'crm' => round($allFields->sum('crm') / $allFields->count()),
                'withdrawal' => $allFields->sum('withdrawal_fact'),
                'staging' => $allFields->sum('staging_fact'),
                'obmen' => $allFields->sum('obmen_fact'),
            ],
            'm' => [
                'trafic' => $workers->sum('fields.trafic_fact'),
                'aspsale' => $workers->sum('fields.aspsale_fact') + $addUsers->sum('fields.aspsale_fact'),
                'asprequest' => $workers->sum('fields.asprequest_fact') + $addUsers->sum('fields.asprequest_fact'),
                'crm' => round($workers->sum('fields.crm') / $workers->count()),
                'withdrawal' => $workers->sum('fields.withdrawal_fact'),
                'staging' => $workers->sum('fields.staging_fact'),
                'obmen' => $workers->sum('fields.obmen_fact'),
            ],
        ];
        $monthPercent = round($workerList->sum('total_percent') / $workerList->count());
        $yPercent = round((
            ((100 * $fact['y']['aspsale']) / $plan['y']['aspsale_plan']) +
            ((100 * $fact['y']['asprequest']) / $plan['y']['asprequest_plan']) +
            ((100 * $fact['y']['obmen']) / $plan['y']['obmen_plan'])
        ) / 3);

        return [
            'workers' => $workerListSorted->toArray(),
            'plan' => $plan,
            'fact' => $fact,
            'yPercent' => $yPercent,
            'monthPercent' => $monthPercent,
            'recordPercent' => $allFields->max('eff'),
        ];
    }
}
