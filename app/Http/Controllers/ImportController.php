<?php

namespace App\Http\Controllers;

use App\Models\User;
use Illuminate\Http\Request;

abstract class WareHouse
{
    const dpNew = "000000002";
    const dpBu  = "000000030";
}

class ImportController extends Controller
{

    public function importTestDrive(Request $r)
    {
		file_put_contents("import_crm.txt", $r->getContent(), FILE_APPEND);
        $user = User::where("ext_id", $r->user)->with('fields')->first();
        if ($user) {
            $user->fields()->firstOrCreate();
            $user->load('fields');
            $user->fields->testdrive_fact = $r->count;
            $user->fields->save();
        }
				
		return "ok";
    }

    public function import(Request $request)
    {
        $varMix = [
            "op_sales"      => "sales_fact",
            "op_acc"        => "acc_fact",
            "op_tradein"    => "tradein_fact",
            "op_to"         => "topack_fact",
            "op_aspsale"    => "aspsale_fact",
            "op_aspzayavka" => "asprequest_fact",
        ];
        $data = json_decode($request->getContent(), true);
        file_put_contents("import_data.txt", $request->getContent(), FILE_APPEND);
        file_put_contents("last_data.txt", $request->getContent());

        foreach ($data as $item) {
            $user = User::where("name", $item['worker'])->with('fields')->first();
            if (!$user) continue;
            $user->fields()->firstOrCreate();
            $user->load('fields');

            if (isset($item['op_sales']) && isset($item['warehouse'])) {
                if (
					($user->dep == "new_add" && $item['warehouse'] == WareHouse::dpNew)
                    || ($user->dep == "new" && $item['warehouse'] == WareHouse::dpNew)
                    || ($user->dep == "bu" && $item['warehouse'] == WareHouse::dpBu)
                ) {
                    $user->fields->sales_fact = $item['op_sales'];
                    $user->fields->save();
                }
            } else {
                foreach ($item as $field => $value) {
                    if ($field === "worker") continue;
                    if (!isset($user->fields[$varMix[$field]])) continue;

                    $user->fields->{$varMix[$field]} = $value;
                    $user->fields->save();
                }
            }
        }

        $opnaSales = 0;
        $opaspSales = 0;
        foreach ($data as $item) {
            $user = User::where("name", $item['worker'])->with('fields')->first();
            if (!$user){ 
				print_r($item['worker'] ."Нет в системе");
				continue;
			}	
            if (isset($item['op_sales']) && isset($item['warehouse'])) {
                if ($user->ext_id == 88) {
                    $opnaSales += $item['op_sales'];
                    continue;
                }
                if ($user->ext_id == 112) {
                    $opaspSales += $item['op_sales'];
                    continue;
                }

                    if ($item['warehouse'] == WareHouse::dpBu && $user->warehouse != WareHouse::dpBu) {
                        $opaspSales = $opaspSales + $item['op_sales'];
                    }
                    if ($item['warehouse'] == WareHouse::dpNew && $user->warehouse != WareHouse::dpNew) {
                        $opnaSales = $opnaSales + $item['op_sales'];
                        print_r($item['worker'] . " - " . $item['op_sales'] . " - Регине\n");
                    }
            }
        }


        if ($opnaSales > 0) {
            $user = User::whereExtId(88)->first();
            $user->fields()->firstOrCreate();
            $user->load('fields');
            $user->fields->sales_fact = $opnaSales;
            $user->fields->save();
        }

        if ($opaspSales > 0) {
            $user = User::whereExtId(112)->first();
            $user->fields()->firstOrCreate();
            $user->load('fields');
            $user->fields->sales_fact = $opaspSales;
            $user->fields->save();
        }

        return $opnaSales;
    }
}
