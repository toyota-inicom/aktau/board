<?php


namespace App\Http\Controllers;
use Google_Client;
use Google_Service_Sheets;

class GoogleTable
{
    /**
     * Returns an authorized API client.
     * @return Google_Client the authorized client object
     */

    public function getClient(){
        $client = new Google_Client();
        $client->useApplicationDefaultCredentials();
        $client->setScopes(Google_Service_Sheets::SPREADSHEETS);
        return $client;
    }
}