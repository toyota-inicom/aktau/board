<?php

namespace App\Http\Livewire\Admin;

use App\Models\Plan;
use App\Models\User;
use Livewire\Component;

class BuyPlan extends Component
{

    public $yearPlan;
    public $monthPlan;
    public $users;

    protected $rules = [
        'yearPlan.aspsale_plan' => 'required',
        'yearPlan.asprequest_plan' => 'required',
        'yearPlan.withdrawal_plan' => 'required',
        'monthPlan.aspsale_plan' => 'required',
        'monthPlan.asprequest_plan' => 'required',
        'monthPlan.withdrawal_plan' => 'required',
        'users.*.fields.aspsale_plan' => 'required',
        'users.*.fields.asprequest_plan' => 'required',
        'users.*.fields.crm' => 'required',
        'users.*.fields.withdrawal_fact' => 'required',
        'users.*.fields.withdrawal_plan' => 'required',
    ];

    public function save()
    {
        $this->yearPlan->save();
        $this->monthPlan->save();
        $this->users->each(function ($user) {
            $user->fields->save();
        });
    }

    public function mount()
    {
        $this->yearPlan = Plan::whereDep('buy')
            ->where('year', date('Y'))
            ->where('type', 'year')
            ->first();
        if (!$this->yearPlan) {
            $this->yearPlan = Plan::create([
                'dep' => 'buy',
                'type' => 'year',
                'year' => date('Y'),
            ]);
        }

        $this->monthPlan = Plan::whereDep('buy')
            ->where('month', (int) date('m'))
            ->where('year', date('Y'))
            ->where('type', 'month')
            ->first();
        if (!$this->monthPlan) {
            $this->monthPlan = Plan::create([
                'dep' => 'buy',
                'type' => 'month',
                'year' => date('Y'),
                'month' => (int) date('m'),
            ]);
        }

        $this->users = User::whereDep("buy")->where('active', true)->with('fields')->get();
    }

    public function render()
    {
        return view('livewire.admin.buy-plan');
    }
}
