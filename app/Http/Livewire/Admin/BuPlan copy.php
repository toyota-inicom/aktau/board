<?php

namespace App\Http\Livewire\Admin;

use App\Models\Plan;
use App\Models\User;
use Livewire\Component;

class BuPlan extends Component
{
    public $yearPlan;
    public $monthPlan;
    public $users;

    protected $rules = [
        'yearPlan.sales_plan'        => 'required',
        'yearPlan.acc_plan'          => 'required',
        'yearPlan.topack_plan'       => 'required',
        'yearPlan.kasko_plan'        => 'required',
        'yearPlan.ogpo_plan'         => 'required',
        'monthPlan.sales_plan'       => 'required',
        'monthPlan.acc_plan'         => 'required',
        'monthPlan.topack_plan'      => 'required',
        'monthPlan.kasko_plan'       => 'required',
        'monthPlan.ogpo_plan'        => 'required',
        'users.*.fields.sales_plan'  => 'required',
        'users.*.fields.acc_plan'    => 'required',
        'users.*.fields.topack_plan' => 'required',
        'users.*.fields.kasko_fact'  => 'required',
        'users.*.fields.kasko_plan'  => 'required',
        'users.*.fields.ogpo_plan'   => 'required',
        'users.*.fields.ogpo_fact'   => 'required',
    ];

    public function save()
    {
        $this->yearPlan->save();
        $this->monthPlan->save();
        $this->users->each(function ($user) {
            $user->fields->save();
        });
    }

    public function mount()
    {
        $this->yearPlan = Plan::whereDep('bu')
            ->where('year', date('Y'))
            ->where('type', 'year')
            ->first();
        if (!$this->yearPlan) {
            $this->yearPlan = Plan::create([
                'dep'  => 'bu',
                'type' => 'year',
                'year' => date('Y')
            ]);
        }

        $this->monthPlan = Plan::whereDep('bu')
            ->where('month', (int)date('m'))
            ->where('year', date('Y'))
            ->where('type', 'month')
            ->first();
        if (!$this->monthPlan) {
            $this->monthPlan = Plan::create([
                'dep' => 'bu',
                'type' => 'month',
                'year' => date('Y'),
                'month' => (int)date('m'),
            ]);
        }

        $this->users = User::whereDep("bu")->where('active', true)->with('fields')->get();
    }

    public function render()
    {
        return view('livewire.admin.bu-plan');
    }
}
