<?php

namespace App\Http\Livewire\Admin;

use App\Models\User;
use Livewire\Component;

class Users extends Component
{
    public $form = false;
    public $newUser = [
        'name'   => null,
        'ext_id' => 0,
        'dep'    => 0,
    ];

//    protected $rules = [
//        "newUser.name"   => "required",
//        "newUser.dep"    => "required",
//        "newUser.ext_id" => "required|gt:0|unique:users,ext_id",
//    ];
//
//    protected $messages = [
//        'newUser.name.required' => 'Имя и фамилия не могут быть пустыми',
//        'newUser.dep.required' => 'Отдел не может быть пустым',
//        'newUser.ext_id.gt:0' => 'ID должен быть больше нуля',
//        'newUser.ext_id.unique' => 'Пользователь с таким ID уже существует',
//    ];


    public function toggleForm()
    {
        $this->form = !$this->form;
    }

    public function toggleActive($user_id)
    {
        $user = User::find($user_id);
        $user->active = !$user->active;
        $user->save();
    }

    public function store()
    {
        $user = User::create($this->newUser);
        $user->fields()->firstOrCreate();
        $this->form = false;
    }

    public function remove($user_id)
    {
        User::find($user_id)->delete();
    }

    public function render()
    {
        return view('livewire.admin.users', [
            'users' => User::all()
        ]);
    }
}
