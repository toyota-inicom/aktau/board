<?php

namespace App\Http\Livewire\Admin;

use App\Models\Plan;
use App\Models\User;
use Livewire\Component;

class BuyPlan extends Component
{
    public $yearPlan;
    public $monthPlan;
    public $users;
    public $chiefs;
    public $depName = "buy";
    public $subName = "buy";

    protected function rules()
    {
        $rules = [];
        foreach (config($this->depName . '_fields.y') as $f) {
            if ($f) {
                $rules["yearPlan.{$f}"] = 'required';
            }
        }
        foreach (config($this->depName . '_fields.m') as $f) {
            if ($f) {
                $rules["monthPlan.{$f}"] = 'required';
            }
        }
        foreach (config($this->depName . '_fields.user') as $f) {
            if (is_array($f)) {
                $rules["users.*.fields." . $f[0]] = 'required';
                $rules["users.*.fields." . $f[1]] = 'required';
            } else {
                $rules["users.*.fields." . $f] = 'required';
            }
        }

        $rules['chiefs.*.fields.kasko_fact'] = 'required';
        $rules['chiefs.*.fields.redemption_fact'] = 'required';
        $rules['chiefs.*.fields.ogpo_fact'] = 'required';
        $rules['chiefs.*.fields.staging_fact'] = 'required';

        $preparedRules = [];

        return array_merge($rules, $preparedRules);
    }

    public function save()
    {
        $this->yearPlan->save();
        $this->monthPlan->save();
        $this->users->each(function ($user) {
            $user->fields->save();
        });
        $this->chiefs->each(function ($chief) {
            $chief->fields->save();
        });
    }

    public function mount()
    {

        $this->yearPlan = Plan::whereDep($this->subName)
            ->where('year', date('Y'))
            ->where('type', 'year')
            ->first();
        if (!$this->yearPlan) {
            $this->yearPlan = Plan::create([
                'dep' => $this->subName,
                'type' => 'year',
                'year' => date('Y'),
            ]);
        }

        $this->monthPlan = Plan::whereDep($this->subName)
            ->where('month', (int) date('m'))
            ->where('year', date('Y'))
            ->where('type', 'month')
            ->first();
        if (!$this->monthPlan) {
            $this->monthPlan = Plan::create([
                'dep' => $this->subName,
                'type' => 'month',
                'year' => date('Y'),
                'month' => (int) date('m'),
            ]);
        }

        $this->users = User::whereDep($this->subName)->where('active', true)->with('fields')->get();
        $this->chiefs = User::whereIn('ext_id', [])->with('fields')->get();
        $this->chiefs->each(function($user){
            $user->fields()->firstOrCreate();
            $user->load("fields");
        });
    }

    public function render()
    {
        return view('livewire.admin.new-plan');
    }
}
