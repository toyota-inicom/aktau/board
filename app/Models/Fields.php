<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

class Fields extends Model
{
    public function __construct(array $attributes = array())
    {
        $this->setRawAttributes(array(
            'month' => (int)date('m'),
            'year' => (int)date('Y'),
        ), true);
        parent::__construct($attributes);
    }

    protected $table = "fields";
    protected $fillable = [
        "user_id",
        "year",
        "month",
        "trafic_fact",
        "tradein_fact",
        "tradein_plan",
        "testdrive_fact",
        "testdrive_plan",
        "sales_fact",
        "sales_plan",
        "acc_fact",
        "acc_plan",
        "topack_fact",
        "topack_plan",
        "kasko_fact",
        "kasko_plan",
        "redemption_fact",
        "eff",
        "ogpo_fact",
        "ogpo_plan",
        "staging_fact",
        "staging_plan",
        "aspsale_fact",
        "aspsale_plan",
        "asprequest_fact",
        "asprequest_plan",
        "standarts",
        "crm",
        "best",
        "norm_hours_plan",
        "norm_hours_fact",
        "amount_part_plan",
        "amount_part_fact",
        "cr_plan",
        "cr_fact",
        "car_entry_plan",
        "car_entry_fact",
        "illiquid_sales_plan",
        "illiquid_sales_fact",
    ];

    protected $attributes = [
        'month' => ''
    ];

}
