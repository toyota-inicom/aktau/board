<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;

class Plan extends Model
{
    protected $fillable = [
        "type",
        "dep",
        "year",
        "month",
        "tradein_plan",
        "testdrive_plan",
        "sales_plan",
        "acc_plan",
        "topack_plan",
        "kasko_plan",
        "ogpo_plan",
        "staging_plan",
        "aspsale_plan",
        "asprequest_plan",
        "norm_hours_plan",
        "amount_part_plan",
        "cr_plan",
        "car_entry_plan",
        "car_entry_fact",
        "illiquid_sales_plan",
    ];
}
