<?php

namespace App\Console;

use App\Http\Controllers\BuyController;
use App\Http\Controllers\GoogleTable;
use App\Http\Controllers\OpaspController;
use App\Http\Controllers\OpnaController;
use App\Models\User;
use Exception;
use Illuminate\Console\Scheduling\Schedule;
use Illuminate\Foundation\Console\Kernel as ConsoleKernel;
use Illuminate\Support\Facades\Http;

class Kernel extends ConsoleKernel
{
    /**
     * The Artisan commands provided by your application.
     *
     * @var array
     */
    protected $commands = [
        //
    ];

    /**
     * Define the application's command schedule.
     *
     * @param \Illuminate\Console\Scheduling\Schedule $schedule
     * @return void
     */
    protected function schedule(Schedule $schedule)
    {
        $schedule->call(function () {
			$this->updateSalesNew();
            $this->updateSalesBu();
            $this->updateSalesBuy();
			
            try {
                $this->uploadGoogle();
                dump("uploadGoogle");
            } catch (Exception $e) {
               dump($e->getMessage());
            }

            try {
                $this->updateRate();
                dump("updateRate");
            } catch (Exception $e) {
                dump($e->getMessage());
            }
			

            
        })->everyTenMinutes();
    }

    public function uploadGoogle()
    {
        $opna = new OpnaController();
        $opna->uploadToGoogle();
        $opna->gUpAll();
        sleep(2);
        $opasp = new OpaspController();
        $opasp->uploadToGoogle();
        $opasp->gUpAll();
        sleep(2);

        $buy = new BuyController();
        $buy->uploadToGoogle();
        $buy->gUpAll();
    }

    //    public function updateOgpo()
    //    {
    //        $users = User::whereNotNull('dep')->get();
    //        $service = new \Google_Service_Sheets((new GoogleTable())->getClient());
    //        $spreadsheetId = '1pI9tsIokfoJFG5I8ODyPUE-uJO3sIvp3qFSH5xYd-to';
    //        $m = (int)date('m') + 1;
    //        foreach ($users as $user) {
    //            $data = $service->spreadsheets_values->get($spreadsheetId, "{$user->name}!B{$m}");
    //            dump($data);
    ////            if ($data) {
    ////                $user->fields->ogpo_fact = (int)$data[0][0];
    ////                $user->fields->save();
    ////            }
    //        }
    //        dump("OGPO UPDATED");
    //    }


    public function updateRate()
    {
        $users = User::whereNotNull('dep')->get();
        $names = $users->pluck('name');
        $service = new \Google_Service_Sheets((new GoogleTable())->getClient());
        $spreadsheetId = '1Nv_DbcZh7Ay63FVxt5NTS0H-n62opXiUCNNqQ0N4_4s';
        $rateData = $service->spreadsheets_values->get($spreadsheetId, "A:B")['values'];
        foreach ($rateData as $rate) {
            if (!isset($rate[0]) && !isset($rate[1])) {
                continue;
            }

            $name = trim($rate[0]);
            if (!$names->contains($name)) {
                continue;
            }

            $user = $users->where('name', $name)->first();
            $user->rate = (int) $rate[1];
            $user->save();
        }
        echo "\rRATE UPDATED";
    }

    public function updateSalesNew()
    {
        $response = Http::withHeaders([
            'Authorization' => 'Basic ' . base64_encode("r.bakaushina:l4uqr4"),
        ])->get('http://toyotacity.autocrm.ru/yii/api/Stat/commonReport');
        $data = json_decode($response->body(), true)['result']['4']['Сотрудники'];
        $this->updateDataNew(User::whereDep('new')->get(), $data);
        echo "\rSALE CRM UPDATED";
    }

    public function updateSalesBu()
    {
        $response = Http::withHeaders([
            'Authorization' => 'Basic ' . base64_encode("a.beregnoy:dgsdgsdg5454554"),
        ])->get('http://toyotacity.autocrm.ru/yii/sale/case/report/');

        $dom = \phpQuery::newDocument($response->body());
        $data = [];

        $tr = $dom->find("#cases-report tr.level-2");
        foreach ($tr as $row) {
            $row = pq($row);
            $id = (int) str_replace("4-", "", $row->attr('data-id'));
			
            $data[$id] = [
                'Трафик' => (int) $row->find(".js-list-cases")[0]->text(),
            ];
        }
        $this->updateDataBu(User::whereDep('bu')->get(), $data);
        echo "\rASP UPDATED";
    }

    public function updateSalesBuy()
    {
        $response = Http::withHeaders([
            'Authorization' => 'Basic ' . base64_encode("a.beregnoy:dgsdgsdg5454554"),
        ])->get('http://toyotacity.autocrm.ru/yii/tradein/case/report/');

        $dom = \phpQuery::newDocument($response->body());
        $data = [];

        $tr = $dom->find("#cases-report tr.level-2");
        foreach ($tr as $row) {
            $row = pq($row);
            $id = (int) str_replace("4-", "", $row->attr('data-id'));
            $data[$id] = [
                'Трафик' => (int) $row->find(".js-list-cases")[0]->text(),
            ];
        }
		
        $this->updateDataBu(User::whereDep('buy')->get(), $data);
        echo "\rTRADEIN UPDATED";
    }

    public function updateDataNew($users, $data)
    {
        foreach ($users as $user) {
            if (isset($data[$user->ext_id])) {
				$user->fields()->firstOrCreate();
                $user->fields->trafic_fact = $data[$user->ext_id]['Трафик'][0]['value'];
                //$user->fields->testdrive_fact = $data[$user->ext_id]['Тест-драйв'][0]['value'];
                $user->fields->save();
            }
        }
    }

    public function updateDataBu($users, $data)
    {
        foreach ($users as $user) {
            if (isset($data[$user->ext_id])) {
				$user->fields()->firstOrCreate();
				$user->fields->trafic_fact = $data[$user->ext_id]['Трафик'];
                $user->fields->save();
            }
        }
    }

    /**
     * Register the commands for the application.
     *
     * @return void
     */
    protected function commands()
    {
        $this->load(__DIR__ . '/Commands');

        require base_path('routes/console.php');
    }
}
