<?php
return [
    'title' => 'ПЛАНЫ Сервис механики',
    'th' => ['Принято', 'Оценки', 'Выкуп', 'Обмен', 'Снятие'],
    'y' => ['aspsale_plan', 'asprequest_plan', null, 'obmen_plan','withdrawal_plan'],
    'm' => ['aspsale_plan', 'asprequest_plan', null, 'obmen_plan','withdrawal_plan'],
    'user' =>
        [
            'aspsale_plan',
            'asprequest_plan',
            'staging_fact',
            ['obmen_plan', 'obmen_fact', 'Обмен'],
            ['withdrawal_plan', 'withdrawal_fact', 'Снятие'],
        ],
];
