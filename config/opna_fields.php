<?php
return [
    'title' => 'ПЛАНЫ ОПНА',
    'th' => ['Продажи', 'Аксессуары', 'Пакеты ТО', 'КАСКО', 'Trade-In', 'Выкуп', 'Test Drive', 'ОГПО + Safe','Постановка'],
    'y' => ['sales_plan', 'acc_plan', 'topack_plan', 'kasko_plan', 'tradein_plan', null, 'testdrive_plan','ogpo_plan', 'staging_plan'],
    'm' => ['sales_plan', 'acc_plan', 'topack_plan', 'kasko_plan', 'tradein_plan', null, 'testdrive_plan','ogpo_plan', 'staging_plan'],
    'user' =>
    [
        'sales_plan',
        'acc_plan',
        'topack_plan',
        ['kasko_plan', 'kasko_fact', 'КАСКО'],
        'tradein_plan',
        'redemption_fact',
        'testdrive_plan',
        ['ogpo_plan', 'ogpo_fact', 'ОГПО'],
        ['staging_plan', 'staging_fact', 'Постановка'],
    ],
];
