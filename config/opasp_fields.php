<?php
return [
    'title' => 'ПЛАНЫ ОПАСП',
    'th' => ['Продажи', 'Аксессуары', 'Пакеты ТО', 'КАСКО', 'ОГПО + Safe','Выкуп'],
    'y' => ['sales_plan', 'acc_plan', 'topack_plan', 'kasko_plan', 'ogpo_plan',null],
    'm' => ['sales_plan', 'acc_plan', 'topack_plan', 'kasko_plan','ogpo_plan',null],
    'user' =>
    [
        'sales_plan',
        'acc_plan',
        'topack_plan',
        ['kasko_plan', 'kasko_fact', 'КАСКО'],
        ['ogpo_plan', 'ogpo_fact', 'ОГПО'],
        'staging_fact',
    ],
];
