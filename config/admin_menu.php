<?php
return [
    [
        'title' => "Пользователи",
        'route' => 'admin.users'
    ],
    [
        'title' => "План ОПНА",
        'route' => 'admin.plan.opna'
    ],
    [
        'title' => "План ОПАСП",
        'route' => 'admin.plan.bu'
    ],
    [
        'title' => 'План "Отдел выкупа"' ,
        'route' => 'admin.plan.buy'
    ],
    [
        'title' => 'План "Отдела сервиса консультанты"' ,
        'route' => 'admin.plan.service_cons'
    ],
    [
        'title' => 'План "Отдела сервиса механики"' ,
        'route' => 'admin.plan.service_mech'
    ]
];
