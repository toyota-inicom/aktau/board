<?php

// @formatter:off
/**
 * A helper file for your Eloquent Models
 * Copy the phpDocs from this file to the correct Model,
 * And remove them from this file, to prevent double declarations.
 *
 * @author Barry vd. Heuvel <barryvdh@gmail.com>
 */


namespace App\Models{
/**
 * App\Models\Fields
 *
 * @property int $id
 * @property int $user_id
 * @property int $month
 * @property int $trafic_fact
 * @property int $tradein_fact
 * @property int $tradein_plan
 * @property int $testdrive_fact
 * @property int $testdrive_plan
 * @property int $sales_fact
 * @property int $sales_plan
 * @property int $acc_fact
 * @property int $acc_plan
 * @property int $topack_fact
 * @property int $topack_plan
 * @property int $kasko_fact
 * @property int $kasko_plan
 * @property int $redemption_fact
 * @property \Illuminate\Support\Carbon|null $created_at
 * @property \Illuminate\Support\Carbon|null $updated_at
 * @property int $eff
 * @property int $ogpo_fact
 * @property int $ogpo_plan
 * @property int $staging_fact
 * @property int $staging_plan
 * @property int $aspsale_fact
 * @property int $aspsale_plan
 * @property int $asprequest_fact
 * @property int $asprequest_plan
 * @property int $standarts
 * @property int $crm
 * @method static \Illuminate\Database\Eloquent\Builder|Fields newModelQuery()
 * @method static \Illuminate\Database\Eloquent\Builder|Fields newQuery()
 * @method static \Illuminate\Database\Eloquent\Builder|Fields query()
 * @method static \Illuminate\Database\Eloquent\Builder|Fields whereAccFact($value)
 * @method static \Illuminate\Database\Eloquent\Builder|Fields whereAccPlan($value)
 * @method static \Illuminate\Database\Eloquent\Builder|Fields whereAsprequestFact($value)
 * @method static \Illuminate\Database\Eloquent\Builder|Fields whereAsprequestPlan($value)
 * @method static \Illuminate\Database\Eloquent\Builder|Fields whereAspsaleFact($value)
 * @method static \Illuminate\Database\Eloquent\Builder|Fields whereAspsalePlan($value)
 * @method static \Illuminate\Database\Eloquent\Builder|Fields whereCreatedAt($value)
 * @method static \Illuminate\Database\Eloquent\Builder|Fields whereCrm($value)
 * @method static \Illuminate\Database\Eloquent\Builder|Fields whereEff($value)
 * @method static \Illuminate\Database\Eloquent\Builder|Fields whereId($value)
 * @method static \Illuminate\Database\Eloquent\Builder|Fields whereKaskoFact($value)
 * @method static \Illuminate\Database\Eloquent\Builder|Fields whereKaskoPlan($value)
 * @method static \Illuminate\Database\Eloquent\Builder|Fields whereMonth($value)
 * @method static \Illuminate\Database\Eloquent\Builder|Fields whereOgpoFact($value)
 * @method static \Illuminate\Database\Eloquent\Builder|Fields whereOgpoPlan($value)
 * @method static \Illuminate\Database\Eloquent\Builder|Fields whereRedemptionFact($value)
 * @method static \Illuminate\Database\Eloquent\Builder|Fields whereSalesFact($value)
 * @method static \Illuminate\Database\Eloquent\Builder|Fields whereSalesPlan($value)
 * @method static \Illuminate\Database\Eloquent\Builder|Fields whereStagingFact($value)
 * @method static \Illuminate\Database\Eloquent\Builder|Fields whereStagingPlan($value)
 * @method static \Illuminate\Database\Eloquent\Builder|Fields whereStandarts($value)
 * @method static \Illuminate\Database\Eloquent\Builder|Fields whereTestdriveFact($value)
 * @method static \Illuminate\Database\Eloquent\Builder|Fields whereTestdrivePlan($value)
 * @method static \Illuminate\Database\Eloquent\Builder|Fields whereTopackFact($value)
 * @method static \Illuminate\Database\Eloquent\Builder|Fields whereTopackPlan($value)
 * @method static \Illuminate\Database\Eloquent\Builder|Fields whereTradeinFact($value)
 * @method static \Illuminate\Database\Eloquent\Builder|Fields whereTradeinPlan($value)
 * @method static \Illuminate\Database\Eloquent\Builder|Fields whereTraficFact($value)
 * @method static \Illuminate\Database\Eloquent\Builder|Fields whereUpdatedAt($value)
 * @method static \Illuminate\Database\Eloquent\Builder|Fields whereUserId($value)
 */
	class Fields extends \Eloquent {}
}

namespace App\Models{
/**
 * App\Models\Plan
 *
 * @property int $id
 * @property string $type
 * @property string $dep
 * @property int $year
 * @property int $month
 * @property int $tradein_plan
 * @property int $testdrive_plan
 * @property int $sales_plan
 * @property int $acc_plan
 * @property int $topack_plan
 * @property int $kasko_plan
 * @property \Illuminate\Support\Carbon|null $created_at
 * @property \Illuminate\Support\Carbon|null $updated_at
 * @property int $ogpo_plan
 * @property int $staging_plan
 * @property int $aspsale_plan
 * @property int $asprequest_plan
 * @method static \Illuminate\Database\Eloquent\Builder|Plan newModelQuery()
 * @method static \Illuminate\Database\Eloquent\Builder|Plan newQuery()
 * @method static \Illuminate\Database\Eloquent\Builder|Plan query()
 * @method static \Illuminate\Database\Eloquent\Builder|Plan whereAccPlan($value)
 * @method static \Illuminate\Database\Eloquent\Builder|Plan whereAsprequestPlan($value)
 * @method static \Illuminate\Database\Eloquent\Builder|Plan whereAspsalePlan($value)
 * @method static \Illuminate\Database\Eloquent\Builder|Plan whereCreatedAt($value)
 * @method static \Illuminate\Database\Eloquent\Builder|Plan whereDep($value)
 * @method static \Illuminate\Database\Eloquent\Builder|Plan whereId($value)
 * @method static \Illuminate\Database\Eloquent\Builder|Plan whereKaskoPlan($value)
 * @method static \Illuminate\Database\Eloquent\Builder|Plan whereMonth($value)
 * @method static \Illuminate\Database\Eloquent\Builder|Plan whereOgpoPlan($value)
 * @method static \Illuminate\Database\Eloquent\Builder|Plan whereSalesPlan($value)
 * @method static \Illuminate\Database\Eloquent\Builder|Plan whereStagingPlan($value)
 * @method static \Illuminate\Database\Eloquent\Builder|Plan whereTestdrivePlan($value)
 * @method static \Illuminate\Database\Eloquent\Builder|Plan whereTopackPlan($value)
 * @method static \Illuminate\Database\Eloquent\Builder|Plan whereTradeinPlan($value)
 * @method static \Illuminate\Database\Eloquent\Builder|Plan whereType($value)
 * @method static \Illuminate\Database\Eloquent\Builder|Plan whereUpdatedAt($value)
 * @method static \Illuminate\Database\Eloquent\Builder|Plan whereYear($value)
 */
	class Plan extends \Eloquent {}
}

namespace App\Models{
/**
 * App\Models\User
 *
 * @property int $id
 * @property string $name
 * @property string|null $photo
 * @property int|null $ext_id
 * @property string|null $email
 * @property string|null $password
 * @property int $active
 * @property string|null $dep
 * @property \Illuminate\Support\Carbon|null $created_at
 * @property \Illuminate\Support\Carbon|null $updated_at
 * @property int $position
 * @property string $arrow
 * @property int $rate
 * @property-read \Illuminate\Database\Eloquent\Collection|\App\Models\Fields[] $all_fields
 * @property-read int|null $all_fields_count
 * @property-read \App\Models\Fields|null $fields
 * @method static \Illuminate\Database\Eloquent\Builder|User newModelQuery()
 * @method static \Illuminate\Database\Eloquent\Builder|User newQuery()
 * @method static \Illuminate\Database\Eloquent\Builder|User query()
 * @method static \Illuminate\Database\Eloquent\Builder|User whereActive($value)
 * @method static \Illuminate\Database\Eloquent\Builder|User whereArrow($value)
 * @method static \Illuminate\Database\Eloquent\Builder|User whereCreatedAt($value)
 * @method static \Illuminate\Database\Eloquent\Builder|User whereDep($value)
 * @method static \Illuminate\Database\Eloquent\Builder|User whereEmail($value)
 * @method static \Illuminate\Database\Eloquent\Builder|User whereExtId($value)
 * @method static \Illuminate\Database\Eloquent\Builder|User whereId($value)
 * @method static \Illuminate\Database\Eloquent\Builder|User whereName($value)
 * @method static \Illuminate\Database\Eloquent\Builder|User wherePassword($value)
 * @method static \Illuminate\Database\Eloquent\Builder|User wherePhoto($value)
 * @method static \Illuminate\Database\Eloquent\Builder|User wherePosition($value)
 * @method static \Illuminate\Database\Eloquent\Builder|User whereRate($value)
 * @method static \Illuminate\Database\Eloquent\Builder|User whereUpdatedAt($value)
 */
	class User extends \Eloquent {}
}

